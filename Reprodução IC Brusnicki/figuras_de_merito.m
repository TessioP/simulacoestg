function [Ik,Jk]=figuras_de_merito(q,qe)
De = quaternion2DCM(qe); % DCM estimada
D = quaternion2DCM(q); % DCM verdadeira
Ik = 180/pi*abs(acos(0.5*(trace(De'*D) - 1))); %erro angular em graus
Jk = trace((De'*De-eye(3))*(De'*De-eye(3))');%�ndice de ortogonalidade
end