function [q2,fi] = propaga_q_Brusnicki(q1,w1,dt)
    % c�lculo da matriz de transi��o de estado
    W = 0.5*[0 -w1(1) -w1(2) -w1(3);
        w1(1) 0 w1(3) -w1(2);
        w1(2) -w1(3) 0 w1(1);
        w1(3) w1(2) -w1(1) 0];
    n_w = norm(w1);
    if n_w~=0
        fi=cos(n_w*dt/2)*eye(4)+1/n_w*sin(n_w*dt/2)*W;
    else
        fi = eye(4);
    end
    %matriz de transi��o de estado
    q2 = fi*q1; % propagac�o do quat�rnion
end