function [futureQuaternion, stateTransitionMatrix] = quaternionPropagation(currentQuaternion, currentAngularSpeed, step)
%QUATERNIONPROPAGATION Summary of this function goes here
%   Detailed explanation goes here

% c�lculo da matriz de transi��o de estado
angularSpeedPropagation = 0.5*[0 -currentAngularSpeed(1) -currentAngularSpeed(2) -currentAngularSpeed(3);
                               currentAngularSpeed(1) 0 currentAngularSpeed(3) -currentAngularSpeed(2);
                               currentAngularSpeed(2) -currentAngularSpeed(3) 0 currentAngularSpeed(1);
                               currentAngularSpeed(3) currentAngularSpeed(2) -currentAngularSpeed(1) 0];

angularSpeedNorm = norm(currentAngularSpeed);

if angularSpeedNorm~=0
        stateTransitionMatrix = (cos(angularSpeedNorm*step/2)*eye(4)+1/angularSpeedNorm*sin(angularSpeedNorm*step/2)*angularSpeedPropagation);
    else
        stateTransitionMatrix = eye(4);
end

futureQuaternion = stateTransitionMatrix*currentQuaternion;

futureQuaternion = futureQuaternion/norm(futureQuaternion);
end

