function [qe_k1_k1,P_k1_k1] = extendedKalmanFilter(qe_k,P_k,wm_k,Q,R,r1,r2,b1,b2,dt)
%EXTENDEDKALMANFILTER Summary of this function goes here
%   Detailed explanation goes here

%% 1a)Propaga��o do estado estimado
% C�lculo de qhat_e(k+1|k):
% propaga��o do estado estimado pela din�mica do sistema
[qe_k1_k, stateTransitionMatrix] = quaternionPropagation(qe_k, wm_k, dt);

% C�lculo de P(k+1|k):
% Propaga��o da matriz de covari�ncia (nesse passo, a incerteza deve
% aumentar)
Csi_k = [-qe_k(2) -qe_k(3) -qe_k(4);
          qe_k(1) -qe_k(4)  qe_k(3);
          qe_k(4)  qe_k(1) -qe_k(2);
         -qe_k(3)  qe_k(2)  qe_k(1)];
Gama_k = dt/2*stateTransitionMatrix*Csi_k;
Q_q_k = Gama_k*Q*Gama_k';
P_k1_k = stateTransitionMatrix*P_k*stateTransitionMatrix'+Q_q_k;

%% Predi��o das medidas
% C�lculo de bhat(k+1|k)
D_q_k = quaternion2DCM(qe_k1_k);
b_estimado = [(D_q_k*r1)' (D_q_k*r2)']';

% C�lculo de Pb(k+1|k)
dDdq1 = [ 2*qe_k1_k(1)   2*qe_k1_k(4) -2*qe_k1_k(3);
          -2*qe_k1_k(4)  2*qe_k1_k(1)  2*qe_k1_k(2);
          2*qe_k1_k(3)  -2*qe_k1_k(2)  2*qe_k1_k(1)];

dDdq2 = [ 2*qe_k1_k(2)   2*qe_k1_k(3)  2*qe_k1_k(4);
          2*qe_k1_k(3)  -2*qe_k1_k(2)  2*qe_k1_k(1);
          2*qe_k1_k(4)  -2*qe_k1_k(1) -2*qe_k1_k(2)];

dDdq3 = [-2*qe_k1_k(3)   2*qe_k1_k(2) -2*qe_k1_k(1);
          2*qe_k1_k(2)   2*qe_k1_k(3)  2*qe_k1_k(4);
          2*qe_k1_k(1)   2*qe_k1_k(4) -2*qe_k1_k(3)];

dDdq4 = [-2*qe_k1_k(4)   2*qe_k1_k(1)  2*qe_k1_k(2);
         -2*qe_k1_k(1)  -2*qe_k1_k(4)  2*qe_k1_k(3);
          2*qe_k1_k(2)   2*qe_k1_k(3)  2*qe_k1_k(4)];

H_1_q = [dDdq1*r1 dDdq2*r1 dDdq3*r1 dDdq4*r1];

H_2_q = [dDdq1*r2 dDdq2*r2 dDdq3*r2 dDdq4*r2];

H_q = [H_1_q; H_2_q];

Pb_k1_k = H_q*P_k1_k*H_q' + R;

%% 1c) Covari�ncia cruzada, Pqb(k+1_k)
Pbq_k1_k = P_k1_k*H_q';

%% 2a) Ganho de Kalman
K = Pbq_k1_k*inv(Pb_k1_k);

%% 2b) Atualiza��o da estimativa
qe_k1_k1 = qe_k1_k + K*( [b1' b2']' - b_estimado );
P_k1_k1 = P_k1_k - K*Pb_k1_k*K';

%% 3) Normaliza��o
qe_k1_k1 = (1/norm(qe_k1_k1))*qe_k1_k1;

end

