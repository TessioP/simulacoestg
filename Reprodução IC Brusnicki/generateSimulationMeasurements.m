function [realAngularSpeeds, measuredAngularSpeeds, realQuaternions, measuredVectors, realDCM] = generateSimulationMeasurements(landmarkPositions, bodyNoise, gyrometerNoise, times)
step = times(2)-times(1);

realQuaternions = zeros(4,length(times));

realQuaternions(:,1) = [1 0 0 0]';

realAngularSpeeds = [0.1*sin(times); 0.1*cos(times); -0.1*(sin(times).*cos(times))];

realDCM = zeros(3,3,length(times));

realDCM(:,:,1) = quaternion2DCM(realQuaternions(:,1));

measuredAngularSpeeds = zeros(3, length(times));
measuredAngularSpeeds(:, 1) = sqrtm(gyrometerNoise)*randn(3,1);

measuredVectors.land1 = zeros(3,length(times));
measuredVectors.land2 = zeros(3,length(times));

measuredVectors.land1(:,1) = realDCM(:,:,1)*landmarkPositions(:,1) + sqrtm(bodyNoise)*randn(3,1);
measuredVectors.land2(:,1) = realDCM(:,:,1)*landmarkPositions(:,2) + sqrtm(bodyNoise)*randn(3,1);

for k=1:length(times)-1
    %% Realizando a propaga��o do quat�rnio e convertendo para DCM
    realQuaternions(:,k+1) = quaternionPropagation(realQuaternions(:,k), realAngularSpeeds(:,k), step);
    realDCM(:,:,k+1) = quaternion2DCM(realQuaternions(:,k+1));
    %% Adicionando ru�do aos valores corretos
    measuredAngularSpeeds(:, k+1) = realAngularSpeeds(:, k+1) + sqrtm(gyrometerNoise)*randn(3,1);
    measuredVectors.land1(:, k+1) = realDCM(:,:,k+1)*landmarkPositions(:,1)+sqrtm(bodyNoise)*randn(3,1);
    measuredVectors.land2(:, k+1) = realDCM(:,:,k+1)*landmarkPositions(:,2)+sqrtm(bodyNoise)*randn(3,1);
end

