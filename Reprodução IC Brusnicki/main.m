clear all;

%% Par�metros de tempo

step = 0.01;

simulationTime = 100;

times = 0:step:simulationTime;

%% Ru�dos para a simula��o da medi��o

sigma_w = 0.005;

sigma_b = 0.01;

gyrometerNoise = eye(3)*sigma_w^2;

bodyNoise = eye(3)*sigma_b^2;

%% Ru�dos utilizados no filtro

R = bodyNoise;

Q = eye(3)*0.05^2;

%% Posi��es iniciais das landmarks

landmarkPositions = [0 0; 5/13 -5/13; -12/13 -12/13];

%% Condi��es iniciais do sistema

P(:,:,1)=0.01*eye(4);

%% Condi��es iniciais do filtro

estimatedQuaternions = zeros(4, length(times));

estimatedQuaternions(:,1) = [1 0 0 0]';

%% Simula��o das medidas

[realAngularSpeeds, measuredAngularSpeeds, realQuaternions, measuredVectors, realDCM] = generateSimulationMeasurements(landmarkPositions, bodyNoise, gyrometerNoise, times);

R = eye(6)*sigma_b^2;

%% Filtro de Kalman Estendido
for k=1:length(times)-1
    [estimatedQuaternions(:,k+1),P(:,:,k+1)] = extendedKalmanFilter(estimatedQuaternions(:,k),P(:,:,k),measuredAngularSpeeds(:,k),Q,R,landmarkPositions(:, 1),landmarkPositions(:,2),measuredVectors.land1(:, k),measuredVectors.land2(:, k),step);
    [Ik(k+1),Jk(k+1)] = figuras_de_merito(realQuaternions(:,k+1),estimatedQuaternions(:,k+1));
end

%% Graficos
figure; hold on;
plot(measuredAngularSpeeds');title('Velocidade angular medida em rad/s');
figure; plot(estimatedQuaternions','b'); plot(realQuaternions','r');
figure; plot(Jk); title('�ndice de Ortogonalidade');
figure; plot(Ik); title('Erro Angular em graus');