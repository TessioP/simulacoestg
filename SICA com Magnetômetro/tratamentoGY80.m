%% Gir�metro
fileName = 'dadosMesa.txt';

data = importdata(fileName);

wx = data(1.450e4:2.000e4, 7);
wy = data(1.450e4:2.000e4, 8);
wz = data(1.450e4:2.000e4, 9);

plot(wx)
hold on;
plot(wy)
hold on;
plot(wz)
legend('wx','wy','wz')

constant10grausPositivo = mean(wy(2000:3000));
constant10grausNegativo = mean(wy(4000:5000));

biasY = 0.5*(constant10grausPositivo + constant10grausNegativo);

K = (constant10grausPositivo - biasY)/10;

figure()
plot(wx/K*pi/180)
hold on;
plot(wy/K*pi/180)
hold on;
plot(wz/K*pi/180)
grid on
legend('wx','wy','wz')

%% Magnet�metro

% Seleciona o arduino conectado na porta com3. Checar se o arduino est� de
% fato conectado nessa porta.
s = serial('com3');

% Configura a baudrate para 115200 bps
set(s,'BaudRate', 115200); % 

fopen(s);

flushinput(s);

fscanf(s);


disp('Iniciando calibra��o');
for k = 1:20000
%     if k==1000
%         disp('X positivo. Pr�ximo: X negativo');
%     elseif k==2000
%         disp('X negativo. Pr�ximo: Y positivo');
%     elseif k ==3000
%         disp('Y positivo. Pr�ximo: Y negativo');
%     elseif k==4000
%         disp('Y negativo. Pr�ximo: Z positivo');
%     elseif k==5000
%         disp('Z positivo. Pr�ximo: Z negativo');
%     elseif k==6000
%         disp('Z negativo');
%     end
    if(rem(k, 1000)==0)
        disp(k);
    end
    
    A = fscanf(s);
    data = str2double(strsplit(A,'\t'));
    
    magBrute(:, k) = [data(1), data(2), data(3)];
end

fileId = fopen('magCalibration.txt', 'w');
fprintf(fileId, '%3d %3d %3d \n', magBrute);
fclose(fileId);

fileName3 = 'magCalibration.txt';

data3 = importdata(fileName3);

mx = data3(:,1);
my = data3(:,2);
mz = data3(:,3);

biasMx = (max(mx) + min(mx))/2;
biasMy = (max(my) + min(my))/2;
biasMz = (max(mz) + min(mz))/2;

scaleMx = (max(mx) - min(mx))/2;
scaleMy = (max(my) - min(my))/2;
scaleMz = (max(mz) - min(mz))/2;

scale = (scaleMx + scaleMy + scaleMz)/3;

scaleMx = scale/scaleMx;

scaleMy = scale/scaleMy;

scaleMz = scale/scaleMz;

newMx = mx*scaleMx - biasMx;

newMy = my*scaleMy - biasMy;

newMz = mz*scaleMz - biasMz;

close all;

figure()
plot(newMx);
hold on
plot(mx);
grid on;
legend('m_x calibrado','m_x bruto')

figure()
plot(newMy);
hold on
plot(my);
grid on;
legend('m_y calibrado','m_y bruto')

figure()
plot(newMz);
hold on
plot(mz);
grid on;
legend('m_z calibrado','m_z bruto')


figure()
plot3(mx,my,mz,'.');
hold on
plot3(newMx, newMy, newMz,'.');
grid on
axis equal;
% fclose(s);