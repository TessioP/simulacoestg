%% Declara��es iniciais

% Seleciona o arduino conectado na porta com3. Checar se o arduino est� de
% fato conectado nessa porta.
% s = serial('com3');
% 
% % Configura a baudrate para 115200 bps
% set(s,'BaudRate', 115200); % 


%% Initial calibration
% Calibra��o simples: apenas estima o bias do gir�metro e calcula as
% matrizes R e Q que ser�o utilizadas no QEKF

disp('Iniciando calibra��o.');
fclose(s);
fopen(s);
flushinput(s);
fscanf(s);

n=500; % Quantidade de dados que ser�o utilizados na calibra��o

C = zeros(n,10);
mag = zeros(n,3);
acc = zeros(n,3);
gyro = zeros(n,3);
step = zeros(n,1);

bias.gyro.x = 0; bias.gyro.y = 0; bias.gyro.z = 0;

scale.gyro = 1;

scale.gyro = pi/(180*114.5099);

bias.mag.x = -160.5; bias.mag.y = -89; bias.mag.z = -106;

scale.mag.x = 1.1021; scale.mag.y = 0.8711; scale.mag.z = 1.0586;

for i=1:n
    A = fscanf(s);
    data = str2double(strsplit(A,'\t'));
    [mag(i,:), acc(i,:), gyro(i,:), step(i,:)] = analizeVectors(data, scale, bias);
end

[R, Q, bias.gyro, noises] = calibration(mag, acc, gyro);

% Os dados do fator de escala do gir�metro foram levantados
% experimentalmente ao se colocar a IMU em uma mesa que gira com velocidade
% angular conhecida.

% Os dados para o fator de escala e bias do magnet�metro foram levantados
% realizando um experimento simples de rota��o em torno dos tr�s eixos.

clear mag acc gyro;

disp('Calibra��o finalizada.');

%% Loop do filtro
% Configura a quantidade de dados que ser�o coletados pelo arduino
simulationSteps = 3500;

% Coleta a primeira amostra de dados e inicializa os objetos em que se
% armazenar�o os dados
A = fscanf(s);
data = str2double(strsplit(A,'\t'));

% Armazena os dados lidos diretamente do arduino
magData.Brute = Magnetometer(data(1), data(2), data(3));
accData.Brute = Accelerometer(data(4), data(5), data(6));
gyroData.Brute = Gyrometer(data(7), data(8), data(9));

% Trata os dados lidos do arduino
[mag, acc, gyro, initialTime] = analizeVectors(data, scale, bias);

% Assume-se que a posi��o inicial do corpo � a primeira leitura dos
% sensores
initialAcc = [acc(1); acc(2); acc(3)];
initialMag = [mag(1); mag(2); mag(3)];

% Armazena os dados tratados
magData.Measured = Magnetometer(mag(1), mag(2), mag(3));
accData.Measured = Accelerometer(acc(1), acc(2), acc(3));
gyroData.Measured = Gyrometer(gyro(1), gyro(2), gyro(3));

% Considera-se a atitude inicial do corpo como atitude de refer�ncia
quaternions.Estimated = Quaternion(0.1,0.1,0.1,1);
quaternions.Propagated = Quaternion(0,0,0,1);

% Considera-se que o filtro come�a correto
accData.Estimated = Accelerometer(accData.Measured.ax(1), accData.Measured.ay(1), accData.Measured.az(1));
magData.Estimated = Magnetometer(magData.Measured.mx(1), magData.Measured.my(1),magData.Measured.mz(1));
gyroData.Estimated = Gyrometer(gyroData.Measured.p(1), gyroData.Measured.q(1), gyroData.Measured.r(1));

% Inicializa��o da covari�ncia do erro de estima��o
P(:,:,1)=eye(7);

elapsedTime = zeros(1,simulationSteps);

for k = 1:simulationSteps
    if(k==500)
        disp('Iniciar movimento 1');
    elseif(k == 1500)
        disp('Iniciar movimento 2');
    elseif(k == 2500)
        disp('Iniciar movimento 3');
    end
    
    A = fscanf(s);
    data = str2double(strsplit(A,'\t'));
    
    magData.Brute = magData.Brute.addPoint(data(1), data(2), data(3));
    accData.Brute = accData.Brute.addPoint(data(4), data(5), data(6));
    gyroData.Brute = gyroData.Brute.addPoint(data(7), data(8), data(9));
    
    [mag, acc, gyro, time] = analizeVectors(data, scale, bias);
    
    elapsedTime(k) = time - initialTime;
    initialTime = time;
    
    magData.Measured = magData.Measured.addPoint(mag(1), mag(2), mag(3));   
    accData.Measured = accData.Measured.addPoint(acc(1), acc(2), acc(3));
    gyroData.Measured = gyroData.Measured.addPoint(gyro(1), gyro(2), gyro(3));
    
    quaternions.Propagated = quaternions.Propagated.addPointFromPQR(gyro(1), gyro(2), gyro(3), elapsedTime(k));
    
    q_k = [quaternions.Estimated.q1(k); quaternions.Estimated.q2(k); quaternions.Estimated.q3(k); quaternions.Estimated.q4(k)];
    u_k = [gyroData.Measured.p(k); gyroData.Measured.q(k); gyroData.Measured.r(k)];
    x_k = [q_k; u_k];
    zMedido_k1 = [accData.Measured.ax(k+1); accData.Measured.ay(k+1); accData.Measured.az(k+1); magData.Measured.mx(k+1); magData.Measured.my(k+1); magData.Measured.mz(k+1)];
    
    [x_k1, P(:,:,k+1), z_k1(k), ze_k1(k)] = extendedKalmanFilter(x_k, P(:,:,k), u_k, zMedido_k1, Q, R, initialAcc, initialMag, elapsedTime(k));
    
    quaternions.Estimated = quaternions.Estimated.addPointFromQuaternion(x_k1(1), x_k1(2), x_k1(3), x_k1(4));
    gyroData.Estimated = gyroData.Estimated.addPoint(x_k1(5), x_k1(6), x_k1(7));
    accData.Estimated = accData.Estimated.addPoint(z_k1(1), z_k1(2), z_k1(3));
    magData.Estimated = magData.Estimated.addPoint(z_k1(4), z_k1(5), z_k1(6));
end

%% Gr�ficos dos quaternions
close all;
for k = 1:20:simulationSteps
    q = [quaternions.Estimated.q1(k), quaternions.Estimated.q2(k), quaternions.Estimated.q3(k), quaternions.Estimated.q4(k)];
    plot_frame(q);
end

fclose(s);