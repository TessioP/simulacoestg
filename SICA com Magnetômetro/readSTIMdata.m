function [stim] = readSTIMdata(fileName)
%READAHRSDATA Summary of this function goes here
%   Detailed explanation goes here
data = dlmread(fileName,';', 10,0);

times = data(:,1);
gyro_STIM_X_1 = data(:,2);
gyro_STIM_Y_1 = data(:,3);
gyro_STIM_Z_1 = data(:,4);
acc_STIM_X_1 = data(:,5);
acc_STIM_Y_1 = data(:,6);
acc_STIM_Z_1 = data(:,7);
counter_1 = data(:,8);
gyro_STIM_X_2 = data(:,9);
gyro_STIM_Y_2 = data(:,10);
gyro_STIM_Z_2 = data(:,11);
acc_STIM_X_2 = data(:,12);
acc_STIM_Y_2 = data(:,13);
acc_STIM_Z_2 = data(:,14);
counter_2 = data(:,15);
gyro_STIM_X_3 = data(:,16);
gyro_STIM_Y_3 = data(:,17);
gyro_STIM_Z_3 = data(:,18);
acc_STIM_X_3 = data(:,19);
acc_STIM_Y_3 = data(:,20);
acc_STIM_Z_3 = data(:,21);
counter_3 = data(:,22);
gyro_STIM_X_4 = data(:,23);
gyro_STIM_Y_4 = data(:,24);
gyro_STIM_Z_4 = data(:,25);
acc_STIM_X_4 = data(:,26);
acc_STIM_Y_4 = data(:,27);
acc_STIM_Z_4 = data(:,28);
counter_4 = data(:,29);
gyro_STIM_X_5 = data(:,30);
gyro_STIM_Y_5 = data(:,31);
gyro_STIM_Z_5 = data(:,32);
acc_STIM_X_5 = data(:,33);
acc_STIM_Y_5 = data(:,34);
acc_STIM_Z_5 = data(:,35);
counter_5 = data(:,36);


for i=1:length(times)
    gyro_STIM_X(5*i-4) = gyro_STIM_X_1(i);
    gyro_STIM_X(5*i-3) = gyro_STIM_X_2(i);
    gyro_STIM_X(5*i-2) = gyro_STIM_X_3(i);
    gyro_STIM_X(5*i-1) = gyro_STIM_X_4(i);
    gyro_STIM_X(5*i) = gyro_STIM_X_5(i);
    
    gyro_STIM_Y(5*i-4) = gyro_STIM_Y_1(i);
    gyro_STIM_Y(5*i-3) = gyro_STIM_Y_2(i);
    gyro_STIM_Y(5*i-2) = gyro_STIM_Y_3(i);
    gyro_STIM_Y(5*i-1) = gyro_STIM_Y_4(i);
    gyro_STIM_Y(5*i) = gyro_STIM_Y_5(i);

    gyro_STIM_Z(5*i-4) = gyro_STIM_Z_1(i);
    gyro_STIM_Z(5*i-3) = gyro_STIM_Z_2(i);
    gyro_STIM_Z(5*i-2) = gyro_STIM_Z_3(i);
    gyro_STIM_Z(5*i-1) = gyro_STIM_Z_4(i);
    gyro_STIM_Z(5*i) = gyro_STIM_Z_5(i);

    acc_STIM_X(5*i-4) = acc_STIM_X_1(i);
    acc_STIM_X(5*i-3) = acc_STIM_X_2(i);
    acc_STIM_X(5*i-2) = acc_STIM_X_3(i);
    acc_STIM_X(5*i-1) = acc_STIM_X_4(i);
    acc_STIM_X(5*i) = acc_STIM_X_5(i);
    
    acc_STIM_Y(5*i-4) = acc_STIM_Y_1(i);
    acc_STIM_Y(5*i-3) = acc_STIM_Y_2(i);
    acc_STIM_Y(5*i-2) = acc_STIM_Y_3(i);
    acc_STIM_Y(5*i-1) = acc_STIM_Y_4(i);
    acc_STIM_Y(5*i) = acc_STIM_Y_5(i);

    acc_STIM_Z(5*i-4) = acc_STIM_Z_1(i);
    acc_STIM_Z(5*i-3) = acc_STIM_Z_2(i);
    acc_STIM_Z(5*i-2) = acc_STIM_Z_3(i);
    acc_STIM_Z(5*i-1) = acc_STIM_Z_4(i);
    acc_STIM_Z(5*i) = acc_STIM_Z_5(i);
end

stim.times = 0:20e-3/5:(length(acc_STIM_X)-1)*20e-3/5;
stim.wx = gyro_STIM_X;
stim.wy = gyro_STIM_Y;
stim.wz = gyro_STIM_Z;
stim.ax = acc_STIM_X;
stim.ay = acc_STIM_Y;
stim.az = acc_STIM_Z;
end

