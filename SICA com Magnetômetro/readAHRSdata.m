function ahrs = readAHRSdata(fileName)
%READAHRSDATA Summary of this function goes here
%   Detailed explanation goes here
data = dlmread(fileName,';', 10,0);
times = data(:,1);
heading_1 = data(:,2);
pitch_1 = data(:,3);
roll_1 = data(:,4);
gyro_AHRS_X_1 = data(:,5);
gyro_AHRS_Y_1 = data(:,6);
gyro_AHRS_Z_1 = data(:,7);
acc_AHRS_X_1 = data(:,8);
acc_AHRS_Y_1 = data(:,9);
acc_AHRS_Z_1 = data(:,10);
mag_AHRS_X_1 = data(:,11);
mag_AHRS_Y_1 = data(:,12);
mag_AHRS_Z_1 = data(:,13);
heading_2 = data(:,14);
pitch_2 = data(:,15);
roll_2 = data(:,16);
gyro_AHRS_X_2 = data(:,17);
gyro_AHRS_Y_2 = data(:,18);
gyro_AHRS_Z_2 = data(:,19);
acc_AHRS_X_2 = data(:,20);
acc_AHRS_Y_2 = data(:,21);
acc_AHRS_Z_2 = data(:,22);
mag_AHRS_X_2 = data(:,23);
mag_AHRS_Y_2 = data(:,24);
mag_AHRS_Z_2 = data(:,25);
% 
for i=1:length(times)
    heading(2*i-1) = heading_1(i);
    heading(2*i) = heading_2(i);
    
    pitch(2*i-1) = pitch_1(i);
    pitch(2*i) = pitch_2(i);
    
    roll(2*i-1) = roll_1(i);
    roll(2*i) = roll_2(i);
    
    
    
    gyro_AHRS_X(2*i-1) = gyro_AHRS_X_1(i);
    gyro_AHRS_X(2*i) = gyro_AHRS_X_2(i);
    gyro_AHRS_Y(2*i-1) = gyro_AHRS_Y_1(i);
    gyro_AHRS_Y(2*i) = gyro_AHRS_Y_2(i);
    gyro_AHRS_Z(2*i-1) = gyro_AHRS_Z_1(i);
    gyro_AHRS_Z(2*i) = gyro_AHRS_Z_2(i);
    
    
    acc_AHRS_X(2*i-1) = acc_AHRS_X_1(i);
    acc_AHRS_X(2*i) = acc_AHRS_X_2(i);
    acc_AHRS_Y(2*i-1) = acc_AHRS_Y_1(i);
    acc_AHRS_Y(2*i) = acc_AHRS_Y_2(i);
    acc_AHRS_Z(2*i-1) = acc_AHRS_Z_1(i);
    acc_AHRS_Z(2*i) = acc_AHRS_Z_2(i);
    
    
    mag_AHRS_X(2*i-1) = mag_AHRS_X_1(i);
    mag_AHRS_X(2*i) = mag_AHRS_X_2(i);
    mag_AHRS_Y(2*i-1) = mag_AHRS_Y_1(i);
    mag_AHRS_Y(2*i) = mag_AHRS_Y_2(i);
    mag_AHRS_Z(2*i-1) = mag_AHRS_Z_1(i);
    mag_AHRS_Z(2*i) = mag_AHRS_Z_2(i);
end

ahrs.wx = gyro_AHRS_X;
ahrs.wy = gyro_AHRS_Y;
ahrs.wz = gyro_AHRS_Z;

ahrs.ax = acc_AHRS_X;
ahrs.ay = acc_AHRS_Y;
ahrs.az = acc_AHRS_Z;

ahrs.mx = mag_AHRS_X;
ahrs.my = mag_AHRS_Y;
ahrs.mz = mag_AHRS_Z;

ahrs.heading = heading;

ahrs.pitch = pitch;

ahrs.roll = roll;

ahrs.times = 0:10e-3:(2*length(times)-1)*10e-3;

end

