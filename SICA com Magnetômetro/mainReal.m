close all;
% clear accData speedData quaternions;
clear all;

%% Leitura dos dados

fileName = 'teste5-270318.txt';
data = importdata(fileName);
mx = data(:,1);
my = data(:,2);
mz = data(:,3);
normaMag = sqrt(mx.^2 + my.^2 + mz.^2);
mx = mx./normaMag;
my = my./normaMag;
mz = mz./normaMag;
bias = 2*pi/4.8578;
ax = data(:,4);
ay = data(:,5);
az = data(:,6);
normaAcc = sqrt(ax.^2 + ay.^2 + az.^2);
ax = ax./normaAcc;
ay = ay./normaAcc;
az = az./normaAcc;
wx = data(:,7)*1e-6*bias;
wy = data(:,8)*1e-6*bias;
wz = data(:,9)*1e-6*bias;
times = data(:,10)*1e-6;

%% Par�metros de tempo
% clc;
% 
% step = 1e-3;
% 
% simulationTime = 200;
% 
% times = 0:step:simulationTime;

%% Ru�dos para a simula��o da medi��o
clc;

accNoise = diag([std(ax(1:500))^2,std(ay(1:500))^2,std(az(1:500))^2]); %covari�ncia do ru�do de medidas dos gir�metros

gyroNoise = diag([std(wx(1:500))^2,std(wy(1:500))^2,std(wz(1:500))^2]);

magNoise = diag([std(mx(1:500))^2,std(my(1:500))^2,std(mz(1:500))^2]);
%% Ru�dos utilizados no filtro
clc;

R = diag([std(ax(1:500))^2,std(ay(1:500))^2,std(az(1:500))^2, std(mx(1:500))^2,std(my(1:500))^2,std(mz(1:500))^2]);

Q = diag([std(wx(1:500))^2, std(wx(1:500))^2, std(wx(1:500))^2, std(wx(1:500))^2,std(wx(1:500))^2,std(wy(1:500))^2,std(wz(1:500))^2]);

%% Leitura inicial
clc;

initialAcc = [ax(1) ay(1) az(1)]';

initialMag = [mx(1) ; my(1); mz(1)];

%% Condi��es iniciais do sistema
clc;

P(:,:,1)=0.01*eye(7); % Covari�ncia do erro de estima��o no instante zero

%% Condi��es iniciais do filtro
clc;

%% Simula��o das medidas
% [speedData, accData, magData, quaternions] = generateData(times, gyroNoise, initialAcc, accNoise, initialMag, magNoise);

speedData.Measured = Gyrometer(wx,wy,wz);

accData.Measured = Accelerometer(ax,ay,az);

magData.Measured = Magnetometer(mx,my,mz);

quaternions.Estimated = Quaternion(0,0,0,1);

speedData.Estimated = Gyrometer(speedData.Measured.p(1), speedData.Measured.q(1), speedData.Measured.r(1));

accData.Estimated = Accelerometer(accData.Measured.ax(1), accData.Measured.ay(1), accData.Measured.az(1));

magData.Estimated = Magnetometer(magData.Measured.mx(1), magData.Measured.my(1),magData.Measured.mz(1));
%% Filtro de Kalman
clc;
for k=1:length(times)-1
    q_k = [quaternions.Estimated.q1(k); quaternions.Estimated.q2(k); quaternions.Estimated.q3(k); quaternions.Estimated.q4(k)];
    u_k = [speedData.Measured.p(k); speedData.Measured.q(k); speedData.Measured.r(k)];
    x_k = [q_k; u_k];
    z_k1 = [accData.Measured.ax(k+1); accData.Measured.ay(k+1); accData.Measured.az(k+1); magData.Measured.mx(k+1); magData.Measured.my(k+1); magData.Measured.mz(k+1)];
    [x_k1, P(:,:,k+1), ze_k1] = extendedKalmanFilter(x_k, P(:,:,k), u_k, z_k1, Q, R, initialAcc, initialMag, times(k));
    quaternions.Estimated = quaternions.Estimated.addPointFromQuaternion(x_k1(1), x_k1(2), x_k1(3), x_k1(4));
    speedData.Estimated = speedData.Estimated.addPoint(x_k1(5), x_k1(6), x_k1(7));
    accData.Estimated = accData.Estimated.addPoint(ze_k1(1), ze_k1(2), ze_k1(3));
    magData.Estimated = magData.Estimated.addPoint(ze_k1(4), ze_k1(5), ze_k1(6));
end
