classdef Gyrometer
    %IMU Summary of this class goes here
    %   bablabla
    
    properties
        p %omega x
        q %omega y
        r %omega z
        totalPoints
    end
    
    methods
        function obj = Gyrometer(p, q, r)
            %IMU Construct an instance of this class
            %   Detailed explanation goes here
            obj.p = p;
            obj.q = q;
            obj.r = r;
            obj.totalPoints = length(p);
        end
        
        function obj = addPoint(obj,p,q,r)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            obj.totalPoints = obj.totalPoints+1;
            obj.p(obj.totalPoints) = p;
            obj.q(obj.totalPoints) = q;
            obj.r(obj.totalPoints) = r;
        end
        
        function plotData(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            figure()
            hold on;
            plot(obj.p);
            hold on;
            plot(obj.q);
            hold on;
            plot(obj.r);
            legend('p = \omega_x', 'q = \omega_y ', 'r = \omega_z');
            grid on;
        end
    end
end

