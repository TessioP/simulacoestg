%% Ensaio de calibra��o do aceler�metro

%% Leitura dos dados da AHRS
% 
fileName = '28082018-2ahrs.csv';

ahrs = readAHRSdata(fileName);

lineWidth = 2;
fontSize = 14;

colors = linspecer(3);

% plotNonCalibredAHRSGraphics(lineWidth, fontSize, ahrs);

interval.lower = [6000, 9750, 13400, 16700, 20500, 24000, 28000, 32000, 35300, 38500, 42300, 45500, 49100, 53000, 57000, 60263];
interval.upper = interval.lower + 1000;

totalExperiments = 16;
% 
%% Separa��o das matrizes

[G, Ac, Mag] = getAHRSExperimentalMatrices(ahrs, totalExperiments, interval);
% 
% Campo magn�tico em NEU atrav�s do Modelo WMM2015
% data: 02/05/18
% mag = [13599.4; -4583.6; 10607.0]*10^(-9); %T
%data: 28/08/18
mag = [13584.6; -4583.8; 10622.0]*10^(-9);%T
m.N = mag(1);
m.E = mag(2);
m.U = mag(3);
f0 = [   m.U  m.E  m.N;
        -m.U  m.E -m.N;
         m.U -m.N  m.E;
        -m.U  m.N  m.E;
        
         m.U -m.E -m.N;
        -m.U -m.E  m.N;
         m.U  m.N -m.E;
        -m.U -m.N -m.E;
    
         m.N  m.U  m.E;
         m.N -m.U -m.E;
        -m.N  m.U -m.E;
        -m.N -m.U  m.E;
        
        -m.N  m.E  m.U;
         m.N  m.E -m.U;
         m.N -m.E  m.U;
        -m.N -m.E -m.U;];

f0(:, 1) = -f0(:,1);
f0(:, 2) = -f0(:,2);

[Mag_align, mag_bias, mag_Sf] = magnetometerCalibration(0, Mag, f0);

clear f0;
f0 = -9.8*[ 1   0   0;
      -1   0   0;
       1   0   0;
      -1   0   0;
      
       1   0   0;
      -1   0   0;
       1   0   0;
      -1   0   0;
      
       0   1   0;
       0  -1   0;
       0   1   0;
       0  -1   0;
       
       0   0   1;
       0   0  -1;
       0   0   1;
       0   0  -1];

% f0(:, 1) = -f0(:,1);
% f0(:, 2) = -f0(:,2);
   
[Acc_align, acc_bias, acc_Sf] = accelerometerCalibration(0, Ac, f0);

accAux = Accelerometer(ahrs.ax, ahrs.ay, ahrs.az);

for i=1:length(accAux.ax)
    Aaux = Acc_align*(acc_Sf*[accAux.ax(i);accAux.ay(i);accAux.az(i)]-acc_bias);
    if i~=1
        accDataCalibrated = accDataCalibrated.addPoint(Aaux(1), Aaux(2), Aaux(3));
    else
        accDataCalibrated = Accelerometer(Aaux(1), Aaux(2), Aaux(3));
    end
end
accDataCalibrated.plotData();


magAux = Magnetometer(ahrs.mx, ahrs.my, ahrs.mz);

for i=1:length(magAux.mx)
    Maux = Mag_align*(mag_Sf*[magAux.mx(i);magAux.my(i);magAux.mz(i)]-mag_bias);
    if i~=1
        magDataCalibrated = magDataCalibrated.addPoint(Maux(1)/norm(Maux), Maux(2)/norm(Maux), Maux(3)/norm(Maux));
    else
        magDataCalibrated = Magnetometer(Maux(1)/norm(Maux), Maux(2)/norm(Maux), Maux(3)/norm(Maux));
    end
end
magDataCalibrated.plotData();
figure()
plot3(magDataCalibrated.mx, magDataCalibrated.my, magDataCalibrated.mz, '.');
axis equal;
grid on

%% Plotting graphics

% colors = linspecer(3);
% 
% figure()
% plot(ahrs.times, magDataCalibrated.ax, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
% hold on;
% plot(ahrs.times, magDataCalibrated.ay, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
% hold on;
% plot(ahrs.times, magDataCalibrated.az, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
% legend('m_x','m_y','m_z')
% xlabel('Tempo [s] ', 'FontSize', fontSize)
% ylabel('Acelera��o [g]', 'FontSize', fontSize)
% grid on
% % title('Acc AHRS');
% print('-depsc', sprintf('ACC-accCalibradoSTIM.eps'));
% % movefile('ACC-accCalibradoSTIM.eps','C:\ITA\tg\Cap3');
% xlim([30 55])
% ylim([0.98, 1.02])
% print('-depsc', sprintf('ACC-accCalibradoSTIMPatamar.eps'));
% % movefile('ACC-accCalibradoSTIMPatamar.eps','C:\ITA\tg\Cap3');
% xlim([30 55])
% ylim([-0.03, 0.03])
% print('-depsc', sprintf('ACC-accCalibradoSTIMBias.eps'));
% % movefile('ACC-accCalibradoSTIMBias.eps','C:\ITA\tg\Cap3');

%% Leitura dos dados da STIM

fileName = '28082018-2stim.csv';

stim = readSTIMdata(fileName);

lineWidth = 2;

fontSize = 14;

% plotNonCalibredSTIMGraphics(lineWidth, fontSize, stim);

%% Divis�o dos ensaios

interval.lower = [13750, 25000, 33650, 43200, 52000, 62000, 70100, 79000, 88500, 97000, 107000, 115350, 125000, 132500, 142000, 151750];
interval.upper = interval.lower + 1000;

totalExperiments = 16;
% 
%% Separa��o das matrizes

[G, Ac] = getStimExperimentalMatrices(stim, totalExperiments, interval);

clear f0;
f0 = -9.8*[ 1   0   0;
      -1   0   0;
       1   0   0;
      -1   0   0;
      
       1   0   0;
      -1   0   0;
       1   0   0;
      -1   0   0;
      
       0   1   0;
       0  -1   0;
       0   1   0;
       0  -1   0;
       
       0   0   1;
       0   0  -1;
       0   0   1;
       0   0  -1];

[M_align_stim_acc, bias_stim_acc, Sf_stim_acc] = accelerometerCalibration(0, Ac, f0);


%% Applying calibration
accAux = Accelerometer(stim.ax, stim.ay, stim.az);

for i=1:length(accAux.ax)
    Aaux = M_align_stim_acc*(Sf_stim_acc*[accAux.ax(i);accAux.ay(i);accAux.az(i)]-bias_stim_acc);
    if i~=1
        accDataCalibrated = accDataCalibrated.addPoint(Aaux(1), Aaux(2), Aaux(3));
    else
        accDataCalibrated = Accelerometer(Aaux(1), Aaux(2), Aaux(3));
    end
end
accDataCalibrated.plotData();

% %% Plotting graphics
% 
% colors = linspecer(3);
% 
% figure()
% plot(stim.times, accDataCalibrated.ax, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
% hold on;
% plot(stim.times, accDataCalibrated.ay, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
% hold on;
% plot(stim.times, accDataCalibrated.az, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
% legend('a_x','a_y','a_z')
% xlabel('Tempo [s] ', 'FontSize', fontSize)
% ylabel('Acelera��o [g]', 'FontSize', fontSize)
% grid on
% % title('Acc AHRS');
% print('-depsc', sprintf('ACC-accCalibradoSTIM.eps'));
% % movefile('ACC-accCalibradoSTIM.eps','C:\ITA\tg\Cap3');
% xlim([30 55])
% ylim([0.98, 1.02])
% print('-depsc', sprintf('ACC-accCalibradoSTIMPatamar.eps'));
% % movefile('ACC-accCalibradoSTIMPatamar.eps','C:\ITA\tg\Cap3');
% xlim([30 55])
% ylim([-0.03, 0.03])
% print('-depsc', sprintf('ACC-accCalibradoSTIMBias.eps'));
% % movefile('ACC-accCalibradoSTIMBias.eps','C:\ITA\tg\Cap3');
