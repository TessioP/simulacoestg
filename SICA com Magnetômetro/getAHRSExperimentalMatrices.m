function [G, Ac, Mag] = getAHRSExperimentalMatrices(ahrs, totalExperiments, interval)
%GETEXPERIMENTALMATRICES Summary of this function goes here
%   Detailed explanation goes here

for k = 1:totalExperiments
    accDataAux = [ahrs.ax(interval.lower(k):(interval.upper(k)))', ahrs.ay(interval.lower(k):(interval.upper(k)))', ahrs.az(interval.lower(k):(interval.upper(k)))'];
    accData(k) = Accelerometer(accDataAux(:,1), accDataAux(:,2), accDataAux(:,3));
    gyroDataAux = [ahrs.wx(interval.lower(k):(interval.upper(k)))' ahrs.wy(interval.lower(k):(interval.upper(k)))', ahrs.wz(interval.lower(k):(interval.upper(k)))'];
    gyroData(k) = Gyrometer(gyroDataAux(:,1), gyroDataAux(:,2), gyroDataAux(:,3));
    magDataAux = [ahrs.mx(interval.lower(k):(interval.upper(k)))' ahrs.my(interval.lower(k):(interval.upper(k)))', ahrs.mz(interval.lower(k):(interval.upper(k)))'];
    magData(k) = Magnetometer(magDataAux(:,1), magDataAux(:,2), magDataAux(:,3));
%     accData(k).plotData();
%     gyroData(k).plotData();
%     magData(k).plotData();
end

for k=1:totalExperiments
    G(:, 3*k-2:3*k) = [gyroData(k).p, gyroData(k).q, gyroData(k).r];
    Ac(:, 3*k-2:3*k) = [accData(k).ax, accData(k).ay, accData(k).az];
    Mag(:, 3*k-2:3*k) = [magData(k).mx, magData(k).my, magData(k).mz];
end

end

