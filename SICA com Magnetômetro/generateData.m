function [speedData, accData, magData, quaternions] = generateData(times, gyroNoise, initialAcc, accNoise, initialMag, magNoise)
%GENERATESENSORDATA Summary of this function goes here
%   Detailed explanation goes here

step = times(2) - times(1);

%% Criando a struct imuData que cont�m dois objetos imu: um para os valores reais e outro para os valores medidos
speedData.Real = Gyrometer(0.2*sin(times), 0.2*cos(times), -0.2*(sin(times).*cos(times)));

% Auxiliar para inserir ruido no sinal real
auxMeasured = [speedData.Real.p(1), speedData.Real.q(1), speedData.Real.r(1)]+(gyroNoise)*randn(3,1);

speedData.Measured = Gyrometer(auxMeasured(1), auxMeasured(2), auxMeasured(3));

%% Criando a struct quaternions que cont�m dois objetos quaternions: um para os valores reais e outro para medidos
quaternions.Real = Quaternion(0, 0, 0, 1);

%% Criando a struct acc que cont�m dois objetos Acc: um para os valores reais e outro para medidos

% Auxiliares para criar os dados de acelera��o
auxReal = quaternions.Real.DCM(:,:,1)*initialAcc;
auxMeasured = auxReal + (accNoise)*randn(3,1);

accData.Real = Accelerometer(auxReal(1), auxReal(2), auxReal(3));
accData.Measured = Accelerometer(auxMeasured(1), auxMeasured(2), auxMeasured(3));

auxReal = quaternions.Real.DCM(:,:,1)*initialMag;
auxMeasured = auxReal + (magNoise)*randn(3,1);
magData.Real = Magnetometer(auxReal(1), auxReal(2), auxReal(3));
magData.Measured = Magnetometer(auxMeasured(1), auxMeasured(2), auxMeasured(3));

%% Preenchendo os vetores de simula��o
    for k=1:length(times)-1
        % Vari�veis auxiliares para c�lculo da propaga��o do quat�rnio
        p = speedData.Real.p(k+1);
        q = speedData.Real.q(k+1);
        r = speedData.Real.r(k+1);
        
        quaternions.Real = quaternions.Real.addPointFromPQR(p,q,r,step);
        
        % Vari�veis auxiliares para c�lculo das componentes de acelera��o
        auxReal = quaternions.Real.DCM(:,:,k+1)*initialAcc;
        auxMeasured = auxReal + (accNoise)*randn(3,1);
        
        accData.Real = accData.Real.addPoint(auxReal(1), auxReal(2), auxReal(3));
        accData.Measured = accData.Measured.addPoint(auxMeasured(1), auxMeasured(2), auxMeasured(3));

        auxReal = quaternions.Real.DCM(:,:,k+1)*initialMag;
        auxMeasured = auxReal + (magNoise)*randn(3,1);
        
        magData.Real = magData.Real.addPoint(auxReal(1), auxReal(2), auxReal(3));
        magData.Measured = magData.Measured.addPoint(auxMeasured(1), auxMeasured(2), auxMeasured(3));
        
        % Inser��o do erro de medida nas velocidades
        auxMeasured = [p q r]' + (gyroNoise)*randn(3,1);
        speedData.Measured = speedData.Measured.addPoint(auxMeasured(1), auxMeasured(2), auxMeasured(3));
    end

end

