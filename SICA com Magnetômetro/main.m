close all;
% clear accData speedData quaternions;
clear all;
%% Par�metros de tempo
tic
clc;

step = 1e-2;

simulationTime = 3;

times = 0:step:simulationTime;
toc
%% Ru�dos para a simula��o da medi��o
clc;

sigma_acc = 0.2;

sigma_gyro = 0.2;

sigma_mag = 0.2;

accNoise = eye(3)*sigma_acc^2; %covari�ncia do ru�do de medidas dos gir�metros

gyroNoise = eye(3)*sigma_gyro^2;

magNoise = eye(3)*sigma_mag^2;
toc
%% Ru�dos utilizados no filtro
clc;

R = eye(6)*sigma_acc^2;

Q = eye(3)*sigma_gyro^2;
toc
%% Leitura inicial
clc;

initialAcc = [-9.8 0 0]';

initialMag = [0 ; 5/13; 12/13];
toc
%% Condi��es iniciais do sistema
clc;

P(:,:,1)=0.01*eye(4); % Covari�ncia do erro de estima��o no instante zero

%% Condi��es iniciais do filtro
clc;

%% Simula��o das medidas
[speedData, accData, magData, quaternions] = generateData(times, gyroNoise, initialAcc, accNoise, initialMag, magNoise);

quaternions.Estimated = Quaternion(0.1,0.1,0.1,sqrt(1-3*0.1^2));

speedData.Estimated = Gyrometer(speedData.Measured.p(1), speedData.Measured.q(1), speedData.Measured.r(1));

accData.Estimated = Accelerometer(accData.Measured.ax(1), accData.Measured.ay(1), accData.Measured.az(1));

magData.Estimated = Magnetometer(magData.Measured.mx(1), magData.Measured.my(1),magData.Measured.mz(1));
%% Filtro de Kalman
clc;
for k=1:length(times)-1
    q_k = [quaternions.Estimated.q1(k); quaternions.Estimated.q2(k); quaternions.Estimated.q3(k); quaternions.Estimated.q4(k)];
    u_k = [speedData.Measured.p(k); speedData.Measured.q(k); speedData.Measured.r(k)];
    x_k = [q_k; u_k];
    zMedido_k1 = [accData.Measured.ax(k+1); accData.Measured.ay(k+1); accData.Measured.az(k+1); magData.Measured.mx(k+1); magData.Measured.my(k+1); magData.Measured.mz(k+1)];
%     [x_k1, P_k1, ze_k1, z_k1] = extendedKalmanFilter(x_k, P_k, u_k, zMedido_k1, Q, R_k1, initialAcc, initialMag, dt)
    [x_k1, P(:,:,k+1), ze_k1, z_k1] = extendedKalmanFilter(x_k, P(:,:,k), u_k, zMedido_k1, Q, R, initialAcc, initialMag, step);
    quaternions.Estimated = quaternions.Estimated.addPointFromQuaternion(x_k1(1), x_k1(2), x_k1(3), x_k1(4));
%     speedData.Estimated = speedData.Estimated.addPoint(x_k1(5), x_k1(6), x_k1(7));
    accData.Estimated = accData.Estimated.addPoint(ze_k1(1), ze_k1(2), ze_k1(3));
    magData.Estimated = magData.Estimated.addPoint(ze_k1(4), ze_k1(5), ze_k1(6));
end


%% Gr�ficos dos quaternions
close all;
% for k = 1:20:length(times)
%     q = [quaternions.Estimated.q1(k), quaternions.Estimated.q2(k), quaternions.Estimated.q3(k), quaternions.Estimated.q4(k)];
%     plot_frame(q);
% end

for k = 1:length(times)
    qReal = [quaternions.Real.q1(k), quaternions.Real.q2(k), quaternions.Real.q3(k), quaternions.Real.q4(k)];
    qEstimated = [quaternions.Estimated.q1(k), quaternions.Estimated.q2(k), quaternions.Estimated.q3(k), quaternions.Estimated.q4(k)];
    eulerEstimated(:, k) = (quaternion2Euler(qEstimated));
    eulerReal(:, k) = (quaternion2Euler(qReal));
end

lineWidth = 2;
fontSize = 14;
colors = linspecer(2);
figure()
plot(times, eulerReal(1,:),'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, eulerEstimated(1,:), '--' ,'Color', colors(2,:), 'LineWidth', lineWidth);
legend({'\theta_{Real}','\theta_{Estimado}'}, 'FontSize', fontSize);
grid on
% title('Arfagem \theta (Pitch)')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Arfagem \theta [Graus]', 'FontSize', fontSize)
print('-depsc', sprintf('QEKFSimulTheta.eps'));
movefile('QEKFSimulTheta.eps','C:\ITA\tg\Cap3');

figure()
plot(times, eulerReal(2,:), 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, eulerEstimated(2,:), '--' , 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\psi_{Real}','\psi_{Estimado}'}, 'FontSize', fontSize);
grid on
% title('Guinada \psi (Yaw)')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Guinada \psi [Graus]', 'FontSize', fontSize)
print('-depsc', sprintf('QEKFSimulPsi.eps'));
movefile('QEKFSimulPsi.eps','C:\ITA\tg\Cap3');

figure()
plot(times, eulerReal(3,:), 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, eulerEstimated(3,:), '--' , 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\phi_{Real}','\phi_{Estimado}'}, 'FontSize', fontSize);
grid on
% title('Rolamento \phi (Roll)')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Rolamento \phi [Graus]', 'FontSize', fontSize)
print('-depsc', sprintf('QEKFSimulPhi.eps'));
movefile('QEKFSimulPhi.eps','C:\ITA\tg\Cap3');
