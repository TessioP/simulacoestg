function [R, Q, biasGyro, noises] = calibration(mag, acc, gyro)
%CALIBRATION Estima os ru�dos dos sensores da IMU e o bias do gir�metro
    mx = mag(:,1);
    my = mag(:,2);
    mz = mag(:,3);
    ax = acc(:,1);
    ay = acc(:,2);
    az = acc(:,3);
    wx = gyro(:,1);
    wy = gyro(:,2);
    wz = gyro(:,3);
    plot(wx)
    noises.mag = diag([std(mx)^2, std(my)^2, std(mz)^2]);
    noises.acc = diag([std(ax)^2, std(ay)^2, std(az)^2]);
    noises.gyro = diag([std(wx)^2, std(wy)^2, std(wz)^2]);
    R = [noises.acc, zeros(3,3); zeros(3,3), noises.gyro];
    Q = noises.gyro;
    biasGyro.x = mean(wx);
    biasGyro.y = mean(wy);
    biasGyro.z = mean(wz);
end

