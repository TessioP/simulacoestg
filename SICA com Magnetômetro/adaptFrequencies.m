function [stim_nav_out, ahrs_nav_out, R, Q, speedData, accData, magData] = adaptFrequencies(stim_nav, ahrs_nav, R_init, Q_init, gyroDataCalibrated, accDataCalibrated, magDataCalibrated)

times = 0:(1/500):stim_nav.times(end);

stim_nav_out.times(1) = 0;
stim_nav_out.wx(1) = stim_nav.wx(1);
stim_nav_out.wy(1) = stim_nav.wy(1);
stim_nav_out.wz(1) = stim_nav.wz(1);
stim_nav_out.ax(1) = stim_nav.ax(1);
stim_nav_out.ay(1) = stim_nav.ay(1);
stim_nav_out.az(1) = stim_nav.az(1);

ahrs_nav_out.times(1) = 0;
ahrs_nav_out.wx(1) = ahrs_nav.wx(1);
ahrs_nav_out.wy(1) = ahrs_nav.wy(1);
ahrs_nav_out.wz(1) = ahrs_nav.wz(1);
ahrs_nav_out.ax(1) = ahrs_nav.ax(1);
ahrs_nav_out.ay(1) = ahrs_nav.ay(1);
ahrs_nav_out.az(1) = ahrs_nav.az(1);
ahrs_nav_out.mx(1) = ahrs_nav.mx(1);
ahrs_nav_out.my(1) = ahrs_nav.my(1);
ahrs_nav_out.mz(1) = ahrs_nav.mz(1);
ahrs_nav_out.heading(1) = ahrs_nav.heading(1);
ahrs_nav_out.pitch(1) = ahrs_nav.pitch(1);
ahrs_nav_out.roll(1) = ahrs_nav.roll(1);

R(:,:,1) = R_init;
Q(:,:,1) = Q_init;

speedData.Measured = Gyrometer(gyroDataCalibrated.p(1), gyroDataCalibrated.q(1), gyroDataCalibrated.r(1));
accData.Measured = Accelerometer(accDataCalibrated.ax(1), accDataCalibrated.ay(1), accDataCalibrated.az(1));
magData.Measured = Magnetometer(magDataCalibrated.mx(1), magDataCalibrated.my(1), magDataCalibrated.mz(1));

stim_counter = 1;
ahrs_counter = 1;
for k=2:length(times)
    stim_nav_out.times(k) = times(k);
    ahrs_nav_out.times(k) = times(k);
    R(:,:,k) = R_init;
    if(times(k) == stim_nav.times(stim_counter+1))
        stim_counter = stim_counter + 1;
    else
        R(1:3,1:3,k) = 1e6*eye(3);
        Q(:,:,k) = 1e6*eye(3);
    end
    
    if(times(k) == ahrs_nav.times(ahrs_counter+1))
        ahrs_counter = ahrs_counter + 1;
    else
        R(4:6,4:6,k) = 1e6*eye(3);
    end
    
    stim_nav_out.wx(k) = stim_nav.wx(stim_counter);
    stim_nav_out.wy(k) = stim_nav.wy(stim_counter);
    stim_nav_out.wz(k) = stim_nav.wz(stim_counter);
    stim_nav_out.ax(k) = stim_nav.ax(stim_counter);
    stim_nav_out.ay(k) = stim_nav.ay(stim_counter);
    stim_nav_out.az(k) = stim_nav.az(stim_counter);
    speedData.Measured = speedData.Measured.addPoint(gyroDataCalibrated.p(stim_counter), gyroDataCalibrated.q(stim_counter), gyroDataCalibrated.r(stim_counter));
    accData.Measured = accData.Measured.addPoint(accDataCalibrated.ax(stim_counter), accDataCalibrated.ay(stim_counter), accDataCalibrated.az(stim_counter));
    
    ahrs_nav_out.wx(k) = ahrs_nav.wx(ahrs_counter);
    ahrs_nav_out.wy(k) = ahrs_nav.wy(ahrs_counter);
    ahrs_nav_out.wz(k) = ahrs_nav.wz(ahrs_counter);
    ahrs_nav_out.ax(k) = ahrs_nav.ax(ahrs_counter);
    ahrs_nav_out.ay(k) = ahrs_nav.ay(ahrs_counter);
    ahrs_nav_out.az(k) = ahrs_nav.az(ahrs_counter);
    ahrs_nav_out.mx(k) = ahrs_nav.mx(ahrs_counter);
    ahrs_nav_out.my(k) = ahrs_nav.my(ahrs_counter);
    ahrs_nav_out.mz(k) = ahrs_nav.mz(ahrs_counter);
    ahrs_nav_out.heading(k) = ahrs_nav.heading(ahrs_counter);
    ahrs_nav_out.pitch(k) = ahrs_nav.pitch(ahrs_counter);
    ahrs_nav_out.roll(k) = ahrs_nav.roll(ahrs_counter);
    magData.Measured = magData.Measured.addPoint(magDataCalibrated.mx(ahrs_counter), magDataCalibrated.my(ahrs_counter), magDataCalibrated.mz(ahrs_counter));

end
    
end