function euler = quaternion2Euler(q)
%QUATERNION2EULER Summary of this function goes here
%   Detailed explanation goes here
D = quaternion2DCM(q);

euler = [0 0 0]'; %theta psi phi
euler(1) = -(180/pi)*atan2(D(1,3), D(1,1));
euler(2) = +(180/pi)*asin(D(1,2));
euler(3) = -(180/pi)*atan2(D(3,2), D(2,2));
end

