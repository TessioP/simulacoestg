%% Dados de calibra��o obtidos pelos outros script
Mag_align = [ 1.0232   -0.2441    0.0873;
              0.0426    1.0002   -0.0832;
             -0.0455    0.0760   -1.0127];
mag_Sf =  1.0e-08*[ 0.3564         0         0;
                         0    0.3025         0;
                         0         0    0.2477];
mag_bias = 1.0e-04*[-0.0161; 0.0321; 0.1082];

M_align_stim_acc = [    1.0000    0.0032    0.0024;
                       -0.0028    1.0000   -0.0005;
                       -0.0039   -0.0002    1.0000];
    
Sf_stim_acc = [9.8227         0         0;
                    0    9.8227         0;
                    0         0    9.8220];
                
bias_stim_acc = [ -0.0170; 0.0452; -0.0568];

Htil_stim_gyros =[ 1.0000    0.0045   -0.0071;
                  -0.0040    1.0000    0.0002;
                   0.0085   -0.0004    1.0000];

FE_stim_gyros = [ 0.0174         0         0;
                       0    0.0174         0;
                       0         0    0.0174];
                   
bias_stim_gyros = [   -0.0006; 0.0011; -0.0000];
%% Leitura e calibra��o dos dados de navega��o da ahrs

fileName = '28082018ahrs_nav.csv';
% fileName = '28082018ahrs_giros.csv';

ahrs_nav = readAHRSdata(fileName);

magAux = Magnetometer(ahrs_nav.mx, ahrs_nav.my, ahrs_nav.mz);

for i=1:length(magAux.mx)
    Maux = Mag_align*(mag_Sf*[magAux.mx(i);magAux.my(i);magAux.mz(i)]-mag_bias);
    if i~=1
        magDataCalibrated = magDataCalibrated.addPoint(Maux(1)/norm(Maux), Maux(2)/norm(Maux), Maux(3)/norm(Maux));
    else
        magDataCalibrated = Magnetometer(Maux(1)/norm(Maux), Maux(2)/norm(Maux), Maux(3)/norm(Maux));
    end
end

%% Plot dos gr�ficos do mag

% magDataCalibrated.plotData();
figure()
plot3(magDataCalibrated.mx, magDataCalibrated.my, magDataCalibrated.mz, '.');
axis equal;
grid on
print('-dpng', sprintf('mag_calibrado_ahrs_nav.eps'));

nome = 'Mag XY';
figure('Name',nome)
plot(magDataCalibrated.my,magDataCalibrated.mx, '.'); axis equal
title(nome)
xlabel('Y'); ylabel('X');
grid on
nome = 'Mag YX';
print('-dpng', sprintf('magXY_calibrado_ahrs_nav.eps'));

figure('Name',nome)
plot(magDataCalibrated.mz,magDataCalibrated.mx, '.'); axis equal
title(nome)
xlabel('Z'); ylabel('X');
grid on
nome = 'Mag ZX';
print('-dpng', sprintf('magZX_calibrado_ahrs_nav.eps'));

figure('Name',nome)
plot(magDataCalibrated.mz,magDataCalibrated.my, '.'); axis equal
title(nome)
xlabel('Z'); ylabel('Y');
grid on
nome = 'Mag ZY';
print('-dpng', sprintf('magZY_calibrado_ahrs_nav.eps'));

%% Leitura e calibra��o dos dados de navega��o da STIM

fileName = '28082018stim_nav.csv';

stim_nav = readSTIMdata(fileName);

lineWidth = 2;

fontSize = 14;

% plotNonCalibredSTIMGraphics(lineWidth, fontSize, stim_nav);

accAux = Accelerometer(stim_nav.ax, stim_nav.ay, stim_nav.az);

for i=1:length(accAux.ax)
    Aaux = M_align_stim_acc*(Sf_stim_acc*[accAux.ax(i);accAux.ay(i);accAux.az(i)]-bias_stim_acc);
    if i~=1
        accDataCalibrated = accDataCalibrated.addPoint(Aaux(1), Aaux(2), Aaux(3));
    else
        accDataCalibrated = Accelerometer(Aaux(1), Aaux(2), Aaux(3));
    end
end
accDataCalibrated.plotData();

gyroAux = Gyrometer(stim_nav.wx, stim_nav.wy, stim_nav.wz);

for i=1:length(gyroAux.p)
    Aaux = Htil_stim_gyros\(FE_stim_gyros*[gyroAux.p(i);gyroAux.q(i);gyroAux.r(i)]-bias_stim_gyros);
    if i~=1
        gyroDataCalibrated = gyroDataCalibrated.addPoint(Aaux(1), Aaux(2), Aaux(3));
    else
        gyroDataCalibrated = Gyrometer(Aaux(1), Aaux(2), Aaux(3));
    end
end
gyroDataCalibrated.plotData();


%% Ruídos para a simulação da medição
gyroNoise = diag([std(gyroDataCalibrated.p(1:500))^2,std(gyroDataCalibrated.q(1:500))^2,std(gyroDataCalibrated.r(1:500))^2]); %covari�ncia do ru�do de medidas dos gir�metros

accNoise = diag([std(accDataCalibrated.ax(1:500))^2,std(accDataCalibrated.ay(1:500))^2,std(accDataCalibrated.az(1:500))^2]);

magNoise = diag([std(magDataCalibrated.mx(1:500))^2,std(magDataCalibrated.my(1:500))^2,std(magDataCalibrated.mz(1:500))^2]);
%% Ru�dos utilizados no filtro
R_init = diag([std(accDataCalibrated.ax(1:500))^2,std(accDataCalibrated.ay(1:500))^2,std(accDataCalibrated.az(1:500))^2, std(magDataCalibrated.mx(1:500))^2,std(magDataCalibrated.my(1:500))^2,std(magDataCalibrated.mz(1:500))^2]);

Q_init = diag([std(gyroDataCalibrated.p(1:500))^2,std(gyroDataCalibrated.q(1:500))^2,std(gyroDataCalibrated.r(1:500))^2]);

%% Leitura inicial
initialAcc = [accDataCalibrated.ax(1) accDataCalibrated.ay(1) accDataCalibrated.az(1)]';

initialMag = [magDataCalibrated.mx(1) ; magDataCalibrated.my(1); magDataCalibrated.mz(1)];

%% Condi��es iniciais do sistema
P(:,:,1)=0.01*eye(4); % Covari�ncia do erro de estima��o no instante zero

%% Condi��es iniciais do filtro

[stim_nav_out, ahrs_nav_out, R, Q, speedData, accData, magData] = adaptFrequencies(stim_nav, ahrs_nav, R_init, Q_init, gyroDataCalibrated, accDataCalibrated, magDataCalibrated);

% In�cio: X South; Y East; Z Up;

quaternions.Estimated = Quaternion(0,0,0,1);

speedData.Estimated = Gyrometer(speedData.Measured.p(1), speedData.Measured.q(1), speedData.Measured.r(1));

accData.Estimated = Accelerometer(accData.Measured.ax(1), accData.Measured.ay(1), accData.Measured.az(1));

magData.Estimated = Magnetometer(magData.Measured.mx(1), magData.Measured.my(1),magData.Measured.mz(1));

disp("done")
%% Filtro de Kalman
clc; close all;
step = stim_nav_out.times(2) - stim_nav_out.times(1);
timesLength = length(stim_nav_out.times)/5;
for k=1:(timesLength-1)
    q_k = [quaternions.Estimated.q1(k); quaternions.Estimated.q2(k); quaternions.Estimated.q3(k); quaternions.Estimated.q4(k)];
    u_k = [speedData.Measured.p(k); speedData.Measured.q(k); speedData.Measured.r(k)];
    x_k = [q_k; u_k];
    zMedido_k1 = [accData.Measured.ax(k+1); accData.Measured.ay(k+1); accData.Measured.az(k+1); magData.Measured.mx(k+1); magData.Measured.my(k+1); magData.Measured.mz(k+1)];
    [x_k1, P(:,:,k+1), ze_k1, z_k1] = extendedKalmanFilter(x_k, P(:,:,k), u_k, zMedido_k1, Q(:,:,k+1), R(:,:,k+1), initialAcc, initialMag, step, 1);
    quaternions.Estimated = quaternions.Estimated.addPointFromQuaternion(x_k1(1), x_k1(2), x_k1(3), x_k1(4));
%     speedData.Estimated = speedData.Estimated.addPoint(x_k1(5), x_k1(6), x_k1(7));
    accData.Estimated = accData.Estimated.addPoint(ze_k1(1), ze_k1(2), ze_k1(3));
    magData.Estimated = magData.Estimated.addPoint(ze_k1(4), ze_k1(5), ze_k1(6));
end

accData.Estimated.plotData();
quaternions.Estimated.plotData();

%% Gr�ficos dos quaternions
for k = 1:timesLength
%     qReal = [quaternions.Real.q1(k), quaternions.Real.q2(k), quaternions.Real.q3(k), quaternions.Real.q4(k)];
    qEstimated = [quaternions.Estimated.q1(k), quaternions.Estimated.q2(k), quaternions.Estimated.q3(k), quaternions.Estimated.q4(k)];
    eulerEstimated(:, k) = (quaternion2Euler(qEstimated));
%     eulerReal(:, k) = (quaternion2Euler(qReal));
end

lineWidth = 2;
fontSize = 14;
colors = linspecer(2);
figure()
% plot(stim_nav.times(1:timesLength), eulerReal(1,:),'Color' ,colors(1,:), 'LineWidth', lineWidth);
% hold on;
plot(stim_nav.times(1:timesLength), eulerEstimated(1,:), '--' ,'Color', colors(2,:), 'LineWidth', lineWidth);
% legend({'\theta_{Real}','\theta_{Estimado}'}, 'FontSize', fontSize);
grid on
title('Arfagem \theta (Pitch)')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Arfagem \theta [Graus]', 'FontSize', fontSize)
% print('-depsc', sprintf('QEKFSimulTheta.eps'));
% movefile('QEKFSimulTheta.eps','C:\ITA\tg\Cap3');
% 
figure()
% plot(stim_nav.times(1:timesLength), eulerReal(2,:), 'Color' ,colors(1,:), 'LineWidth', lineWidth);
% hold on;
plot(stim_nav.times(1:timesLength), eulerEstimated(2,:), '--' , 'Color' ,colors(2,:), 'LineWidth', lineWidth);
% legend({'\psi_{Real}','\psi_{Estimado}'}, 'FontSize', fontSize);
grid on
title('Guinada \psi (Yaw)')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Guinada \psi [Graus]', 'FontSize', fontSize)
% print('-depsc', sprintf('QEKFSimulPsi.eps'));
% movefile('QEKFSimulPsi.eps','C:\ITA\tg\Cap3');

figure()
% plot(stim_nav.times(1:timesLength), eulerReal(3,:), 'Color' ,colors(1,:), 'LineWidth', lineWidth);
% hold on;
plot(stim_nav.times(1:timesLength), eulerEstimated(3,:), '--' , 'Color' ,colors(2,:), 'LineWidth', lineWidth);
% legend({'\phi_{Real}','\phi_{Estimado}'}, 'FontSize', fontSize);
grid on
title('Rolamento \phi (Roll)')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Rolamento \phi [Graus]', 'FontSize', fontSize)
% print('-depsc', sprintf('QEKFSimulPhi.eps'));
% movefile('QEKFSimulPhi.eps','C:\ITA\tg\Cap3');
