function [mag, acc, gyro, step] = analizeVectors(data, scale, bias)
%ANALIZEVECTORS Trata os dados brutos lidos da IMU
%   Esta fun��o aplica uma constante de corre��o nos dados da IMU, al�m de
%   retirar o bias. Por fim, normaliza os vetores do magnet�metro e do
%   aceler�metro.
    timeConstant = 1e6;
    mx = (data(1) - bias.mag.x)*scale.mag.x;
    my = (data(2) - bias.mag.y)*scale.mag.y;
    mz = (data(3) - bias.mag.z)*scale.mag.z;
    ax = data(4);
    ay = data(5);
    az = data(6);
    wx = (data(7)- bias.gyro.x)*scale.gyro;
    wy = (data(8)- bias.gyro.y)*scale.gyro;
    wz = (data(9)- bias.gyro.z)*scale.gyro;
    step = data(10)/timeConstant;
    mag = [mx, my, mz]/norm([mx, my, mz]);
    acc = [ax, ay, az]/norm([ax, ay, az]);
    gyro = [wx, wy, wz];
end