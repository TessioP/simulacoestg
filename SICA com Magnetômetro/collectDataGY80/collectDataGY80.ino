#include <Wire.h>
#include "SdFat.h"
SdFat SD;
#define Register_ID 0
#define Register_2D 0x2D
#define Register_X0 0x32
#define Register_X1 0x33
#define Register_Y0 0x34
#define Register_Y1 0x35
#define Register_Z0 0x36
#define Register_Z1 0x37
#define CTRL_REG1 0x20
#define CTRL_REG2 0x21
#define CTRL_REG3 0x22
#define CTRL_REG4 0x23
#define CTRL_REG5 0x24
#define MagAddress 0x1E //0011110b, I2C 7bit address of HMC5883HMC5883
int L3G4200D_Address = 105; //I2C address of the L3G4200D
int ADXAddress = 0xA7 >> 1;  // the default 7-bit slave address
int reading = 0;
int val=0;
int X0,X1,X_out;
int Y0,Y1,Y_out;
int Z1,Z0,Z_out;
double Xg,Yg,Zg;  //accel data
int wx, wy, wz;  //gyro data
//long Sumwx, Sumwy, Sumwz;
long t1,t2;     //time data
int mx,my,mz; //magnetometer data
long n = 0;
const int chipSelect = 4;
File dataFile;


void setup() {
  Wire.begin();
  SD.begin(chipSelect);
//  dataFile = SD.open("ensaio_SICA.txt", FILE_WRITE);
  digitalWrite(13,HIGH);
  Serial.begin(115200);
//  Serial.println("starting up L3G4200D");
  setupL3G4200D(250); // Configure L3G4200  - 250, 500 or 2000 deg/sec
  delay(1500); //wait for the sensor to be ready 
  
  // enable to measute g data
//  Serial.println("enabling ADXL345");
  Wire.beginTransmission(ADXAddress);
  Wire.write(Register_2D);
  Wire.write(8);                //measuring enable
  Wire.endTransmission();     // stop transmitting

  //Put the HMC5883 IC into the correct operating mode
//  Serial.println("configuring HMC5883");
  Wire.beginTransmission(MagAddress); //open communication with HMC5883
  Wire.write(0x02); //select mode register
  Wire.write(0x00); //continuous measurement mode
  Wire.endTransmission();

//  Sumwx = 0;
//  Sumwy = 0;
//  Sumwz = 0;

  t1 = micros();

  
}

void loop() {

//  n++;
//  Serial.println(n);
  //Tell the HMC5883 where to begin reading data
  Wire.beginTransmission(MagAddress);
  Wire.write(0x03); //select register 3, X MSB register
  Wire.endTransmission();
  
 
 //Read data from each axis, 2 registers per axis
  Wire.requestFrom(MagAddress, 6);
  if(6<=Wire.available()){
    mx = Wire.read()<<8; //X msb
    mx |= Wire.read(); //X lsb
    mz = Wire.read()<<8; //Z msb
    mz |= Wire.read(); //Z lsb
    my = Wire.read()<<8; //Y msb
    my |= Wire.read(); //Y lsb
  }

  //Serial.print("\t");
 // Serial.print("mx:");
  Serial.print(mx);Serial.print("\t");
//  dataFile.print(mx);dataFile.print("\t");
  //Serial.print("my:");
  Serial.print(my);Serial.print("\t");
//  dataFile.print(my);dataFile.print("\t");
 // Serial.print("mz:");
  Serial.print(mz);Serial.print("\t");
//  dataFile.print(mz);dataFile.print("\t");

  
  
  //--------------X
  Wire.beginTransmission(ADXAddress); // transmit to device
  Wire.write(Register_X0);
  Wire.write(Register_X1);
  Wire.endTransmission();
  Wire.requestFrom(ADXAddress,2); 
  if(Wire.available()<=2)   
  {
    X0 = Wire.read();
    X1 = Wire.read(); 
    X1=X1<<8;
    X_out=X0+X1;   
  }

  //------------------Y
  Wire.beginTransmission(ADXAddress); // transmit to device
  Wire.write(Register_Y0);
  Wire.write(Register_Y1);
  Wire.endTransmission();
  Wire.requestFrom(ADXAddress,2); 
  if(Wire.available()<=2)   
  {
    Y0 = Wire.read();
    Y1 = Wire.read(); 
    Y1=Y1<<8;
    Y_out=Y0+Y1;
  }
  //------------------Z
  Wire.beginTransmission(ADXAddress); // transmit to device
  Wire.write(Register_Z0);
  Wire.write(Register_Z1);
  Wire.endTransmission();
  Wire.requestFrom(ADXAddress,2); 
  if(Wire.available()<=2)   
  {
    Z0 = Wire.read();
    Z1 = Wire.read(); 
    Z1=Z1<<8;
    Z_out=Z0+Z1;
  }
  //
  Xg=X_out/256.0;
  Yg=Y_out/256.0;
  Zg=Z_out/256.0;


 // Serial.print("\t");
 // Serial.print("ax:"); 
  Serial.print(Xg);Serial.print("\t");
//  dataFile.print(Xg);dataFile.print("\t");
//  Serial.print("ay:");
  Serial.print(Yg);Serial.print("\t");
//  dataFile.print(Yg);dataFile.print("\t");
//  Serial.print("az:");
  Serial.print(Zg);Serial.print("\t");
//  dataFile.print(Zg);dataFile.print("\t");

  
  
  //delay(200);
  //Serial.println(Xg);
  //delay(200);

//  Sumwx += (long)wx;
//  Sumwy += (long)wy;
//  Sumwz += (long)wz;

  getGyroValues();  // This will update wx, wy, and wz with new values
  //Serial.print("\t\");
// Serial.print("wx:");
  Serial.print(wx);Serial.print("\t");
//  dataFile.print(wx);dataFile.print("\t");
//  Serial.print("wy:");
  Serial.print(wy);Serial.print("\t");
//  dataFile.print(wy);dataFile.print("\t");
//  Serial.print("wz:");
  Serial.print(wz);Serial.print("\t");
//  dataFile.print(wz);dataFile.print("\t");




 // t1 = micros();
//  Serial.print("dt:");
  t2 = micros();
  
  Serial.println(t2);
//  dataFile.println(t2);
  
//  if(n%10000 == 0){
//    n = 0;
//    dataFile.close();
//    Serial.println("fechou");
//    dataFile = SD.open("ensaio_SICA.txt", FILE_WRITE);
//  }
  
  t1 = t2;

  
  
  //delay(100); //Just here to slow down the serial to make it more readable


}




void getGyroValues(){

  byte xMSB = readRegister(L3G4200D_Address, 0x29);
  byte xLSB = readRegister(L3G4200D_Address, 0x28);
  wx = ((xMSB << 8) | xLSB);

  byte yMSB = readRegister(L3G4200D_Address, 0x2B);
  byte yLSB = readRegister(L3G4200D_Address, 0x2A);
  wy = ((yMSB << 8) | yLSB);

  byte zMSB = readRegister(L3G4200D_Address, 0x2D);
  byte zLSB = readRegister(L3G4200D_Address, 0x2C);
  wz = ((zMSB << 8) | zLSB);
}

int setupL3G4200D(int scale){
  //From  Jim Lindblom of Sparkfun's code

  // Enable wx, wy, wz and turn off power down:
  writeRegister(L3G4200D_Address, CTRL_REG1, 0b00001111);

  // If you'd like to adjust/use the HPF, you can edit the line below to configure CTRL_REG2:
  writeRegister(L3G4200D_Address, CTRL_REG2, 0b00000000);

  // Configure CTRL_REG3 to generate data ready interrupt on INT2
  // No interrupts used on INT1, if you'd like to configure INT1
  // or INT2 otherwise, consult the datasheet:
  writeRegister(L3G4200D_Address, CTRL_REG3, 0b00001000);

  // CTRL_REG4 controls the full-scale range, among other things:

  if(scale == 250){
    writeRegister(L3G4200D_Address, CTRL_REG4, 0b00000000);
  }else if(scale == 500){
    writeRegister(L3G4200D_Address, CTRL_REG4, 0b00010000);
  }else{
    writeRegister(L3G4200D_Address, CTRL_REG4, 0b00110000);
  }

  // CTRL_REG5 controls high-pass filtering of outputs, use it
  // if you'd like:
  writeRegister(L3G4200D_Address, CTRL_REG5, 0b00000000);
}

void writeRegister(int deviceAddress, byte address, byte val) {
    Wire.beginTransmission(deviceAddress); // start transmission to device 
    Wire.write(address);       // send register address
    Wire.write(val);         // send value to write
    Wire.endTransmission();     // end transmission
}

int readRegister(int deviceAddress, byte address){

    int v;
    Wire.beginTransmission(deviceAddress);
    Wire.write(address); // register to read
    Wire.endTransmission();

    Wire.requestFrom(deviceAddress, 1); // read a byte

    while(!Wire.available()) {
        // waiting
    }

    v = Wire.read();
    return v;
}
