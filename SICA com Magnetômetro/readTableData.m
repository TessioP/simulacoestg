%% Leitura dos dados da mesa
% 
fileName = '28082018_mesa.csv';

data = dlmread(fileName,';', 10,0);
table.times = data(:,1);
table.inner.theta = data(:,2);
table.inner.dtheta = data(:,3);
table.inner.ddtheta = data(:,4);
table.middle.theta = data(:,5);
table.middle.dtheta = data(:,6);
table.middle.ddtheta = data(:,7);
table.outer.theta = data(:,8);
table.outer.dtheta = data(:,9);
table.outer.ddtheta = data(:,10);

figure()
plot(table.times, table.inner.theta, table.times, table.middle.theta, table.times, table.outer.theta)
figure()
plot(table.times, table.inner.dtheta, table.times, table.middle.dtheta, table.times, table.outer.dtheta)
figure()
plot(table.times, table.inner.ddtheta, table.times, table.middle.ddtheta, table.times, table.outer.ddtheta)

% figure()
% plot(table.times, table.inner.theta)
% xlim([0 90])
% figure()
% plot(table.times, table.middle.theta)
% xlim([0 90])
% figure()
% plot(table.times, table.outer.theta)
% xlim([0 90])