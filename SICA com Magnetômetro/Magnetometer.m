classdef Magnetometer
    %Magnetometer: Classe que armazena os dados lidos pelo magnet�metro
    
    properties
        mx
        my
        mz
        totalPoints
    end
    
    methods
        function obj = Magnetometer(mx,my,mz)
            %Magnetometer: Constr�i uma inst�ncia da classe
            %   Pode receber tanto uma tr�ade individual como um conjunto
            %   de n tr�ades de componentes do campo magn�tico.
            obj.mx = mx;
            obj.my = my;
            obj.mz = mz;
            obj.totalPoints = length(mx);
        end
        
        function obj = addPoint(obj,mx,my,mz)
            %addPoint: Adiciona uma tr�ade de componentes do campo magn�tico na inst�ncia da
            %classe.
            obj.totalPoints = obj.totalPoints+1;
            obj.mx(obj.totalPoints) = mx;
            obj.my(obj.totalPoints) = my;
            obj.mz(obj.totalPoints) = mz;
        end
        function plotData(obj)
            %plotData Plota os gr�ficos das tr�s componentes de campo
            %magn�tico armazenadas na classe.
            figure()
            hold on;
            plot(obj.mx);
            hold on;
            plot(obj.my);
            hold on;
            plot(obj.mz);
            legend('m_x', 'm_y ', 'm_z');
            grid on;
        end
    end
end