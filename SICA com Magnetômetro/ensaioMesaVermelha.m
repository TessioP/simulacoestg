fileName = 'teste_mesa_vermelha_2.txt';

data = importdata(fileName);

%% Ensaio 01: 5000:9242

interval.lower(1) = 5000;
interval.upper(1) = 9242;
times.test1 = data(interval.lower(1):interval.upper(1), 10);

%% Ensaio 02: 9243:13410

interval.lower(2) = 9243;
interval.upper(2) = 13410;
times.test2 = data(interval.lower(2):interval.upper(2), 10);


%% Ensaio 03: 14440:18610

interval.lower(3) = 14440;
interval.upper(3) = 18610;
times.test3 = data(interval.lower(3):interval.upper(3), 10);

%% Ensaio 04: 18600:22780

interval.lower(4) = 18600;
interval.upper(4) = 22780;
times.test4 = data(interval.lower(4):interval.upper(4), 10);

%% Ensaio 05: 23450:27585

interval.lower(5) = 23450;
interval.upper(5) = 27585;
times.test5 = data(interval.lower(5):interval.upper(5), 10);

%% Ensaio 06: 27580:31710

interval.lower(6) = 27580;
interval.upper(6) = 31710;
times.test6 = data(interval.lower(6):interval.upper(6), 10);

%% Ensaio 07: 32740:36890

interval.lower(7) = 32740;
interval.upper(7) = 36890;
times.test7 = data(interval.lower(7):interval.upper(7), 10);

%% Ensaio 08: 36880:41065

interval.lower(8) = 36880;
interval.upper(8) = 41065;
times.test8 = data(interval.lower(8):interval.upper(8), 10);

%% Ensaio 09: 41740:45955

interval.lower(9) = 41740;
interval.upper(9) = 45955;
times.test9 = data(interval.lower(9):interval.upper(9), 10);

%% Ensaio 10: 45950:50080

interval.lower(10) = 45950;
interval.upper(10) = 50080;
times.test10 = data(interval.lower(10):interval.upper(10), 10);

%% Ensaio 11: 51090:55200

interval.lower(11) = 51090;
interval.upper(11) = 55200;
times.test11 = data(interval.lower(11):interval.upper(11), 10);

%% Ensaio 12: 55190:59350

interval.lower(12) = 55190;
interval.upper(12) = 59350;
times.test12 = data(interval.lower(12):interval.upper(12), 10);

for k = 1:12
    magDataAux = data(interval.lower(k):(interval.upper(k)),1:3);
    magData(k) = Magnetometer(magDataAux(:,1), magDataAux(:,2), magDataAux(:,3));
    accDataAux = data(interval.lower(k):(interval.upper(k)),4:6);
    accData(k) = Accelerometer(accDataAux(:,1), accDataAux(:,2), accDataAux(:,3));
    gyroDataAux = data(interval.lower(k):interval.upper(k),7:9);
    gyroData(k) = Gyrometer(gyroDataAux(:,1), gyroDataAux(:,2), gyroDataAux(:,3));
end

%% Gir�metro

% G(:, 1:3) = [gyroData(1).p(1400:3000),gyroData(1).q(1400:3000),gyroData(1).r(1400:3000)];
% G(:, 4:6) = [gyroData(2).p(1400:3000),gyroData(2).q(1400:3000),gyroData(2).r(1400:3000)];
% G(:, 7:9) = [gyroData(3).p(1400:3000),gyroData(3).q(1400:3000),gyroData(3).r(1400:3000)];

for k=1:12
    G(:, 3*k-2:3*k) = [gyroData(k).p(1400:3000),gyroData(k).q(1400:3000),gyroData(k).r(1400:3000)];
    M(:, 3*k-2:3*k) = [magData(k).mx(1400:3000),magData(k).my(1400:3000),magData(k).mz(1400:3000)];
end

w = [ 1  0  0;
     -1  0  0;
      1  0  0;
     -1  0  0;
      0  1  0;
      0 -1  0;
      0  1  0;
      0 -1  0;
      0  0  1;
      0  0 -1;
      0  0 -1;
      0  0  1];

omega = (30);

E = size(w,1);

Qg = size(w,2);

Gm = mean(G);

for j=1:(E/2)
    for i = 1:Qg
        Gbar(i,j) = Gm((j-1)*E/2 + i) - Gm((j-1)*E/2 + i + Qg);
    end
end

for j=1:(E/2)
    W(:,j) = omega*(w(2*j-1,:)'-w(2*j,:)');
end

Hsf = Gbar*pinv(W);

for i=1:Qg
    FE(i,i) = 1/norm(Hsf(i,:), 2);
end

Htil = FE*Hsf;

Hstar = inv(Htil);

for j = 1 : E
    for i = 1 : Qg
        Gv(i,j) = Gm((j-1)*Qg+i);
    end
end

b_til = mean(FE*Gv - Htil*omega*w',2);

b_dif = mean(FE*Gbar - Htil*W, 2);

bias = b_til;

gyroAux = Gyrometer(data(:,7), data(:,8), data(:,9));

%% Applying calibration

for i=1:length(gyroAux.p)
    Aaux = Htil\(FE*[gyroAux.p(i);gyroAux.q(i);gyroAux.r(i)]-bias);
    if i~=1
        gyroDataCalibrated = gyroDataCalibrated.addPoint(Aaux(1), Aaux(2), Aaux(3));
    else
        gyroDataCalibrated = Gyrometer(Aaux(1), Aaux(2), Aaux(3));
    end
end

%% Plot data

plot(data(:,7:9));
legend('p = \omega_x', 'q = \omega_y ', 'r = \omega_z');
grid on;
ylim([-4000 4000]);
xlim([0.4e4 5.6e4]);
gyroDataCalibrated.plotData();
ylim([-40 40]);
xlim([0.4e4 5.6e4]);

%%
mx = data(:,1);
my = data(:,2);
mz = data(:,3);

moduloMag = sqrt(mx.^2 + my.^2+mz.^2);
% 
% biasMx = (max(mx) + min(mx))/2;
% biasMy = (max(my) + min(my))/2;
% biasMz = (max(mz) + min(mz))/2;
% 
% scaleMx = (max(mx) - min(mx))/2;
% scaleMy = (max(my) - min(my))/2;
% scaleMz = (max(mz) - min(mz))/2;
% 
% scale = (scaleMx + scaleMy + scaleMz)/3;
% 
% scaleMx = scale/scaleMx;
% 
% scaleMy = scale/scaleMy;
% 
% scaleMz = scale/scaleMz;
% 
% newMx = mx*scaleMx - biasMx;
% 
% newMy = my*scaleMy - biasMy;
% 
% newMz = mz*scaleMz - biasMz;
% 
% close all;
% 
% figure()
% plot(newMx);
% hold on
% plot(mx);
% grid on;
% legend('m_x calibrado','m_x bruto')
% 
% figure()
% plot(newMy);
% hold on
% plot(my);
% grid on;
% legend('m_y calibrado','m_y bruto')
% 
% figure()
% plot(newMz);
% hold on
% plot(mz);
% grid on;
% legend('m_z calibrado','m_z bruto')
% 
% 
% figure()
% plot3(mx,my,mz,'.');
% hold on
% plot3(newMx, newMy, newMz,'.');
% grid on
% axis equal;