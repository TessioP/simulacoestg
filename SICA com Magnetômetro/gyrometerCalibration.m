function [Htil, FE, bias] = gyrometerCalibration(G)
%GYROMETERCALIBRATION Summary of this function goes here
%   Detailed explanation goes here
w = [ -1  0  0;
       1  0  0;
      -1  0  0;
       1  0  0
    
      0  1  0;
      0 -1  0;
      0  1  0;
      0 -1  0;
      
      0  0  -1;
      0  0   1;
      0  0  -1;
      0  0   1];

omega = deg2rad(30);

E = size(w,1);

Qg = size(w,2);

Gm = mean(G);

for j=1:(E/2)
    for i = 1:Qg
        Gbar(i,j) = Gm((j-1)*E/2 + i) - Gm((j-1)*E/2 + i + Qg);
    end
end

for j=1:(E/2)
    W(:,j) = omega*(w(2*j-1,:)'-w(2*j,:)');
end

Hsf = Gbar*pinv(W);

for i=1:Qg
    FE(i,i) = 1/norm(Hsf(i,:), 2);
end

Htil = FE*Hsf;

Hstar = inv(Htil);

for j = 1 : E
    for i = 1 : Qg
        Gv(i,j) = Gm((j-1)*Qg+i);
    end
end

b_til = mean(FE*Gv - Htil*omega*w',2);

b_dif = mean(FE*Gbar - Htil*W, 2);

bias = b_til;

end

