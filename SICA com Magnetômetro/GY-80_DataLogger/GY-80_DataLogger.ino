
// GY-80 IMU measurements
// Author: Roberto Brusnicki
// 08-May-2016

// libraries
#include "Wire.h"
#include "I2Cdev.h"
#include "HMC5883L.h"
#include "ADXL345.h"
#include "BMP085.h"
#include "L3G4200D.h"
#include <SPI.h>
#include "SdFat.h"
SdFat SD;
#include <Wire.h>
#define Register_ID 0
#define Register_2D 0x2D
#define Register_X0 0x32
#define Register_X1 0x33
#define Register_Y0 0x34
#define Register_Y1 0x35
#define Register_Z0 0x36
#define Register_Z1 0x37
#define CTRL_REG1 0x20
#define CTRL_REG2 0x21
#define CTRL_REG3 0x22
#define CTRL_REG4 0x23
#define CTRL_REG5 0x24
#define MagAddress 0x1E //0011110b, I2C 7bit address of HMC5883HMC5883
int L3G4200D_Address = 105; //I2C address of the L3G4200D
int ADXAddress = 0xA7 >> 1;  // the default 7-bit slave address
const int chipSelect = 4;
File dataFile;

// devices in gy-80
HMC5883L mag;
ADXL345  acc;
BMP085   bar;
L3G4200D gyr;



int16_t mx, my, mz, old_mx, old_my, old_mz;
int16_t ax, ay, az, old_ax, old_ay, old_az;
int16_t wx, wy, wz;
uint16_t rate;
long t1, t2, t3, n=0;
float temperature;
float pressure;
float altitude;
long sumWx = 0;
long sumWy = 0;
long sumWz = 0;

void setup() {
    // join I2C bus (I2Cdev library doesn't do this automatically)
    Wire.begin();

    // initialize serial communication
    Serial.begin(115200);
    setupL3G4200D(2000);
//    Serial.println();
//
//    Serial.print("Initializing SD card...");

    // see if the card is present and can be initialized:
//    if (!SD.begin(chipSelect)) {
//        Serial.println("Card failed, or not present");
//        // don't do anything more:
//        return;
//    }

//    Serial.println("card initialized.");
    digitalWrite(13,HIGH);
    
    // initialize devices
//    Serial.println("Initializing I2C devices...");
    mag.initialize();
    acc.initialize();
    gyr.initialize();
    bar.initialize(); 

    // verify connection
//    Serial.println("Testing device connections...");
//    Serial.println(mag.testConnection() ? "HMC5883L connection successful" : "HMC5883L connection failed");
//    Serial.println(acc.testConnection() ? "ADXL345 connection successful"  : "ADXL345 connection failed" );
//    Serial.println(gyr.testConnection() ? "L3G4200D connection successful" : "L3G4200D connection failed");
//    Serial.println(bar.testConnection() ? "BMP085 connection successful"   : "BMP085 connection failed"  );
    
    // set rates
    mag.setDataRate(6);                     //  160 Hz  
    acc.setRate(0xB);                       //  400 Hz
    gyr.setOutputDataRate(400);             //  400 Hz
    bar.setControl(BMP085_MODE_PRESSURE_0); // ~200 Hz  (faster mode, dt= 4.5ms)

//    dataFile = SD.open("teste5-270318.txt", FILE_WRITE);
    t1 = micros();

}


void loop() {
  
        n++;
        
        // barometer gives always the same readings without this 
     //   bar.setControl(BMP085_MODE_PRESSURE_0);  
        
        // read measurements from devices
        mag.getHeading(&mx, &my, &mz);
        acc.getAcceleration(&ax, &ay, &az);
//        gyr.getAngularVelocity(&wx, &wy, &wz);
     //   pressure = bar.getPressure();
        getGyroValues();

        // print in sdcard 
//        dataFile.print(mx); dataFile.print("\t");
//        dataFile.print(my); dataFile.print("\t");
//        dataFile.print(mz); dataFile.print("\t");
//    
//        dataFile.print(ax); dataFile.print("\t");
//        dataFile.print(ay); dataFile.print("\t");
//        dataFile.print(az); dataFile.print("\t");
//    
//        dataFile.print(wx); dataFile.print("\t");
//        dataFile.print(wy); dataFile.print("\t");
//        dataFile.print(wz); dataFile.print("\t");

        Serial.print(mx); Serial.print("\t");
        Serial.print(my); Serial.print("\t");
        Serial.print(mz); Serial.print("\t");
    
        Serial.print(ax); Serial.print("\t");
        Serial.print(ay); Serial.print("\t");
        Serial.print(az); Serial.print("\t");
    
        Serial.print(wx); Serial.print("\t");
        Serial.print(wy); Serial.print("\t");
        Serial.print(wz); Serial.print("\t");
//        Serial.print(sumWx); Serial.print("\t");
//        Serial.print(sumWy); Serial.print("\t");
//        Serial.println(sumWz);
//        sumWx = sumWx + wx;
//        sumWy = sumWy + wy;
//        sumWz = sumWz + wz;
            
      //  dataFile.print(pressure);
     
        //print time spend for each measurement
        t2 = micros();
//        dataFile.println(t2-t1);
        Serial.println(t2);
        
        //every some s, close file, and open it again. This takes around 25ms =(
//        if(n%5000 == 0){
//            dataFile.close();
//            Serial.println("fechou");
//            delay(1000);
//            dataFile = SD.open("teste5-270318.txt", FILE_WRITE);
//        }
        t1=t2;
}

void getGyroValues(){

  byte xMSB = readRegister(L3G4200D_Address, 0x29);
  byte xLSB = readRegister(L3G4200D_Address, 0x28);
  wx = ((xMSB << 8) | xLSB);

  byte yMSB = readRegister(L3G4200D_Address, 0x2B);
  byte yLSB = readRegister(L3G4200D_Address, 0x2A);
  wy = ((yMSB << 8) | yLSB);

  byte zMSB = readRegister(L3G4200D_Address, 0x2D);
  byte zLSB = readRegister(L3G4200D_Address, 0x2C);
  wz = ((zMSB << 8) | zLSB);
}

int setupL3G4200D(int scale){
  //From  Jim Lindblom of Sparkfun's code

  // Enable wx, wy, wz and turn off power down:
  writeRegister(L3G4200D_Address, CTRL_REG1, 0b00001111);

  // If you'd like to adjust/use the HPF, you can edit the line below to configure CTRL_REG2:
  writeRegister(L3G4200D_Address, CTRL_REG2, 0b00000000);

  // Configure CTRL_REG3 to generate data ready interrupt on INT2
  // No interrupts used on INT1, if you'd like to configure INT1
  // or INT2 otherwise, consult the datasheet:
  writeRegister(L3G4200D_Address, CTRL_REG3, 0b00001000);

  // CTRL_REG4 controls the full-scale range, among other things:

  if(scale == 250){
    writeRegister(L3G4200D_Address, CTRL_REG4, 0b00000000);
  }else if(scale == 500){
    writeRegister(L3G4200D_Address, CTRL_REG4, 0b00010000);
  }else{
    writeRegister(L3G4200D_Address, CTRL_REG4, 0b00110000);
  }

  // CTRL_REG5 controls high-pass filtering of outputs, use it
  // if you'd like:
  writeRegister(L3G4200D_Address, CTRL_REG5, 0b00000000);
}

void writeRegister(int deviceAddress, byte address, byte val) {
    Wire.beginTransmission(deviceAddress); // start transmission to device 
    Wire.write(address);       // send register address
    Wire.write(val);         // send value to write
    Wire.endTransmission();     // end transmission
}

int readRegister(int deviceAddress, byte address){

    int v;
    Wire.beginTransmission(deviceAddress);
    Wire.write(address); // register to read
    Wire.endTransmission();

    Wire.requestFrom(deviceAddress, 1); // read a byte

    while(!Wire.available()) {
        // waiting
    }

    v = Wire.read();
    return v;
}
