function [M_align,bias, Sf] = accelerometerCalibration(eNL, Ac, f0)
%ACCEROMETERCALIBRATION Summary of this function goes here
%   Detailed explanation goes her      
% f0 = [     0        -sind(eNL)    cosd(eNL);
%            0        -sind(eNL)   -cosd(eNL);
%            0         sind(eNL)    cosd(eNL);
%            0         sind(eNL)   -cosd(eNL);
%            
%            0        -sind(eNL)    cosd(eNL);
%            0        -sind(eNL)   -cosd(eNL);
%            0         sind(eNL)    cosd(eNL);
%            0         sind(eNL)   -cosd(eNL);
%            
%            0        -cosd(eNL)   -sind(eNL);
%            0        +cosd(eNL)    sind(eNL);
%            0        -cosd(eNL)    sind(eNL);
%            0        +cosd(eNL)   -sind(eNL);
%            
%        -cosd(eNL)    sind(eNL)       0     ;
%        +cosd(eNL)    sind(eNL)       0     ;
%        -cosd(eNL)       0         sind(eNL);
%        +cosd(eNL)       0         sind(eNL)];


           

M_l_neu = [ cosd(eNL)    -sind(eNL)  0;
    sind(eNL)     cosd(eNL)  0;
    0             0       1];

E = size(f0,1);

Qa = size(f0,2);

Fc = eye(Qa);

A = zeros(Qa,E);

Am = mean(Ac);

for j = 1 : E
    for i = 1 : Qa
        A(i,j) = Am((j-1)*Qa + i);
    end
end

F = [f0'; ones(1,E)];

Mbf_til = A*pinv(F);

for i = 1 : Qa
    Fc(i,i) = 1/norm(Mbf_til(i,1:Qa),2);
end

Mb_til = Fc*Mbf_til;

M_til = Mb_til(:,1:Qa);

b_til = Mb_til(:,Qa+1);

M_align = M_l_neu/M_til;

bias = M_l_neu*b_til;

Sf = Fc;

end

