%% Gr�ficos dos quaternions
figure()
plot(quaternions.Propagated.q1, '--k');
hold on;
plot(quaternions.Estimated.q1);
grid on;
legend('Valor propagado da componente q_1','Valor estimado da componente q_1')
title('Componente q_1')

figure()
plot(quaternions.Propagated.q2, '--k');
hold on;
plot(quaternions.Estimated.q2);
grid on;
legend('Valor propagado da componente q_2','Valor estimado da componente q_2')
title('Componente q_2')

figure()
plot(quaternions.Propagated.q3, '--k');
hold on;
plot(quaternions.Estimated.q3);
grid on;
legend('Valor propagado da componente q_3','Valor estimado da componente q_3')
title('Componente q_3')

figure()
plot(quaternions.Propagated.q4, '--k');
hold on;
plot(quaternions.Estimated.q4);
grid on;
legend('Valor propagado da componente q_4','Valor estimado da componente q_4')
title('Componente q_4')

%% Gr�ficos da acelera��o

figure()
subplot(2,1,1);
plot(accData.Measured.ax, '--k');
hold on;
plot(accData.Estimated.ax);
legend('Valor real da componente a_x','Valor estimado da componente a_x')
title('Componente a_x')
grid on;
subplot(2,1,2);
plot(accData.Measured.ax - accData.Estimated.ax);
title('Diferen�a entre o medido e o estimado')
grid on;

figure()
subplot(2,1,1);
plot(accData.Measured.ay, '--k');
hold on;
plot(accData.Estimated.ay);
grid on;
legend('Valor real da componente a_y','Valor estimado da componente a_y')
title('Componente a_y')
subplot(2,1,2);
plot(accData.Measured.ay - accData.Estimated.ay);
title('Diferen�a entre o medido e o estimado')
grid on;

figure()
subplot(2,1,1);
plot(accData.Measured.az, '--k');
hold on;
plot(accData.Estimated.az);
grid on;
legend('Valor real da componente a_z','Valor estimado da componente a_z')
title('Componente a_z')
subplot(2,1,2);
plot(accData.Measured.az - accData.Estimated.az);
title('Diferen�a entre o medido e o estimado')
grid on;

%% Gr�ficos do campo magn�tico

figure()
subplot(2,1,1);
plot(magData.Measured.mx, '--k');
hold on;
plot(magData.Estimated.mx);
grid on;
legend('Valor real da componente m_x','Valor estimado da componente m_x')
title('Componente m_x')
subplot(2,1,2);
plot(magData.Measured.mx - magData.Estimated.mx);
title('Diferen�a entre o medido e o estimado')
grid on;

figure()
subplot(2,1,1);
plot(magData.Measured.my, '--k');
hold on;
plot(magData.Estimated.my);
grid on;
legend('Valor real da componente m_y','Valor estimado da componente m_y')
title('Componente m_y')
subplot(2,1,2);
plot(magData.Measured.my - magData.Estimated.my);
title('Diferen�a entre o medido e o estimado')
grid on;

figure()
subplot(2,1,1);
plot(magData.Measured.mz, '--k');
hold on;
plot(magData.Estimated.mz);
grid on;
legend('Valor real da componente m_z','Valor estimado da componente m_z')
title('Componente m_z')
subplot(2,1,2);
plot(magData.Measured.mz - magData.Estimated.mz);
title('Diferen�a entre o medido e o estimado')
grid on;

%% Gr�ficos dos quaternions

for k = 1:15:simulationSteps
    q = [quaternions.Estimated.q1(k), quaternions.Estimated.q2(k), quaternions.Estimated.q3(k), quaternions.Estimated.q4(k)];
    plot_frame(q);
end

%% Melhorar gr�ficos

fontSize = 14;
lineWidth = 2;
handles = findall(0, 'type', 'figure');
for i=1:length(handles)
    handle = handles(i);
%     set(findall(handle, '-property', 'LineWidth'), 'LineWidth', lineWidth);
%     set(findall(handle, '-property', 'FontSize'), 'FontSize', fontSize);
    print(handle, '-depsc', sprintf('figure%d.eps', i));
end