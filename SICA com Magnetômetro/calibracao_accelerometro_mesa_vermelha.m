fileName = 'teste_mesa_vermelha_acc.txt';

data = importdata(fileName);

%% Ensaio 01: 3100:4100

interval.lower(1) = 3100;
interval.upper(1) = 4100;
times.test1 = data(interval.lower(1):interval.upper(1), 10);

%% Ensaio 02: 4700:5700

interval.lower(2) = 4700;
interval.upper(2) = 5700;
times.test2 = data(interval.lower(2):interval.upper(2), 10);

%% Ensaio 03: 6200:7200

interval.lower(3) = 6200;
interval.upper(3) = 7200;
times.test3 = data(interval.lower(3):interval.upper(3), 10);


%% Ensaio 04: 7900:8900

interval.lower(4) = 7900;
interval.upper(4) = 8900;
times.test4 = data(interval.lower(4):interval.upper(4), 10);

%% Ensaio 05: 9500:10500

interval.lower(5) = 9500;
interval.upper(5) = 10500;
times.test5 = data(interval.lower(5):interval.upper(5), 10);

%% Ensaio 06: 11000:12000

interval.lower(6) = 11000;
interval.upper(6) = 12000;
times.test6 = data(interval.lower(6):interval.upper(6), 10);

%% Ensaio 07: 12500:13500

interval.lower(7) = 12500;
interval.upper(7) = 13500;
times.test7 = data(interval.lower(7):interval.upper(7), 10);

%% Ensaio 08: 14000:15000

interval.lower(9) = 14000;
interval.upper(9) = 15000;
times.test9 = data(interval.lower(9):interval.upper(9), 10);

%% Ensaio 09: 15500:16500

interval.lower(9) = 15500;
interval.upper(9) = 16500;
times.test9 = data(interval.lower(9):interval.upper(9), 10);

%% Ensaio 10: 17000:18000

interval.lower(10) = 17000;
interval.upper(10) = 18000;
times.test10 = data(interval.lower(10):interval.upper(10), 10);

%% Ensaio 11: 18500:19500

interval.lower(11) = 18500;
interval.upper(11) = 19500;
times.test11 = data(interval.lower(11):interval.upper(11), 10);

%% Ensaio 12: 19900:20900

interval.lower(12) = 19900;
interval.upper(12) = 20900;
times.test12 = data(interval.lower(12):interval.upper(12), 10);

%% Ensaio 13: 21300:22300

interval.lower(13) = 21300;
interval.upper(13) = 22300;
times.test13 = data(interval.lower(13):interval.upper(13), 10);

%% Ensaio 14: 22900:23900

interval.lower(14) = 22900;
interval.upper(14) = 23900;
times.test14 = data(interval.lower(14):interval.upper(14), 10);

%% Ensaio 15: 24300:25300

interval.lower(15) = 24300;
interval.upper(15) = 25300;
times.test15 = data(interval.lower(15):interval.upper(15), 10);

%% Ensaio 16: 25800:26800

interval.lower(16) = 25800;
interval.upper(16) = 26800;
times.test16 = data(interval.lower(16):interval.upper(16), 10);

totalExperiments = 16;

for k = 1:totalExperiments
    magDataAux = data(interval.lower(k):(interval.upper(k)),1:3);
    magData(k) = Magnetometer(magDataAux(:,1), magDataAux(:,2), magDataAux(:,3));
    accDataAux = data(interval.lower(k):(interval.upper(k)),4:6);
    accData(k) = Accelerometer(accDataAux(:,1), accDataAux(:,2), accDataAux(:,3));
    gyroDataAux = data(interval.lower(k):interval.upper(k),7:9);
    gyroData(k) = Gyrometer(gyroDataAux(:,1), gyroDataAux(:,2), gyroDataAux(:,3));
    %     accData(k).plotData();
end

%% Gir�metro

% G(:, 1:3) = [gyroData(1).p(1400:3000),gyroData(1).q(1400:3000),gyroData(1).r(1400:3000)];
% G(:, 4:6) = [gyroData(2).p(1400:3000),gyroData(2).q(1400:3000),gyroData(2).r(1400:3000)];
% G(:, 7:9) = [gyroData(3).p(1400:3000),gyroData(3).q(1400:3000),gyroData(3).r(1400:3000)];

totalExperiments = 16;

for k=1:totalExperiments
    G(:, 3*k-2:3*k) = [gyroData(k).p,gyroData(k).q,gyroData(k).r];
    M(:, 3*k-2:3*k) = [magData(k).mx,magData(k).my,magData(k).mz];
    Ac(:, 3*k-2:3*k) = [accData(k).ax,accData(k).ay,accData(k).az];
end

eNL = 0;

f0 = [  cosd(eNL)   -sind(eNL)       0     ;
       -cosd(eNL)   -sind(eNL)       0     ;
        cosd(eNL)       0        -sind(eNL);
       -cosd(eNL)       0        -sind(eNL);
    
        cosd(eNL)    sind(eNL)       0     ;
       -cosd(eNL)    sind(eNL)       0     ;
        cosd(eNL)       0         sind(eNL);
       -cosd(eNL)       0         sind(eNL);
    
           0        -cosd(eNL)   -sind(eNL);
           0        +cosd(eNL)    sind(eNL);
           0        -cosd(eNL)    sind(eNL);
           0        +cosd(eNL)   -sind(eNL);
    
           0        -sind(eNL)    cosd(eNL);
           0        -sind(eNL)   -cosd(eNL);
           0         sind(eNL)    cosd(eNL);
           0         sind(eNL)   -cosd(eNL)];

M_l_neu = [ cosd(eNL)    -sind(eNL)  0;
    sind(eNL)     cosd(eNL)  0;
    0             0       1];

E = size(f0,1);

Qa = size(f0,2);

Fc = eye(Qa);

A = zeros(Qa,E);

Am = mean(Ac);

for j = 1 : E
    for i = 1 : Qa
        A(i,j) = Am((j-1)*Qa + i);
    end
end

F = [f0'; ones(1,E)];

Mbf_til = A*pinv(F);

for i = 1 : Qa
    Fc(i,i) = 1/norm(Mbf_til(i,1:Qa),2);
end

Mb_til = Fc*Mbf_til;

M_til = Mb_til(:,1:Qa);

b_til = Mb_til(:,Qa+1);

M_align = M_l_neu/M_til;

bias = M_l_neu*b_til;

Sf = Fc;

accAux = Accelerometer(data(:,4), data(:,5), data(:,6));

for i=1:length(accAux.ax)
    Aaux = M_align*(Sf*[accAux.ax(i);accAux.ay(i);accAux.az(i)]-bias);
    if i~=1
        accDataCalibrated = accDataCalibrated.addPoint(Aaux(1), Aaux(2), Aaux(3));
    else
        accDataCalibrated = Accelerometer(Aaux(1), Aaux(2), Aaux(3));
    end
end
figure()
plot(data(:, 4:6));
grid on;
accDataCalibrated.plotData();
