function [G, Ac] = getStimExperimentalMatrices(stim, totalExperiments, interval)
%GETEXPERIMENTALMATRICES Summary of this function goes here
%   Detailed explanation goes here

for k = 1:totalExperiments
    accDataAux = [stim.ax(interval.lower(k):(interval.upper(k)))', stim.ay(interval.lower(k):(interval.upper(k)))', stim.az(interval.lower(k):(interval.upper(k)))'];
    accData(k) = Accelerometer(accDataAux(:,1), accDataAux(:,2), accDataAux(:,3));
    gyroDataAux = [stim.wx(interval.lower(k):(interval.upper(k)))' stim.wy(interval.lower(k):(interval.upper(k)))', stim.wz(interval.lower(k):(interval.upper(k)))'];
    gyroData(k) = Gyrometer(gyroDataAux(:,1), gyroDataAux(:,2), gyroDataAux(:,3));
%     accData(k).plotData();
%     gyroData(k).plotData();
    
end

for k=1:totalExperiments
    G(:, 3*k-2:3*k) = [gyroData(k).p,gyroData(k).q,gyroData(k).r];
    Ac(:, 3*k-2:3*k) = [accData(k).ax,accData(k).ay,accData(k).az];
end

end

