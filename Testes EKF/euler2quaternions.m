function [q] = euler2quaternions(euler)
%EULER2QUATERNIONS Summary of this function goes here
%   Detailed explanation goes here
theta = pi/180*euler(1); psi = pi/180*euler(2); phi = pi/180*euler(3);

D = [           cos(psi)*cos(theta)                          sin(psi)     -sin(theta)*cos(psi);
    (sin(theta)*sin(phi)-cos(theta)*sin(psi)*cos(phi))  cos(psi)*cos(phi)  (cos(theta)*sin(phi)+sin(theta)*sin(psi)*cos(phi));
    (sin(theta)*cos(phi)+cos(theta)*sin(psi)*sin(phi)) -cos(psi)*sin(phi)  (cos(theta)*cos(phi)-sin(theta)*sin(psi)*sin(phi))];
T = trace(D);

q = [0 0 0 1];
qsq = [1 + 2*D(1,1)-T; 1 + 2*D(2,2)-T; 1 + 2*D(3,3)-T; 1+T]/4;

[x, i] = max(qsq);

if i == 4
    q(4) = sqrt(x);
    q(1) = (D(2,3) - D(3,2))/(4*q(4));
    q(2) = (D(3,1) - D(1,3))/(4*q(4));
    q(3) = (D(1,2) - D(2,1))/(4*q(4));
elseif i == 3
    q(3) = sqrt(x);
    q(1) = (D(1,3) + D(3,1))/(4*q(3));
    q(2) = (D(3,2) + D(2,3))/(4*q(3));
    q(3) = (D(1,2) - D(2,1))/(4*q(3));
elseif i == 2
    q(2) = sqrt(x);
    q(1) = (D(1,2) + D(2,1))/(4*q(2));
    q(2) = (D(3,2) + D(2,3))/(4*q(2));
    q(3) = (D(3,1) - D(1,3))/(4*q(2));
else
    q(1) = sqrt(x);
    q(2) = (D(1,2) + D(2,1))/(4*q(1));
    q(3) = (D(1,3) + D(3,1))/(4*q(1));
    q(4) = (D(2,3) - D(3,2))/(4*q(1));
end

end