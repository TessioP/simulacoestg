function [futureQuaternion, stateTransitionMatrix] = quaternionPropagation(currentQuaternion, angularSpeed, step)
%QUATERNIONPROPAGATION Summary of this function goes here
%   Detailed explanation goes here

% c�lculo da matriz de transi��o de estado
angularSpeedPropagation = [       0          angularSpeed(3) -angularSpeed(2)  angularSpeed(1);
                           -angularSpeed(3)        0          angularSpeed(1)  angularSpeed(2);
                            angularSpeed(2) -angularSpeed(1)        0          angularSpeed(3);
                           -angularSpeed(1) -angularSpeed(2) -angularSpeed(3)        0        ];

angularSpeedNorm = norm(angularSpeed);

if angularSpeedNorm~=0
        stateTransitionMatrix = (cos(angularSpeedNorm*step/2)*eye(4)+1/angularSpeedNorm*sin(angularSpeedNorm*step/2)*angularSpeedPropagation);
    else
        stateTransitionMatrix = eye(4) + (step/2)*angularSpeedPropagation;
end

futureQuaternion = stateTransitionMatrix*currentQuaternion;

futureQuaternion = futureQuaternion/norm(futureQuaternion);
end

