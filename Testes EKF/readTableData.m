function [table, body] = readTableData(fileName)
%READTABLEDATA Summary of this function goes here
%   Detailed explanation goes here

%% Leitura dos dados da mesa
data = dlmread(fileName,';', 500,0);
table.times = data(:,1);
table.times = table.times - table.times(1);
table.inner.theta = data(:,2);
table.inner.dtheta = data(:,3);
table.inner.ddtheta = data(:,4);
table.middle.theta = data(:,5);
table.middle.dtheta = data(:,6);
table.middle.ddtheta = data(:,7);
table.outer.theta = data(:,8);
table.outer.dtheta = data(:,9);
table.outer.ddtheta = data(:,10);

body.omega = zeros(3, length(table.outer.theta));
for k=1:length(table.outer.theta)
    gama = table.outer.theta(k);      %posi��o do eixo outer
    beta = table.middle.theta(k);     %posi��o do eixo middle
    alfa = table.inner.theta(k);      %posi��o do eixo inner
    
    table.quaternions(:, k) = e2qZYX([gama, beta, alfa]);

    qAux = [table.quaternions(4, k); table.quaternions(1:3, k)];
    
%     table.euler(:,k) = 180/pi*q2eYZX(table.quaternions(:,k));

    table.euler(:,k) = 180/pi*quat2angle(qAux', 'YZX')';
    
    % Transfere as velocidades angulares da mesa para o referencial inercial
    D = [ 0     sind(alfa) -sind(beta)*cosd(alfa);
          0     cosd(alfa)  sind(alfa)*sind(beta);
          1        0              cosd(beta)    ];
    
    %omega da mesa convertido para omega do corpo.
    body.omega(:, k) = D*pi/180*[table.inner.dtheta(k); table.middle.dtheta(k); table.outer.dtheta(k)];
end

end
