%% Ensaio de calibra��o do gir�metro

%% Leitura dos dados da AHRS
% 
% fileName = '28082018ahrs_giros.csv';
% 
% ahrs_gyros = readAHRSdata(fileName);
% 
% lineWidth = 2;
% fontSize = 14;
% 
% colors = linspecer(3);
% 
% lineWidth = 2;
% fontSize = 14;
% 
% colors = linspecer(3);
% 
% plotNonCalibredAHRSGraphics(lineWidth, fontSize, ahrs_gyros);
% 
% % magAux = Magnetometer(ahrs_gyros.mx, ahrs_gyros.my, ahrs_gyros.mz);
% % 
% % for i=1:length(magAux.mx)
% %     Maux = Mag_align*(mag_Sf*[magAux.mx(i);magAux.my(i);magAux.mz(i)]-mag_bias);
% %     if i~=1
% %         magDataCalibrated = magDataCalibrated.addPoint(Maux(1)/norm(Maux), Maux(2)/norm(Maux), Maux(3)/norm(Maux));
% %     else
% %         magDataCalibrated = Magnetometer(Maux(1)/norm(Maux), Maux(2)/norm(Maux), Maux(3)/norm(Maux));
% %     end
% % end
% % magDataCalibrated.plotData();
% figure()
% plot3(ahrs_gyros.mx, ahrs_gyros.my, ahrs_gyros.mz)
% axis equal;
% grid on
% % figure()
% % plot3(magDataCalibrated.mx, magDataCalibrated.my, magDataCalibrated.mz, '.');
% % axis equal;
% % grid on
% 
% 
% %% Divis�o dos ensaios
% 
% interval.lower = [3000 6000 11000 14000 18000 22000 26500 30000 35000 38000 43000 46000];
% interval.upper = interval.lower + 1000;



%% Leitura dos dados da STIM

fileName = '28082018stim_giros.csv';

stim = readSTIMdata(fileName);

lineWidth = 1.5;

fontSize = 14;

colors = linspecer(3);

% plotNonCalibredSTIMGraphics(lineWidth, fontSize, stim);
figure()
plot(stim.times, stim.wx, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(stim.times, stim.wy, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
plot(stim.times, stim.wz, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('\omega_x','\omega_y','\omega_z')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Dado bruto do sensor', 'FontSize', fontSize)
grid on
% title('Gyros STIM');
print('-depsc', sprintf('GYRO-gyrosBrutosSTIM.eps'));
movefile('GYRO-gyrosBrutosSTIM.eps','C:\ITA\tg\Cap3');

xlim([50 65])
ylim([-0.55, 0.55])
print('-depsc', sprintf('GYRO-gyroBrutosSTIMBias.eps'));
movefile('GYRO-gyroBrutosSTIMBias.eps','C:\ITA\tg\Cap3');
xlim([50 65])
ylim([29.6, 30.4])
print('-depsc', sprintf('GYRO-gyroBrutosSTIMPatamar.eps'));
movefile('GYRO-gyroBrutosSTIMPatamar.eps','C:\ITA\tg\Cap3');

%% Divis�o dos ensaios

interval.lower = [6150 11715 23350 32300 43825 51680 62710 72115 83000 92000 104000 112240];

interval.upper = interval.lower + 1000;

totalExperiments = 12;
% 
% 
%% Separa��o das matrizes

[G, Ac] = getStimExperimentalMatrices(stim, totalExperiments, interval);

[Htil_stim_gyros, FE_stim_gyros, bias_stim_gyros] = gyrometerCalibration(G);



%% Applying calibration

gyroAux = Gyrometer(stim.wx, stim.wy, stim.wz);

for i=1:length(gyroAux.wx)
    Aaux = Htil_stim_gyros\(FE_stim_gyros*[gyroAux.wx(i);gyroAux.wy(i);gyroAux.wz(i)]-bias_stim_gyros);
    if i~=1
        gyroDataCalibrated = gyroDataCalibrated.addPoint(Aaux(1), Aaux(2), Aaux(3));
    else
        gyroDataCalibrated = Gyrometer(Aaux(1), Aaux(2), Aaux(3));
    end
end
gyroDataCalibrated.plotData();                                                                                                                                                              
%% Plotting graphics

colors = linspecer(3);

figure()
plot(stim.times, gyroDataCalibrated.wx, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(stim.times, gyroDataCalibrated.wy, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
plot(stim.times, gyroDataCalibrated.wz, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('\omega_x','\omega_y','\omega_z')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Velocidade [�/s]', 'FontSize', fontSize)
grid on
% title('Acc AHRS');
print('-depsc', sprintf('GYRO-gyroCalibradoSTIM.eps'));
movefile('GYRO-gyroCalibradoSTIM.eps','C:\ITA\tg\Cap3');
xlim([50 65])
ylim([-0.55, 0.55])
print('-depsc', sprintf('GYRO-gyrosCalibradoSTIMBias.eps'));
movefile('GYRO-gyrosCalibradoSTIMBias.eps','C:\ITA\tg\Cap3');
xlim([50 65])
ylim([29.6, 30.4])
print('-depsc', sprintf('GYRO-gyroCalibradoSTIMPatamar.eps'));
movefile('GYRO-gyroCalibradoSTIMPatamar.eps','C:\ITA\tg\Cap3');


