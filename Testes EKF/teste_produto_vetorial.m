times = 0:(1/500):stim_nav.times(end);

for i=10:10:261150
    accAux = [accData.Calibrated.ax(i/2), accData.Calibrated.ay(i/2), accData.Calibrated.az(i/2)];
    magAux = [magData.Calibrated.mx(i/5), magData.Calibrated.my(i/5), magData.Calibrated.mz(i/5)];
    angleMagGrav(i/10) = acos(dot(accAux, magAux)/norm(accAux)/norm(magAux));
end

plot(angleMagGrav)