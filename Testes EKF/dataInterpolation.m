%% Script for interpolation data test


fileName = '28082018_mesa.csv';
[table, body] = readTableData(fileName);


fileName = '28082018ahrs_nav.csv';
ahrs_nav = readAHRSdata(fileName);


fileName = '28082018stim_nav.csv';
stim_nav = readSTIMdata(fileName);

desiredFs = 500;
[body.omegaResampled(1, :), table.timesResampled] = resample(body.omega(1, :), table.times, desiredFs);
[body.omegaResampled(2, :), table.timesResampled] = resample(body.omega(2, :), table.times, desiredFs);
[body.omegaResampled(3, :), table.timesResampled] = resample(body.omega(3, :), table.times, desiredFs);

[stim_nav.wxResampled, stim_nav.timesResampled] = resample(stim_nav.wx, stim_nav.times, desiredFs);
[stim_nav.wyResampled, stim_nav.timesResampled] = resample(stim_nav.wy, stim_nav.times, desiredFs);
[stim_nav.wzResampled, stim_nav.timesResampled] = resample(stim_nav.wz, stim_nav.times, desiredFs);


ahrsInitialPoint = 5167;
tableInitialPoint = 30776;
stimInitialPoint = 26344;

totalSteps = 210000-tableInitialPoint;

table.timesResampled = table.timesResampled - table.timesResampled(tableInitialPoint);

stim_nav.timesResampled = stim_nav.timesResampled - stim_nav.timesResampled(stimInitialPoint);

% plot(table.timesResampled(tableInitialPoint:tableInitialPoint+totalSteps), body.omegaResampled(tableInitialPoint:tableInitialPoint+totalSteps), stim_nav.timesResampled(stimInitialPoint:stimInitialPoint+totalSteps), stim_nav.wxResampled(stimInitialPoint:stimInitialPoint+totalSteps));