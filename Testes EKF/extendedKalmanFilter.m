function [x_k1, P_k1, ze_k1, z_k1] = extendedKalmanFilter(x_k, P_k, u_k, zMedido_k1, Q, R_k1, initialAcc, initialMag, dt, doEKF)
%EXTENDEDKALMANFILTER Summary of this function goes here
%   Detailed explanation goes here
Lx = 0;
Ly = 0;
Lz = 0.25;
% Lz = 0;
%% 1a)Propaga��o do estado estimado
% C�lculo de xhat_e(k+1|k):
% propaga��o do estado estimado pela din�mica do sistema
qe_k = x_k(1:4);
% wm_k = x_k(5:7);
[qe_k1, Exp_Omega_k_T] = quaternionPropagation(qe_k, u_k, dt);

% we_k1 = u_k;

% xe_k1 = [qe_k1; we_k1];
xe_k1 = qe_k1;

% C�lculo de P(k+1|k):
% Propaga��o da matriz de covari�ncia (nesse passo, a incerteza deve
% aumentar)
F_k_1  =[[ 1              0.5*dt*u_k(3) -0.5*dt*u_k(2)  0.5*dt*u_k(1)];
         [-0.5*dt*u_k(3)  1              0.5*dt*u_k(1)  0.5*dt*u_k(2)];
         [ 0.5*dt*u_k(2) -0.5*dt*u_k(1)  1              0.5*dt*u_k(3)];
         [-0.5*dt*u_k(1) -0.5*dt*u_k(2) -0.5*dt*u_k(3)  1            ]];

% F_k_2  =[[ 0.5*dt*qe_k(4) -0.5*dt*qe_k(3)  0.5*dt*qe_k(2)];
%          [ 0.5*dt*qe_k(3)  0.5*dt*qe_k(4) -0.5*dt*qe_k(1)];
%          [-0.5*dt*qe_k(2)  0.5*dt*qe_k(1)  0.5*dt*qe_k(4)];
%          [-0.5*dt*qe_k(1) -0.5*dt*qe_k(2) -0.5*dt*qe_k(3)]];
mod = norm(u_k);
T = dt;
p = u_k(1);
q = u_k(2);
r = u_k(3);
F_k_1 = [[              cos((T*mod)/2),  (r*sin((T*mod)/2))/(2*mod), -(q*sin((T*mod)/2))/(2*mod), (p*sin((T*mod)/2))/(2*mod)];
        [ -(r*sin((T*mod)/2))/(2*mod),              cos((T*mod)/2),  (p*sin((T*mod)/2))/(2*mod), (q*sin((T*mod)/2))/(2*mod)];
        [  (q*sin((T*mod)/2))/(2*mod), -(p*sin((T*mod)/2))/(2*mod),              cos((T*mod)/2), (r*sin((T*mod)/2))/(2*mod)];
        [ -(p*sin((T*mod)/2))/(2*mod), -(q*sin((T*mod)/2))/(2*mod), -(r*sin((T*mod)/2))/(2*mod),             cos((T*mod)/2)]];

F_k  = zeros(4,4);

F_k(1:4,1:4) = F_k_1;
% F_k(1:4,5:7) = F_k_2;

Csi_k = [-qe_k(2) -qe_k(3) -qe_k(4);
          qe_k(1) -qe_k(4)  qe_k(3);
          qe_k(4)  qe_k(1) -qe_k(2);
         -qe_k(3)  qe_k(2)  qe_k(1)];

Gama_k = (dt/2)*Exp_Omega_k_T*Csi_k;       

Q_q_k = Gama_k*Q*Gama_k';    

% Q_k = [Q_q_k, zeros(4,3); zeros(3, 4), Q];
Q_k = Q_q_k;

% P_k1 = F_k*P_k*F_k' + Q_k;
P_k1 = Exp_Omega_k_T*P_k*Exp_Omega_k_T' + Q_k;

if doEKF

    %% Predi��o das medidas
    xe1 = xe_k1(1);      %q1
    xe2 = xe_k1(2);      %q2
    xe3 = xe_k1(3);      %q3
    xe4 = xe_k1(4);      %q4
    xe5 = u_k(1);      %wx
    xe6 = u_k(2);      %wy
    xe7 = u_k(3);      %wz

    % C�lculo de zhat(k+1|k)
    % 
    ax = initialAcc(1); ay = initialAcc(2); az = initialAcc(3);
    mx = initialMag(1); my = initialMag(2); mz = initialMag(3);
%     mx = 0; my = 0; mz = 0;
    ze_k1 = [ay*(2*xe1*xe2 + 2*xe3*xe4) - ax*(2*xe2^2 + 2*xe3^2 - 1) + az*(2*xe1*xe3 - 2*xe2*xe4) - Lx*(xe6^2 + xe7^2);
             ax*(2*xe1*xe2 - 2*xe3*xe4) - ay*(2*xe1^2 + 2*xe3^2 - 1) + az*(2*xe1*xe4 + 2*xe2*xe3) - Ly*(xe5^2 + xe7^2);
             ax*(2*xe1*xe3 + 2*xe2*xe4) - az*(2*xe1^2 + 2*xe2^2 - 1) - ay*(2*xe1*xe4 - 2*xe2*xe3) - Lz*(xe5^2 + xe6^2);
             my*(2*xe1*xe2 + 2*xe3*xe4) - mx*(2*xe2^2 + 2*xe3^2 - 1) + mz*(2*xe1*xe3 - 2*xe2*xe4);
             mx*(2*xe1*xe2 - 2*xe3*xe4) - my*(2*xe1^2 + 2*xe3^2 - 1) + mz*(2*xe1*xe4 + 2*xe2*xe3);
             mx*(2*xe1*xe3 + 2*xe2*xe4) - mz*(2*xe1^2 + 2*xe2^2 - 1) - my*(2*xe1*xe4 - 2*xe2*xe3)];

    %% C�lculo de Pz(k+1|k)
    H_k1 = [            2*ay*xe2 + 2*az*xe3, 2*ay*xe1 - 4*ax*xe2 - 2*az*xe4, 2*ay*xe4 - 4*ax*xe3 + 2*az*xe1, 2*ay*xe3 - 2*az*xe2;
             2*ax*xe2 - 4*ay*xe1 + 2*az*xe4,            2*ax*xe1 + 2*az*xe3, 2*az*xe2 - 4*ay*xe3 - 2*ax*xe4, 2*az*xe1 - 2*ax*xe3;
             2*ax*xe3 - 2*ay*xe4 - 4*az*xe1, 2*ax*xe4 + 2*ay*xe3 - 4*az*xe2,            2*ax*xe1 + 2*ay*xe2, 2*ax*xe2 - 2*ay*xe1;
                        2*my*xe2 + 2*mz*xe3, 2*my*xe1 - 4*mx*xe2 - 2*mz*xe4, 2*my*xe4 - 4*mx*xe3 + 2*mz*xe1, 2*my*xe3 - 2*mz*xe2;
             2*mx*xe2 - 4*my*xe1 + 2*mz*xe4,            2*mx*xe1 + 2*mz*xe3, 2*mz*xe2 - 4*my*xe3 - 2*mx*xe4, 2*mz*xe1 - 2*mx*xe3;
             2*mx*xe3 - 2*my*xe4 - 4*mz*xe1, 2*mx*xe4 + 2*my*xe3 - 4*mz*xe2,            2*mx*xe1 + 2*my*xe2, 2*mx*xe2 - 2*my*xe1];

    Pz_k1 = H_k1*P_k1*H_k1' + R_k1;

    %% 1c) Covari�ncia cruzada, Pzx(k+1|k)
    Pxz_k1 = P_k1*H_k1';

    %% 2a) Ganho de Kalman
    K_k1 = Pxz_k1*pinv(Pz_k1);

    %% 2b) Atualiza��o da estimativa
    % ze_k1 = H_k1*xe_k1;
    x_k1 = xe_k1 + K_k1*(zMedido_k1 - ze_k1);
    x_k1(1:4) = x_k1(1:4)/norm(x_k1(1:4));  %normaliza��o do quaternion

    %% 3) Atualiza��o da Covari�ncia
    P_k1 = P_k1 - K_k1*Pz_k1*K_k1';

    xe1 = x_k1(1);      %q1
    xe2 = x_k1(2);      %q2
    xe3 = x_k1(3);      %q3
    xe4 = x_k1(4);      %q4
    xe5 = u_k(1);      %wx
    xe6 = u_k(2);      %wy
    xe7 = u_k(3);      %wz

    z_k1 = [ay*(2*xe1*xe2 + 2*xe3*xe4) - ax*(2*xe2^2 + 2*xe3^2 - 1) + az*(2*xe1*xe3 - 2*xe2*xe4) - Lx*(xe6^2 + xe7^2);
             ax*(2*xe1*xe2 - 2*xe3*xe4) - ay*(2*xe1^2 + 2*xe3^2 - 1) + az*(2*xe1*xe4 + 2*xe2*xe3) - Ly*(xe5^2 + xe7^2);
             ax*(2*xe1*xe3 + 2*xe2*xe4) - az*(2*xe1^2 + 2*xe2^2 - 1) - ay*(2*xe1*xe4 - 2*xe2*xe3) - Lz*(xe5^2 + xe6^2);
             my*(2*xe1*xe2 + 2*xe3*xe4) - mx*(2*xe2^2 + 2*xe3^2 - 1) + mz*(2*xe1*xe3 - 2*xe2*xe4);
             mx*(2*xe1*xe2 - 2*xe3*xe4) - my*(2*xe1^2 + 2*xe3^2 - 1) + mz*(2*xe1*xe4 + 2*xe2*xe3);
             mx*(2*xe1*xe3 + 2*xe2*xe4) - mz*(2*xe1^2 + 2*xe2^2 - 1) - my*(2*xe1*xe4 - 2*xe2*xe3)];
else
    x_k1 = xe_k1;
    z_k1 = zMedido_k1;
    ze_k1 = z_k1;
end

end

