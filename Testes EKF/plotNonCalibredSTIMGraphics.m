function [] = plotNonCalibredSTIMGraphics(lineWidth, fontSize, stim)
%PLOTNONCALIBREDSTIMGRAPHICS Summary of this function goes here
%   Detailed explanation goes here

colors = linspecer(3);

figure()
plot(stim.times, stim.wx, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(stim.times, stim.wy, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
plot(stim.times, stim.wz, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('\omega_x','\omega_y','\omega_z')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Dado bruto do sensor', 'FontSize', fontSize)
grid on
% title('Gyros STIM');
% print('-depsc', sprintf('GYRO-gyrosBrutosSTIM.eps'));
% movefile('GYRO-gyrosBrutosSTIM.eps','C:\ITA\tg\Cap3');

% xlim([25 40])
% ylim([-0.55, 0.55])
% print('-depsc', sprintf('GYRO-gyroBrutosSTIMBias.eps'));
% movefile('GYRO-gyroBrutosSTIMBias.eps','C:\ITA\tg\Cap3');
% xlim([50 65])
% ylim([29.6, 30.4])
% print('-depsc', sprintf('GYRO-gyroBrutosSTIMPatamar.eps'));
% movefile('GYRO-gyroBrutosSTIMPatamar.eps','C:\ITA\tg\Cap3');


figure()
plot(stim.times, stim.ax, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(stim.times, stim.ay, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
plot(stim.times, stim.az, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('a_x','a_y','a_z')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Dado bruto do sensor', 'FontSize', fontSize)
grid on
% title('Acc AHRS');
print('-depsc', sprintf('ACC-accBrutosSTIM.eps'));
movefile('ACC-accBrutosSTIM.eps','C:\ITA\tg\Cap3');


% xlim([30 55])
% ylim([0.98, 1.02])
% print('-depsc', sprintf('ACC-accBrutosSTIMPatamar.eps'));
% % movefile('ACC-accBrutosSTIMPatamar.eps','C:\ITA\tg\Cap3');
% xlim([30 55])
% ylim([-0.03, 0.03])
% print('-depsc', sprintf('ACC-accBrutosSTIMBias.eps'));
% % movefile('ACC-accBrutosSTIMBias.eps','C:\ITA\tg\Cap3');
end