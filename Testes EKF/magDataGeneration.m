%% Reading, resampling and generating magnetometer data

clear all; close all; clc;
%% Reading absolut values from table encoders

fileName = '28082018_mesa.csv';

[table, body] = readTableData(fileName);

%% Calibration data obtained from other scripts
Mag_align = [ 1.0232   -0.2441    0.0873;
              0.0426    1.0002   -0.0832;
             -0.0455    0.0760   -1.0127];
mag_Sf =  1.0e-08*[ 0.3564         0         0;
                         0    0.3025         0;
                         0         0    0.2477];
mag_bias = 1.0e-04*[-0.0161; 0.0321; 0.1082];

M_align_stim_acc = [    1.0000    0.0032    0.0024;
                       -0.0028    1.0000   -0.0005;
                       -0.0039   -0.0002    1.0000];
    
Sf_stim_acc = [9.8227         0         0;
                    0    9.8227         0;
                    0         0    9.8220];
                
bias_stim_acc = [ -0.0170; 0.0452; -0.0568];

Htil_stim_gyros =[ 1.0000    0.0045   -0.0071;
                  -0.0040    1.0000    0.0002;
                   0.0085   -0.0004    1.0000];

FE_stim_gyros = [ 0.0174         0         0;
                       0    0.0174         0;
                       0         0    0.0174];
                   
bias_stim_gyros = [   -0.0006; 0.0011; -0.0000];

%% Reading and calibration of STIM data

fileName = '28082018stim_nav.csv';
% fileName = '28082018stim_giros.csv';
stim_nav = readSTIMdata(fileName);

% plotNonCalibredSTIMGraphics(lineWidth, fontSize, stim_nav);

accAux.ax = stim_nav.ax;
accAux.ay = stim_nav.ay;
accAux.az = stim_nav.az;

accData.Calibrated.ax = zeros(1, length(stim_nav.ax));
accData.Calibrated.ay = zeros(1, length(stim_nav.ay));
accData.Calibrated.az = zeros(1, length(stim_nav.az));

for i=1:length(accAux.ax)
    Aaux = M_align_stim_acc*(Sf_stim_acc*[accAux.ax(i);accAux.ay(i);accAux.az(i)]-bias_stim_acc);
    accData.Calibrated.ax(i) = Aaux(1);
    accData.Calibrated.ay(i) = Aaux(2);
    accData.Calibrated.az(i) = Aaux(3);
end

gyroAux.wx = stim_nav.wx;
gyroAux.wy = stim_nav.wy;
gyroAux.wz = stim_nav.wz;

gyroData.Calibrated.wx = zeros(1, length(stim_nav.wx));
gyroData.Calibrated.wy = zeros(1, length(stim_nav.wy));
gyroData.Calibrated.wz = zeros(1, length(stim_nav.wz));

for i=1:length(gyroAux.wx)
    Aaux = Htil_stim_gyros\(FE_stim_gyros*[gyroAux.wx(i);gyroAux.wy(i);gyroAux.wz(i)]-bias_stim_gyros);
    gyroData.Calibrated.wx(i) = Aaux(1);
    gyroData.Calibrated.wy(i) = Aaux(2);
    gyroData.Calibrated.wz(i) = Aaux(3);
end

%% Resampling

desiredFs = 500;
[body.omegaResampled(1, :), table.timesResampled] = resample(body.omega(1, :), table.times, desiredFs);
[body.omegaResampled(2, :), table.timesResampled] = resample(body.omega(2, :), table.times, desiredFs);
[body.omegaResampled(3, :), table.timesResampled] = resample(body.omega(3, :), table.times, desiredFs);

[gyroData.Calibrated.wxResampled, stim_nav.timesResampled] = resample(gyroData.Calibrated.wx, stim_nav.times, desiredFs);
[gyroData.Calibrated.wyResampled, stim_nav.timesResampled] = resample(gyroData.Calibrated.wy, stim_nav.times, desiredFs);
[gyroData.Calibrated.wzResampled, stim_nav.timesResampled] = resample(gyroData.Calibrated.wz, stim_nav.times, desiredFs);

[accData.Calibrated.axResampled, stim_nav.timesResampled] = resample(accData.Calibrated.ax, stim_nav.times, desiredFs);
[accData.Calibrated.ayResampled, stim_nav.timesResampled] = resample(accData.Calibrated.ay, stim_nav.times, desiredFs);
[accData.Calibrated.azResampled, stim_nav.timesResampled] = resample(accData.Calibrated.az, stim_nav.times, desiredFs);

bodyStimDifference = 30796 - 26362 + 1;
% ahrsInitialPoint = 5167;
% tableInitialPoint = 30776;
% stimInitialPoint = 26344;

finalPoint = length(body.omegaResampled)-bodyStimDifference+1;

timesAux = table.timesResampled - table.timesResampled(bodyStimDifference);

times = timesAux(bodyStimDifference:end);

gyroData.Measured.wx = gyroData.Calibrated.wxResampled(1:finalPoint);
gyroData.Measured.wy = gyroData.Calibrated.wyResampled(1:finalPoint);
gyroData.Measured.wz = gyroData.Calibrated.wzResampled(1:finalPoint);

accData.Measured.ax = accData.Calibrated.axResampled(1:finalPoint);
accData.Measured.ay = accData.Calibrated.ayResampled(1:finalPoint);
accData.Measured.az = accData.Calibrated.azResampled(1:finalPoint);

body.omegaReal(1, :) = body.omegaResampled(1, bodyStimDifference:end);
body.omegaReal(2, :) = body.omegaResampled(2, bodyStimDifference:end);
body.omegaReal(3, :) = body.omegaResampled(3, bodyStimDifference:end);


% plot(times, body.omegaReal(1, :), times, gyroData.Measured.wx);


% plot(table.timesResampled(bodyStimDifference:end), body.omegaResampled(1, bodyStimDifference:end), stim_nav.timesResampled(1:finalPoint), gyroData.Calibrated.wxResampled(1:finalPoint));

%% Propagating magnetometer vector with the absolute measure of body angular speeds

% Initializations
magNoiseX = 0.0062;
magNoiseY = 0.0071;
magNoiseZ = 0.0067;

ahrsInitialPoint = 1;
initialMag = [0.5311; 0.6189; 0.5788];
initialMag = initialMag/norm(initialMag);
stimInitialPoint = 1;
initialAcc = [accData.Measured.ax(1); accData.Measured.ay(1); accData.Measured.az(1)];

loopSize = length(times);
timeStep = times(2) - times(1);

magData.Propagated.mx = zeros(1, loopSize);
magData.Propagated.my = zeros(1, loopSize);
magData.Propagated.mz = zeros(1, loopSize);

accData.Propagated.ax = zeros(1, loopSize);
accData.Propagated.ay = zeros(1, loopSize);
accData.Propagated.az = zeros(1, loopSize);

body.quaternions = zeros(4, loopSize);

% Main loop
for k = 1:loopSize
    if k==1
        body.quaternions(:, k) = [0 0 0 1]';
    else
        [body.quaternions(:, k), ~] = quaternionPropagation(body.quaternions(:, k-1), body.omegaReal(:,k)', timeStep);
    end
    
    DCM = quaternion2DCM(body.quaternions(:, k));
    
    auxMag = DCM*initialMag;

    magData.Propagated.mx(k) = auxMag(1);
    magData.Propagated.my(k) = auxMag(2);
    magData.Propagated.mz(k) = auxMag(3);
    
    
    magData.Measured.mx(k) = auxMag(1) + magNoiseX*randn;
    magData.Measured.my(k) = auxMag(2) + magNoiseY*randn;
    magData.Measured.mz(k) = auxMag(3) + magNoiseZ*randn;
    
    
    accAux = DCM*initialAcc;

    accData.Propagated.ax(k) = accAux(1);
    accData.Propagated.ay(k) = accAux(2);
    accData.Propagated.az(k) = accAux(3);
end


% figure();
% plot(accData.Propagated.ax); hold on; plot(accData.Measured.ax);
% figure();
% plot(accData.Propagated.ay); hold on; plot(accData.Measured.ay);
% figure();
% plot(accData.Propagated.az); hold on; plot(accData.Measured.az);