classdef Accelerometer
    %Accelerometer: Classe que armazena os dados lidos pelo aceler�metro
    
    properties
        ax
        ay
        az
        totalPoints
    end
    
    methods
        function obj = Accelerometer(ax,ay,az)
            %Accelerometer: Constr�i uma inst�ncia da classe
            %   Pode receber tanto uma tr�ade individual como um conjunto
            %   de n tr�ades de acelera��es
            obj.ax = ax;
            obj.ay = ay;
            obj.az = az;
            obj.totalPoints = length(ax);
        end
        
        function obj = addPoint(obj,ax,ay,az)
            %addPoint: Adiciona uma tr�ade de acelera��es na inst�ncia da
            %classe.
            obj.totalPoints = obj.totalPoints+1;
            obj.ax(obj.totalPoints) = ax;
            obj.ay(obj.totalPoints) = ay;
            obj.az(obj.totalPoints) = az;
        end
        function plotData(obj)
            %plotData Plota os gr�ficos das tr�s componentes de acelera��o
            %armazenadas na classe.
            figure()
            hold on;
            plot(obj.ax);
            hold on;
            plot(obj.ay);
            hold on;
            plot(obj.az);
            legend('a_x', 'a_y ', 'a_z');
            grid on;
        end
    end
end