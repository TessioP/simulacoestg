%% Plot das velocidades angulares reais

lineWidth = 1.5;
fontSize = 14;

colors = linspecer(3);


figure()
plot(times, body.omegaReal(1, :), 'Color' ,colors(1,:), 'LineWidth', lineWidth);
legend('\omega_x')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Velocidade [rad/s]', 'FontSize', fontSize)
xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'MESA-wx.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');

figure()
plot(times, body.omegaReal(2, :), 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend('\omega_y')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Velocidade [rad/s]', 'FontSize', fontSize)
xlim([50 415])
grid on
fileName = 'MESA-wy.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');

figure()
plot(times, body.omegaReal(3, :), 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('\omega_z')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Velocidade [rad/s]', 'FontSize', fontSize)
xlim([50 415])
ylim([-0.4, 0.8])
grid on
fileName = 'MESA-wz.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');

%% Plot da leitura dos gir�metros
colors = linspecer(3);
figure()
plot(times, gyroData.Measured.wx, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
legend('\omega_x')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Velocidade [rad/s]', 'FontSize', fontSize)
xlim([50 415])
grid on
fileName = 'Measured-wx.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');

figure()
plot(times, gyroData.Measured.wy, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend('\omega_y')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Velocidade [rad/s]', 'FontSize', fontSize)
xlim([50 415])
grid on
fileName = 'Measured-wy.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');

figure()
plot(times, gyroData.Measured.wz, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('\omega_z')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Velocidade [rad/s]', 'FontSize', fontSize)
xlim([50 415])
ylim([-0.4, 0.8])
grid on
fileName = 'Measured-wz.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');


colors = linspecer(2);
figure()
plot(times, gyroData.Measured.wx, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, body.omegaReal(1, :), 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend('\omega_{x-measured}', '\omega_{x-real}')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Velocidade [rad/s]', 'FontSize', fontSize)
xlim([50 415])
grid on
fileName = 'Measured-Real-wx.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');
xlim([52 59])
ylim([-0.2 0.2])
grid on
fileName = 'Measured-Real-wx-zoom.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');



figure()
plot(times, gyroData.Measured.wy, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, body.omegaReal(2, :), 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend('\omega_{y-measured}', '\omega_{y-real}')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Velocidade [rad/s]', 'FontSize', fontSize)
xlim([50 415])
grid on
fileName = 'Measured-Real-wy.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');
xlim([54 59])
ylim([0.3 0.7])
grid on
fileName = 'Measured-Real-wy-zoom.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');




figure()
plot(times, gyroData.Measured.wz, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, body.omegaReal(3, :), 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend('\omega_{z-measured}', '\omega_{z-real}')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Velocidade [rad/s]', 'FontSize', fontSize)
xlim([50 415])
grid on
fileName = 'Measured-Real-wz.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');
xlim([58 64])
ylim([-0.4 0.1])
grid on
fileName = 'Measured-Real-wz-zoom.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');

%% Gr�ficos do magnet�metro

lineWidth = 1;
fontSize = 14;

colors = linspecer(3);

figure()
plot(times, magData.Measured.mx, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
legend('m_x')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Componente X do vetor campo magn�tico', 'FontSize', fontSize)
xlim([50 415])
grid on
fileName = 'Measured-mx.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');

figure()
plot(times, magData.Measured.my, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend('m_y')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Componente Y do vetor campo magn�tico', 'FontSize', fontSize)
xlim([50 415])
grid on
fileName = 'Measured-my.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');

figure()
plot(times, magData.Measured.mz, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('m_z')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Componente Z do vetor campo magn�tico', 'FontSize', fontSize)
xlim([50 415])
grid on
fileName = 'Measured-mz.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');

%% Gr�ficos do aceler�metro

figure()
plot(times, accData.Measured.ax, 'Color' ,colors(1,:), 'LineWidth', lineWidth)
hold on;
plot(times, accData.Propagated.ax, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
grid on;
legend('Medido', 'Propagado')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Velocidade [rad/s]', 'FontSize', fontSize)
ylim([-10, 10])
xlim([50 415])
fileName = 'Propagated-ax.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');
xlim([50 100])
fileName = 'Propagated-ax-zoom.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');

%% Gr�ficos dos angulos de euler
lineWidth = 1;

figure()
plot(times, 180/pi*pitchReal, 'Color' ,colors(1,:), 'LineWidth', lineWidth)
hold on;
plot(times, 180/pi*pitchEstimated, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
grid on;
legend('Refer�ncia', 'Estimado')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Arfagem [^o]', 'FontSize', fontSize)
% fileName = 'Generated-pitch.eps';
% print('-depsc', sprintf(fileName));
% movefile(fileName,'C:\ITA\tg\Cap3');
xlim([0 2])
% fileName = 'Generated-pitch-zoom.eps';
% print('-depsc', sprintf(fileName));
% movefile(fileName,'C:\ITA\tg\Cap3');

figure()
plot(times, 180/pi*rollReal, 'Color' ,colors(1,:), 'LineWidth', lineWidth)
hold on;
plot(times, 180/pi*rollEstimated, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
grid on;
legend('Refer�ncia', 'Estimado')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Rolamento [^o]', 'FontSize', fontSize)
% fileName = 'Generated-roll.eps';
% print('-depsc', sprintf(fileName));
% movefile(fileName,'C:\ITA\tg\Cap3');
xlim([0 2])
% fileName = 'Generated-roll-zoom.eps';
% print('-depsc', sprintf(fileName));
% movefile(fileName,'C:\ITA\tg\Cap3');

figure()
plot(times, 180/pi*rollReal, 'Color' ,colors(1,:), 'LineWidth', lineWidth)
hold on;
plot(times, 180/pi*rollEstimated, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
grid on;
legend('Refer�ncia', 'Estimado')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Guinada [^o]', 'FontSize', fontSize)
% fileName = 'Generated-yaw.eps';
% print('-depsc', sprintf(fileName));
% movefile(fileName,'C:\ITA\tg\Cap3');
xlim([0 2])
% fileName = 'Generated-yaw-zoom.eps';
% print('-depsc', sprintf(fileName));
% movefile(fileName,'C:\ITA\tg\Cap3');
%%

figure()
plot(times, 180/pi*(pitchReal - pitchEstimated), 'Color' ,colors(1,:), 'LineWidth', lineWidth)
hold on;
plot(times, 180/pi*(rollReal - rollEstimated), 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
plot(times, 180/pi*(yawReal - yawEstimated), 'Color' ,colors(3,:), 'LineWidth', lineWidth);
grid on;
legend('Arfagem', 'Rolamento', 'Guinada')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Erro [^o]', 'FontSize', fontSize)
fileName = 'Generated-erro.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');
xlim([0 6])
ylim([-110 20])
fileName = 'Generated-erro-zoom.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');


%% Gr�ficos dos angulos de euler para o caso sem filtro
lineWidth = 1;

figure()
plot(times, 180/pi*pitchReal, 'Color' ,colors(1,:), 'LineWidth', lineWidth)
hold on;
plot(times, 180/pi*pitchEstimated, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
grid on;
legend('Refer�ncia', 'Estimado')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Arfagem [^o]', 'FontSize', fontSize)
fileName = 'Generated-pitch-semFiltro.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');
xlim([423.8 425.8])
fileName = 'Generated-pitch-zoom-semFiltro.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');

figure()
plot(times, 180/pi*rollReal, 'Color' ,colors(1,:), 'LineWidth', lineWidth)
hold on;
plot(times, 180/pi*rollEstimated, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
grid on;
legend('Refer�ncia', 'Estimado')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Rolamento [^o]', 'FontSize', fontSize)
fileName = 'Generated-roll-semFiltro.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');
xlim([423.8 425.8])
fileName = 'Generated-roll-zoom-semFiltro.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');

figure()
plot(times, 180/pi*rollReal, 'Color' ,colors(1,:), 'LineWidth', lineWidth)
hold on;
plot(times, 180/pi*rollEstimated, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
grid on;
legend('Refer�ncia', 'Estimado')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Guinada [^o]', 'FontSize', fontSize)
fileName = 'Generated-yaw-semFiltro.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');
xlim([423.8 425.8])
fileName = 'Generated-yaw-zoom-semFiltro.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap3');