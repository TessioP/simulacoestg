%% D� a express�o para a propaga��o das medidas utilizada no QEKF

syms xe1 xe2 xe3 xe4 xe5 xe6 xe7 ax ay az mx my mz Lx Ly Lz
%xe = [q1 q2 q3 q4 p q r]
D11 = 1-2*xe2^2-2*xe3^2;
D12 = 2*(xe1*xe2+xe4*xe3);
D13 = 2*(xe1*xe3-xe4*xe2);
D21 = 2*(xe1*xe2-xe4*xe3);
D22 = 1-2*xe1^2-2*xe3^2;
D23 = 2*(xe3*xe2+xe4*xe1);
D31 = 2*(xe1*xe3+xe4*xe2);
D32 = 2*(xe3*xe2-xe4*xe1);
D33 = 1-2*xe1^2-2*xe2^2;


D = [ D11 D12 D13;
      D21 D22 D23;
      D31 D32 D33];

Z(1:3, 1) = D*[ax; ay; az] - [Lx*(xe6^2 + xe7^2); Ly*(xe5^2 + xe7^2); Lz*(xe5^2 + xe6^2)];

Z(4:6, 1) = D*[mx; my; mz];

H = jacobian(Z, [xe1 xe2 xe3 xe4 xe5 xe6 xe7].')

H = simplify(H)

% f = D*[]
% F_k1 = jacobian(, [xe1 xe2 xe3 xe4].');
% F_k1 = simplify(F_k1)