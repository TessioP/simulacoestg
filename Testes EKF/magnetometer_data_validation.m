clear all; close all; clc;
%% Reading absolut values from table encoders

fileName = '28082018_mesa.csv';

[table, body] = readTableData(fileName);

%% Calibration data obtained from other scripts
Mag_align = [ 1.0232   -0.2441    0.0873;
              0.0426    1.0002   -0.0832;
             -0.0455    0.0760   -1.0127];
mag_Sf =  1.0e-08*[ 0.3564         0         0;
                         0    0.3025         0;
                         0         0    0.2477];
mag_bias = 1.0e-04*[-0.0161; 0.0321; 0.1082];

M_align_stim_acc = [    1.0000    0.0032    0.0024;
                       -0.0028    1.0000   -0.0005;
                       -0.0039   -0.0002    1.0000];
    
Sf_stim_acc = [9.8227         0         0;
                    0    9.8227         0;
                    0         0    9.8220];
                
bias_stim_acc = [ -0.0170; 0.0452; -0.0568];

Htil_stim_gyros =[ 1.0000    0.0045   -0.0071;
                  -0.0040    1.0000    0.0002;
                   0.0085   -0.0004    1.0000];

FE_stim_gyros = [ 0.0174         0         0;
                       0    0.0174         0;
                       0         0    0.0174];
                   
bias_stim_gyros = [   -0.0006; 0.0011; -0.0000];

%% Reading and calibration of magnetometer data
fileName = '28082018ahrs_nav.csv';

ahrs_nav = readAHRSdata(fileName);

% Initializations

magData.Brute.mx = ahrs_nav.mx;
magData.Brute.my = ahrs_nav.my;
magData.Brute.mz = ahrs_nav.mz;

magAux.mx = ahrs_nav.mx;
magAux.my = ahrs_nav.my;
magAux.mz = ahrs_nav.mz;

magData.Calibrated.mx = zeros(1, length(ahrs_nav.mx));
magData.Calibrated.my = zeros(1, length(ahrs_nav.my));
magData.Calibrated.mz = zeros(1, length(ahrs_nav.mz));

% Main loop of calibration

for i=1:length(magAux.mx)
    Maux = Mag_align*(mag_Sf*[magAux.mx(i);magAux.my(i);magAux.mz(i)]-mag_bias);
    magData.Calibrated.mx(i) = Maux(1)/norm(Maux);
    magData.Calibrated.my(i) = Maux(2)/norm(Maux);
    magData.Calibrated.mz(i) = Maux(3)/norm(Maux);
end

%% Reading and calibration of STIM data

fileName = '28082018stim_nav.csv';
% fileName = '28082018stim_giros.csv';
stim_nav = readSTIMdata(fileName);

% plotNonCalibredSTIMGraphics(lineWidth, fontSize, stim_nav);

accAux.ax = stim_nav.ax;
accAux.ay = stim_nav.ay;
accAux.az = stim_nav.az;

accData.Calibrated.ax = zeros(1, length(stim_nav.ax));
accData.Calibrated.ay = zeros(1, length(stim_nav.ay));
accData.Calibrated.az = zeros(1, length(stim_nav.az));

for i=1:length(accAux.ax)
    Aaux = M_align_stim_acc*(Sf_stim_acc*[accAux.ax(i);accAux.ay(i);accAux.az(i)]-bias_stim_acc);
    accData.Calibrated.ax(i) = Aaux(1);
    accData.Calibrated.ay(i) = Aaux(2);
    accData.Calibrated.az(i) = Aaux(3);
end

gyroAux.wx = stim_nav.wx;
gyroAux.wy = stim_nav.wy;
gyroAux.wz = stim_nav.wz;

gyroData.Calibrated.wx = zeros(1, length(stim_nav.wx));
gyroData.Calibrated.wy = zeros(1, length(stim_nav.wy));
gyroData.Calibrated.wz = zeros(1, length(stim_nav.wz));

for i=1:length(gyroAux.wx)
    Aaux = Htil_stim_gyros\(FE_stim_gyros*[gyroAux.wx(i);gyroAux.wy(i);gyroAux.wz(i)]-bias_stim_gyros);
    gyroData.Calibrated.wx(i) = Aaux(1);
    gyroData.Calibrated.wy(i) = Aaux(2);
    gyroData.Calibrated.wz(i) = Aaux(3);
end

%% Resampling

desiredFs = 500;
[body.omegaResampled(1, :), table.timesResampled] = resample(body.omega(1, :), table.times, desiredFs);
[body.omegaResampled(2, :), table.timesResampled] = resample(body.omega(2, :), table.times, desiredFs);
[body.omegaResampled(3, :), table.timesResampled] = resample(body.omega(3, :), table.times, desiredFs);

[gyroData.Calibrated.wxResampled, stim_nav.timesResampled] = resample(gyroData.Calibrated.wx, stim_nav.times, desiredFs);
[gyroData.Calibrated.wyResampled, stim_nav.timesResampled] = resample(gyroData.Calibrated.wy, stim_nav.times, desiredFs);
[gyroData.Calibrated.wzResampled, stim_nav.timesResampled] = resample(gyroData.Calibrated.wz, stim_nav.times, desiredFs);

[accData.Calibrated.axResampled, stim_nav.timesResampled] = resample(accData.Calibrated.ax, stim_nav.times, desiredFs);
[accData.Calibrated.ayResampled, stim_nav.timesResampled] = resample(accData.Calibrated.ay, stim_nav.times, desiredFs);
[accData.Calibrated.azResampled, stim_nav.timesResampled] = resample(accData.Calibrated.az, stim_nav.times, desiredFs);

[magData.Calibrated.mxResampled, ahrs_nav.timesResampled] = resample(magData.Calibrated.mx, ahrs_nav.times, desiredFs);
[magData.Calibrated.myResampled, ahrs_nav.timesResampled] = resample(magData.Calibrated.my, ahrs_nav.times, desiredFs);
[magData.Calibrated.mzResampled, ahrs_nav.timesResampled] = resample(magData.Calibrated.mz, ahrs_nav.times, desiredFs);

[magData.Brute.mxResampled, ahrs_nav.timesResampled] = resample(magData.Brute.mx, ahrs_nav.times, desiredFs);
[magData.Brute.myResampled, ahrs_nav.timesResampled] = resample(magData.Brute.my, ahrs_nav.times, desiredFs);
[magData.Brute.mzResampled, ahrs_nav.timesResampled] = resample(magData.Brute.mz, ahrs_nav.times, desiredFs);
% 
% axStd = std(accData.Calibrated.axResampled(1:500))
% ayStd = std(accData.Calibrated.ayResampled(1:500))
% azStd = std(accData.Calibrated.azResampled(1:500))
% mxStd = std(magData.Calibrated.mxResampled(1:500))
% myStd = std(magData.Calibrated.myResampled(1:500))
% mzStd = std(magData.Calibrated.mzResampled(1:500))
% wxStd = std(gyroData.Calibrated.wxResampled(1:500))
% wyStd = std(gyroData.Calibrated.wyResampled(1:500))
% wzStd = std(gyroData.Calibrated.wzResampled(1:500))




bodyStimDifference = 30796 - 26362 + 1;
% ahrsInitialPoint = 5167;
% tableInitialPoint = 30776;
% stimInitialPoint = 26344;

finalPoint = length(body.omegaResampled)-bodyStimDifference+1;

table.timesResampled = table.timesResampled - table.timesResampled(bodyStimDifference);

% stim_nav.timesResampled = stim_nav.timesResampled - stim_nav.timesResampled(stimInitialPoint);

plot(table.timesResampled(bodyStimDifference:end), body.omegaResampled(1, bodyStimDifference:end), stim_nav.timesResampled(1:finalPoint), gyroData.Calibrated.wxResampled(1:finalPoint));

%% Propagating magnetometer vector with the absolute measure of body angular speeds

% Initializations


% stim_nav.times = stim_nav.times - stim_nav.times(stimInitialPoint);
% table.times = table.times - table.times(tableInitialPoint);
% ahrs_nav.times = ahrs_nav.times - ahrs_nav.times(ahrsInitialPoint);
ahrsInitialPoint = 1;
initialMag = [magData.Brute.mx(ahrsInitialPoint); magData.Brute.my(ahrsInitialPoint); magData.Brute.mz(ahrsInitialPoint)];
stimInitialPoint = 1;
initialAcc = [accData.Calibrated.axResampled(stimInitialPoint); accData.Calibrated.ayResampled(stimInitialPoint); accData.Calibrated.azResampled(stimInitialPoint)];

loopSize = finalPoint;
timeStep = table.timesResampled(2) - table.timesResampled(1);

magData.Propagated.mx = zeros(1, loopSize);
magData.Propagated.my = zeros(1, loopSize);
magData.Propagated.mz = zeros(1, loopSize);

accData.Propagated.ax = zeros(1, loopSize);
accData.Propagated.ay = zeros(1, loopSize);
accData.Propagated.az = zeros(1, loopSize);

body.quaternions = zeros(4, loopSize);

% AHRS loop
for k = 1:loopSize
    if k==1
        body.quaternions(:, k) = [0 0 0 1]';
    else
        [body.quaternions(:, k), ~] = quaternionPropagation(body.quaternions(:, k-1), body.omegaResampled(:,k+bodyStimDifference-1)', timeStep);
    end
    
    DCM = quaternion2DCM(body.quaternions(:, k));
    
    auxMag = DCM\[magData.Brute.mxResampled(k); magData.Brute.myResampled(k); magData.Brute.mzResampled(k)];
    
    alpha(k) = acosd(dot(auxMag, [1; 0; 0])/norm(auxMag));
    beta(k) = acosd(dot(auxMag, [0; 1; 0])/norm(auxMag));
    gama(k) = acosd(dot(auxMag, [0; 0; 1])/norm(auxMag));
%     magData.Propagated.mx(k) = auxMag(1);
%     magData.Propagated.my(k) = auxMag(2);
%     magData.Propagated.mz(k) = auxMag(3);
    
    

    accAux = DCM\[accData.Calibrated.axResampled(k); accData.Calibrated.ayResampled(k); accData.Calibrated.azResampled(k)];
    alpha_acc(k) = acosd(dot(accAux, [1; 0; 0])/norm(accAux));
    beta_acc(k) = acosd(dot(accAux, [0; 1; 0])/norm(accAux));
    gama_acc(k) = acosd(dot(accAux, [0; 0; 1])/norm(accAux));
%     accData.Propagated.ax(k) = accAux(1);
%     accData.Propagated.ay(k) = accAux(2);
%     accData.Propagated.az(k) = accAux(3);
%     
end

%% Magnetometer graphics
colors = linspecer(3);
lineWidth = 2;
fontSize = 14;
figure()
plot(ahrs_nav.times(ahrsInitialPoint:end), magData.Brute.mx(ahrsInitialPoint:end),'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
legend({'m_x_{calibrated}'}, 'FontSize', fontSize);
grid on
title('M_x calibrated')
xlabel('Tempo', 'FontSize', fontSize)
ylabel('M_x', 'FontSize', fontSize)

figure()
plot(table.times(tableInitialPoint:tableInitialPoint+loopSize-1), magData.Propagated.mx(1:loopSize), 'Color', colors(2,:), 'LineWidth', lineWidth);
hold on;
hold on;
legend({'m_x_{propagated}'}, 'FontSize', fontSize);
grid on
title('M_x propagated')
xlabel('Tempo', 'FontSize', fontSize)
ylabel('M_x', 'FontSize', fontSize)


figure()
plot(ahrs_nav.times(ahrsInitialPoint:end), magData.Brute.my(ahrsInitialPoint:end),'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
legend({'m_y_{calibrated}'}, 'FontSize', fontSize);
grid on
title('M_y calibrated')
xlabel('Tempo', 'FontSize', fontSize)
ylabel('M_y', 'FontSize', fontSize)

figure()
plot(table.times(tableInitialPoint:tableInitialPoint+loopSize-1), magData.Propagated.my(1:loopSize), 'Color', colors(2,:), 'LineWidth', lineWidth);
hold on;
hold on;
legend({'m_y_{propagated}'}, 'FontSize', fontSize);
grid on
title('M_y propagated')
xlabel('Tempo', 'FontSize', fontSize)
ylabel('M_y', 'FontSize', fontSize)


figure()
plot(ahrs_nav.times(ahrsInitialPoint:end), magData.Brute.mz(ahrsInitialPoint:end),'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
legend({'m_z_{calibrated}'}, 'FontSize', fontSize);
grid on
title('M_z calibrated')
xlabel('Tempo', 'FontSize', fontSize)
ylabel('M_z', 'FontSize', fontSize)

figure()
plot(table.times(tableInitialPoint:tableInitialPoint+loopSize-1), magData.Propagated.mz(1:loopSize), 'Color', colors(2,:), 'LineWidth', lineWidth);
hold on;
hold on;
legend({'m_z_{propagated}'}, 'FontSize', fontSize);
grid on
title('M_z propagated')
xlabel('Tempo', 'FontSize', fontSize)
ylabel('M_z', 'FontSize', fontSize)

%% Accelerometer graphics


colors = linspecer(3);
lineWidth = 2;
fontSize = 14;
figure()
plot(stim_nav.times(stimInitialPoint:end), accData.Calibrated.ax(stimInitialPoint:end),'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
legend({'a_x_{calibrated}'}, 'FontSize', fontSize);
grid on
title('a_x calibrated')
xlabel('Tempo', 'FontSize', fontSize)
ylabel('a_x', 'FontSize', fontSize)

figure()
plot(table.times(tableInitialPoint:tableInitialPoint+loopSize-1), accData.Propagated.ax(1:loopSize), 'Color', colors(2,:), 'LineWidth', lineWidth);
hold on;
legend({'a_x_{propagated}'}, 'FontSize', fontSize);
grid on
title('a_x propagated')
xlabel('Tempo', 'FontSize', fontSize)
ylabel('a_x', 'FontSize', fontSize)

figure()
plot(stim_nav.times(stimInitialPoint:end), accData.Calibrated.ax(stimInitialPoint:end),'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
legend({'a_y_{calibrated}'}, 'FontSize', fontSize);
grid on
title('a_y calibrated')
xlabel('Tempo', 'FontSize', fontSize)
ylabel('a_y', 'FontSize', fontSize)

figure()
plot(table.times(tableInitialPoint:tableInitialPoint+loopSize-1), accData.Propagated.ax(1:loopSize), 'Color', colors(2,:), 'LineWidth', lineWidth);
hold on;
legend({'a_y_{propagated}'}, 'FontSize', fontSize);
grid on
title('a_y propagated')
xlabel('Tempo', 'FontSize', fontSize)
ylabel('a_y', 'FontSize', fontSize)

figure()
plot(stim_nav.times(stimInitialPoint:end), accData.Calibrated.az(stimInitialPoint:end),'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
legend({'a_z_{calibrated}'}, 'FontSize', fontSize);
grid on
title('a_z calibrated')
xlabel('Tempo', 'FontSize', fontSize)
ylabel('a_z', 'FontSize', fontSize)

figure()
plot(table.times(tableInitialPoint:tableInitialPoint+loopSize-1), accData.Propagated.az(1:loopSize), 'Color', colors(2,:), 'LineWidth', lineWidth);
hold on;
legend({'a_z_{propagated}'}, 'FontSize', fontSize);
grid on
title('a_z propagated')
xlabel('Tempo', 'FontSize', fontSize)
ylabel('a_z', 'FontSize', fontSize)