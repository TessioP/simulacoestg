%% Read data, calbriate and generate magnetometer data
% run('magDataGeneration.m');

%% Ru�dos utilizados no filtro

axStd = 0.0230;
ayStd = 0.0197;
azStd = 0.0716;

mxStd = 0.0062;
myStd = 0.0071;
mzStd = 0.0067;

wxStd = 0.0013;
wyStd = 0.0014;
wzStd = 0.0013;

R_init = diag([std(accData.Measured.ax(1:500))^2,std(accData.Measured.ay(1:500))^2,std(accData.Measured.az(1:500))^2, mxStd^2,myStd^2,mzStd^2]);

Q_init = diag([std(gyroData.Measured.wx(1:500))^2,std(gyroData.Measured.wy(1:500))^2,std(gyroData.Measured.wz(1:500))^2]);

%% Condi��es iniciais do sistema
P(:,:,1)=1e5*eye(4); % Covariância do erro de estimação no instante zero
P_prop(:,:,1)=1e5*eye(4); % Covariância do erro de estimação no instante zero

%% Condi��es iniciais do filtro

% In�cio: X South; Y East; Z Up;
gyroData.Estimated.wx = zeros(1, length(times));
gyroData.Estimated.wy = zeros(1, length(times));
gyroData.Estimated.wz = zeros(1, length(times)); 

accData.Estimated.ax = zeros(1, length(times));
accData.Estimated.ay = zeros(1, length(times));
accData.Estimated.az = zeros(1, length(times));

magData.Estimated.mx = zeros(1, length(times));
magData.Estimated.my = zeros(1, length(times));
magData.Estimated.mz = zeros(1, length(times));

quaternions.Estimated.q1 = zeros(1, length(times));
quaternions.Estimated.q2 = zeros(1, length(times));
quaternions.Estimated.q3 = zeros(1, length(times));
quaternions.Estimated.q4 = zeros(1, length(times));

quaternions.Propagated.q1 = zeros(1, length(times));
quaternions.Propagated.q2 = zeros(1, length(times));
quaternions.Propagated.q3 = zeros(1, length(times));
quaternions.Propagated.q4 = zeros(1, length(times));

qAux = angle2quat(deg2rad(90),deg2rad(45),deg2rad(45), 'YZX');
% qAux = angle2quat(deg2rad(0), deg2rad(0), deg2rad(0), 'YZX');

quaternions.Estimated.q1(1) = qAux(2);
quaternions.Estimated.q2(1) = qAux(3);
quaternions.Estimated.q3(1) = qAux(4);
quaternions.Estimated.q4(1) = qAux(1);

quaternions.Propagated.q1(1) = qAux(2);
quaternions.Propagated.q2(1) = qAux(3);
quaternions.Propagated.q3(1) = qAux(4);
quaternions.Propagated.q4(1) = qAux(1);


gyroData.Estimated.wx(1) = gyroData.Measured.wx(1);
gyroData.Estimated.wy(1) = gyroData.Measured.wy(1);
gyroData.Estimated.wz(1) = gyroData.Measured.wz(1);

accData.Estimated.ax(1) = accData.Measured.ax(1);
accData.Estimated.ay(1) = accData.Measured.ay(1);
accData.Estimated.az(1) = accData.Measured.az(1);

magData.Estimated.mx(1) = magData.Measured.mx(1);
magData.Estimated.my(1) = magData.Measured.my(1);
magData.Estimated.mz(1) = magData.Measured.mz(1);


%% Filtro de Kalman
step = times(2) - times(1);
timesLength = length(times);
for k=1:(timesLength-2)
    
    q_k = [quaternions.Estimated.q1(k); quaternions.Estimated.q2(k); quaternions.Estimated.q3(k); quaternions.Estimated.q4(k)];
    q_k_prop = [quaternions.Propagated.q1(k); quaternions.Propagated.q2(k); quaternions.Propagated.q3(k); quaternions.Propagated.q4(k)];
    
    u_k = [gyroData.Measured.wx(k); gyroData.Measured.wy(k); gyroData.Measured.wz(k)];
    x_k = [q_k; u_k];
    x_k_prop = [q_k_prop; u_k];
    
    zMedido_k1 = [accData.Measured.ax(k+1); accData.Measured.ay(k+1); accData.Measured.az(k+1); magData.Measured.mx(k+1); magData.Measured.my(k+1); magData.Measured.mz(k+1)];
    
    [x_k1, P(:,:,k+1), ze_k1, z_k1] = extendedKalmanFilter(x_k, P(:,:,k), u_k, zMedido_k1, Q_init, R_init, initialAcc, initialMag, step, 1);
    [x_k1_prop, P_prop(:,:,k+1), ze_k1_prop, z_k1_prop] = extendedKalmanFilter(x_k_prop, P_prop(:,:,k), u_k, zMedido_k1, Q_init, R_init, initialAcc, initialMag, step, 0);
    
    quaternions.Propagated.q1(k+1) = x_k1_prop(1);
    quaternions.Propagated.q2(k+1) = x_k1_prop(2);
    quaternions.Propagated.q3(k+1) = x_k1_prop(3);
    quaternions.Propagated.q4(k+1) = x_k1_prop(4);
    
    quaternions.Estimated.q1(k+1) = x_k1(1);
    quaternions.Estimated.q2(k+1) = x_k1(2);
    quaternions.Estimated.q3(k+1) = x_k1(3);
    quaternions.Estimated.q4(k+1) = x_k1(4);
    
    accData.Estimated.ax(k+1) = ze_k1(1);
    accData.Estimated.ay(k+1) = ze_k1(2);
    accData.Estimated.az(k+1) = ze_k1(3);
    
    magData.Estimated.mx(k+1) = ze_k1(4);
    magData.Estimated.my(k+1) = ze_k1(5);
    magData.Estimated.mz(k+1) = ze_k1(6);
end


%% Plot quaternions 3D

eulerEstimated = zeros(timesLength, 3);
eulerPropagated = zeros(timesLength, 3);
eulerReal = zeros(timesLength, 3);
qEstimated = zeros(4, timesLength);
qPropagated = zeros(4, timesLength);
qReal = zeros(4, timesLength);
for k = 1:timesLength
    qReal(:, k) = body.quaternions(:, k)';
    qAux = [qReal(4, k); qReal(1:3, k)];
%     [eulerReal(k, 1), eulerReal(k, 2), eulerReal(k, 3)] = quat2angle(qAux', 'YZX');
    [pitchReal(k), yawReal(k), rollReal(k)] = quat2angle(qAux', 'YZX');
    qEstimated(:, k) = [quaternions.Estimated.q1(k), quaternions.Estimated.q2(k), quaternions.Estimated.q3(k), quaternions.Estimated.q4(k)];
    qPropagated(:, k) = [quaternions.Propagated.q1(k), quaternions.Propagated.q2(k), quaternions.Propagated.q3(k), quaternions.Propagated.q4(k)];
    qAux = [qEstimated(4, k); qEstimated(1:3, k)];
%     [eulerEstimated(k, 1), eulerEstimated(k, 2), eulerEstimated(k, 3)] = quat2angle(qAux', 'YZX');
    [pitchEstimated(k), yawEstimated(k), rollEstimated(k)] = quat2angle(qAux', 'YZX');
    qAux = [qPropagated(4, k); qPropagated(1:3, k)];
    eulerPropagated(k, :) = quat2angle(qAux', 'YZX');
    
%     eulerReal(:, k) = (quaternion2Euler(qReal));
end

o  = [ 0  0  0]';

vx = [10  0  0]';

vy = [ 0 10  0]';

vz = [ 0  0 10]';
% Convert to rotation matrix - Rot converts global to local frame


plot3 = 0;

if plot3 == 1
figure()
grid on
axis([-11 11 -11 11 -11 11]);
colors = linspecer(3);
    for k = 1:50:timesLength
        Rot = quaternion2DCM(qPropagated(:,k));
        % Plottin
        
        Vx = Rot'*vx;        % rotates coordinate system
        
        Vy = Rot'*vy;
        
        Vz = Rot'*vz;
        
        eixoXEstimated = line([o(1), Vx(1)], [o(2), Vx(2)], [o(3), Vx(3)],'Color' ,colors(1,:));
        
        hold on
        
        eixoYEstimated = line([o(1), Vy(1)], [o(2), Vy(2)], [o(3), Vy(3)],'Color' ,colors(1,:));
        
        hold on
        
        eixoZEstimated = line([o(1), Vz(1)], [o(2), Vz(2)], [o(3), Vz(3)],'Color' ,colors(1,:));
        
        hold on
        
        Rot = quaternion2DCM(qReal(:,k));
        % Plotting
        
        Vx = Rot'*vx;        % rotates coordinate system
        
        Vy = Rot'*vy;
        
        Vz = Rot'*vz;
        
        eixoXProp = line([o(1), Vx(1)], [o(2), Vx(2)], [o(3), Vx(3)],'Color' ,colors(2,:));
        
        hold on
        
        eixoYProp = line([o(1), Vy(1)], [o(2), Vy(2)], [o(3), Vy(3)],'Color' ,colors(2,:));
        
        hold on
        
        eixoZProp = line([o(1), Vz(1)], [o(2), Vz(2)], [o(3), Vz(3)],'Color' ,colors(2,:));
        
        hold on
        
        drawnow
        
        delete(eixoXEstimated); delete(eixoYEstimated); delete(eixoZEstimated);
        
        delete(eixoXProp); delete(eixoYProp); delete(eixoZProp);
    end
    hold off
end

%%
colors = linspecer(2);
lineWidth = 2;
fontSize = 14;

figure()
plot(times, qPropagated(1, :),'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, qEstimated(1, :),'Color' ,colors(2,:), 'LineWidth', lineWidth);
grid on;
legend('q_1 real','q_1 estimado')

%% Calculation of quality index
for k=1:length(qReal)
   DCM1 = quaternion2DCM(qReal(:,k));
   DCM2 = quaternion2DCM(qEstimated(:,k));
   I(k) = abs(acos(0.5*(trace(transpose(DCM2)*DCM1)-1)));
   aux = transpose(DCM2)*DCM1 - eye(3);
   J(k) = trace( transpose(aux)*aux );
end

figure();plot(I)
figure();plot(J)