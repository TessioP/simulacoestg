function [] = plotQuaternions(quaternions)
%PLOTQUATERNIONS Plot a 3D graphics of a quaternium vector.
%   Receives a quaternium vector on the format q = [q1 q2 q3 q4], where q4
%   is the real component and makes a 3D plot of the attitude.

% Vectors in local frame
o  = [ 0  0  0]';

vx = [10  0  0]';

vy = [ 0 10  0]';

vz = [ 0  0 10]';
% Convert to rotation matrix - Rot converts global to local frame
figure()
axis([-11 11 -11 11 -11 11]);
grid on;
hold on;
colors = linspecer(3);
for k = 12000:max(size(quaternions))
    Rot = quaternion2DCM(quaternions(:,k));
    % Plotting
    
    Vx = Rot'*vx;        % rotates coordinate system
    
    Vy = Rot'*vy;
    
    Vz = Rot'*vz;
    
    eixoX = line([o(1), Vx(1)], [o(2), Vx(2)], [o(3), Vx(3)],'Color' ,colors(1,:)); 
    
    hold on
    
    eixoY = line([o(1), Vy(1)], [o(2), Vy(2)], [o(3), Vy(3)],'Color' ,colors(2,:)); 
    
    hold on
    
    eixoZ = line([o(1), Vz(1)], [o(2), Vz(2)], [o(3), Vz(3)],'Color' ,colors(3,:)); 
    
    hold on
    
    drawnow
    delete(eixoX); delete(eixoY); delete(eixoZ);
end
hold off
end

