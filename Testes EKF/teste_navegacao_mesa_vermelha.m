clear all; close all; clc;

%% Calibration data obtained from other scripts
tic
Mag_align = [ 1.0232   -0.2441    0.0873;
              0.0426    1.0002   -0.0832;
             -0.0455    0.0760   -1.0127];
mag_Sf =  1.0e-08*[ 0.3564         0         0;
                         0    0.3025         0;
                         0         0    0.2477];
mag_bias = 1.0e-04*[-0.0161; 0.0321; 0.1082];

M_align_stim_acc = [    1.0000    0.0032    0.0024;
                       -0.0028    1.0000   -0.0005;
                       -0.0039   -0.0002    1.0000];
    
Sf_stim_acc = [9.8227         0         0;
                    0    9.8227         0;
                    0         0    9.8220];
                
bias_stim_acc = [ -0.0170; 0.0452; -0.0568];

Htil_stim_gyros =[ 1.0000    0.0045   -0.0071;
                  -0.0040    1.0000    0.0002;
                   0.0085   -0.0004    1.0000];

FE_stim_gyros = [ 0.0174         0         0;
                       0    0.0174         0;
                       0         0    0.0174];
                   
bias_stim_gyros = [   -0.0006; 0.0011; -0.0000];
%% Reading and calibration of AHRS data
% 
fileName = '28082018ahrs_nav.csv';
% fileName = '28082018ahrs_giros.csv';

ahrs_nav = readAHRSdata(fileName);

lineWidth = 2;
fontSize = 14;

% plotNonCalibredAHRSGraphics(lineWidth, fontSize, ahrs_nav);

magAux.mx = ahrs_nav.mx;
magAux.my = ahrs_nav.my;
magAux.mz = ahrs_nav.mz;

magData.Calibrated.mx = zeros(1, length(ahrs_nav.mx));
magData.Calibrated.my = zeros(1, length(ahrs_nav.my));
magData.Calibrated.mz = zeros(1, length(ahrs_nav.mz));

for i=1:length(magAux.mx)
    Maux = Mag_align*(mag_Sf*[magAux.mx(i);magAux.my(i);magAux.mz(i)]-mag_bias);
    magData.Calibrated.mx(i) = Maux(1)/norm(Maux);
    magData.Calibrated.my(i) = Maux(2)/norm(Maux);
    magData.Calibrated.mz(i) = Maux(3)/norm(Maux);
end

%% Magnetometer graphics plot

% magData.Calibrated.plotData();
figure()
plot3(magData.Calibrated.mx, magData.Calibrated.my, magData.Calibrated.mz, '.');
axis equal;
grid on
% print('-dpng', sprintf('mag_calibrado_ahrs_nav.eps'));

nome = 'Mag XY';
figure('Name',nome)
plot(magData.Calibrated.my,magData.Calibrated.mx, '.'); axis equal
title(nome)
xlabel('Y'); ylabel('X');
grid on
nome = 'Mag YX';
% print('-dpng', sprintf('magXY_calibrado_ahrs_nav.eps'));

figure('Name',nome)
plot(magData.Calibrated.mz,magData.Calibrated.mx, '.'); axis equal
title(nome)
xlabel('Z'); ylabel('X');
grid on
nome = 'Mag ZX';
% print('-dpng', sprintf('magZX_calibrado_ahrs_nav.eps'));

figure('Name',nome)
plot(magData.Calibrated.mz,magData.Calibrated.my, '.'); axis equal
title(nome)
xlabel('Z'); ylabel('Y');
grid on
nome = 'Mag ZY';
% print('-dpng', sprintf('magZY_calibrado_ahrs_nav.eps'));

%% Reading and calibration of STIM data

fileName = '28082018stim_nav.csv';
% fileName = '28082018stim_giros.csv';
stim_nav = readSTIMdata(fileName);

% plotNonCalibredSTIMGraphics(lineWidth, fontSize, stim_nav);

accAux.ax = stim_nav.ax;
accAux.ay = stim_nav.ay;
accAux.az = stim_nav.az;

accData.Calibrated.ax = zeros(1, length(stim_nav.ax));
accData.Calibrated.ay = zeros(1, length(stim_nav.ay));
accData.Calibrated.az = zeros(1, length(stim_nav.az));

for i=1:length(accAux.ax)
    Aaux = M_align_stim_acc*(Sf_stim_acc*[accAux.ax(i);accAux.ay(i);accAux.az(i)]-bias_stim_acc);
    accData.Calibrated.ax(i) = Aaux(1);
    accData.Calibrated.ay(i) = Aaux(2);
    accData.Calibrated.az(i) = Aaux(3);
end

figure()
plot(accData.Calibrated.ax);
hold on;
plot(accData.Calibrated.ay);
hold on;
plot(accData.Calibrated.az);
hold on;
legend('a_x','a_y','a_z');
grid on;
ylim([-10, 10]);
title('Dados de acelera��o calibrados')

gyroAux.wx = stim_nav.wx;
gyroAux.wy = stim_nav.wy;
gyroAux.wz = stim_nav.wz;

gyroData.Calibrated.wx = zeros(1, length(stim_nav.wx));
gyroData.Calibrated.wy = zeros(1, length(stim_nav.wy));
gyroData.Calibrated.wz = zeros(1, length(stim_nav.wz));

for i=1:length(gyroAux.wx)
    Aaux = Htil_stim_gyros\(FE_stim_gyros*[gyroAux.wx(i);gyroAux.wy(i);gyroAux.wz(i)]-bias_stim_gyros);
    gyroData.Calibrated.wx(i) = Aaux(1);
    gyroData.Calibrated.wy(i) = Aaux(2);
    gyroData.Calibrated.wz(i) = Aaux(3);
end

figure()
plot(gyroData.Calibrated.wx);
hold on;
plot(gyroData.Calibrated.wy);
hold on;
plot(gyroData.Calibrated.wz);
hold on;
legend('\omega_x','\omega_y','\omega_z');
grid on;
title('Dados de velocidade angular calibrados')

%% Reading absolut values from table encoders

fileName = '28082018_mesa.csv';

[table, body] = readTableData(fileName);

qReal = zeros(4, length(table.times));
timeStep = table.times(2) - table.times(1);
for k = 1:length(table.times)
    if k==1
        qReal(:, k) = [0 0 0 1]';
    else
        [qReal(:, k), ~] = quaternionPropagation(qReal(:, k-1), body.omega(:,k)', timeStep);
    end
end

%% Ru�dos para a simula��o da medi��o
gyroNoise = diag([std(gyroData.Calibrated.wx(1:500))^2,std(gyroData.Calibrated.wy(1:500))^2,std(gyroData.Calibrated.wz(1:500))^2]); %covari�ncia do ru�do de medidas dos gir�metros

accNoise = diag([std(accData.Calibrated.ax(1:500))^2,std(accData.Calibrated.ay(1:500))^2,std(accData.Calibrated.az(1:500))^2]);

magNoise = diag([std(magData.Calibrated.mx(1:500))^2,std(magData.Calibrated.my(1:500))^2,std(magData.Calibrated.mz(1:500))^2]);
%% Ru�dos utilizados no filtro
R_init = diag([std(accData.Calibrated.ax(1:500))^2,std(accData.Calibrated.ay(1:500))^2,std(accData.Calibrated.az(1:500))^2, std(magData.Calibrated.mx(1:500))^2,std(magData.Calibrated.my(1:500))^2,std(magData.Calibrated.mz(1:500))^2]);

Q_init = diag([std(gyroData.Calibrated.wx(1:500))^2,std(gyroData.Calibrated.wy(1:500))^2,std(gyroData.Calibrated.wz(1:500))^2]);

%% Leitura inicial
initialAcc = [accData.Calibrated.ax(1) accData.Calibrated.ay(1) accData.Calibrated.az(1)]';

initialMag = [magData.Calibrated.mx(1) ; magData.Calibrated.my(1); magData.Calibrated.mz(1)];

%% Condi��es iniciais do sistema
P(:,:,1)=0*eye(4); % Covariância do erro de estimação no instante zero
P_prop(:,:,1)=0.01*eye(4); % Covariância do erro de estimação no instante zero

%% Condi��es iniciais do filtro

[stim_nav_out, ahrs_nav_out, R, Q, gyroData, accData, magData, times] = adaptFrequencies(stim_nav, ahrs_nav, R_init, Q_init, gyroData, accData, magData);

% times = stim_nav.times;
% gyroData.Measured = gyroData.Calibrated;
% magData.Measured = magData.Calibrated;
% accData.Measured = accData.Calibrated;

figure()
plot(accData.Measured.ax);
hold on;
plot(accData.Measured.ay);
hold on;
plot(accData.Measured.az);
hold on;
legend('a_x','a_y','a_z');
grid on;
ylim([-10, 10]);
title('Dados de acelera��o espandidos')

figure()
plot(gyroData.Measured.wx);
hold on;
plot(gyroData.Measured.wy);
hold on;
plot(gyroData.Measured.wz);
hold on;
legend('\omega_x','\omega_y','\omega_z');
grid on;
title('Dados de velocidade angular espandidos')

figure()
plot3(magData.Measured.mx, magData.Measured.my, magData.Measured.mz, '.');
axis equal;
grid on

% In�cio: X South; Y East; Z Up;


gyroData.Estimated.wx = zeros(1, length(times));
gyroData.Estimated.wy = zeros(1, length(times));
gyroData.Estimated.wz = zeros(1, length(times));

accData.Estimated.ax = zeros(1, length(times));
accData.Estimated.ay = zeros(1, length(times));
accData.Estimated.az = zeros(1, length(times));

magData.Estimated.mx = zeros(1, length(times));
magData.Estimated.my = zeros(1, length(times));
magData.Estimated.mz = zeros(1, length(times));

quaternions.Estimated.q1 = zeros(1, length(times));
quaternions.Estimated.q2 = zeros(1, length(times));
quaternions.Estimated.q3 = zeros(1, length(times));
quaternions.Estimated.q4 = zeros(1, length(times));

quaternions.Propagated.q1 = zeros(1, length(times));
quaternions.Propagated.q2 = zeros(1, length(times));
quaternions.Propagated.q3 = zeros(1, length(times));
quaternions.Propagated.q4 = zeros(1, length(times));

qAux = euler2quaternions([ahrs_nav.pitch, ahrs_nav.heading, ahrs_nav.roll]);

% quaternions.Estimated.q1(1) = qAux(1);
% quaternions.Estimated.q2(1) = qAux(2);
% quaternions.Estimated.q3(1) = qAux(3);
quaternions.Estimated.q4(1) = 1;

% quaternions.Propagated.q1(1) = qAux(1);
% quaternions.Propagated.q2(1) = qAux(2);
% quaternions.Propagated.q3(1) = qAux(3);
quaternions.Propagated.q4(1) = 1;


gyroData.Estimated.wx(1) = gyroData.Measured.wx(1);
gyroData.Estimated.wy(1) = gyroData.Measured.wy(1);
gyroData.Estimated.wz(1) = gyroData.Measured.wz(1);

accData.Estimated.ax(1) = accData.Measured.ax(1);
accData.Estimated.ay(1) = accData.Measured.ay(1);
accData.Estimated.az(1) = accData.Measured.az(1);

magData.Estimated.mx(1) = magData.Measured.mx(1);
magData.Estimated.my(1) = magData.Measured.my(1);
magData.Estimated.mz(1) = magData.Measured.mz(1);


%% Filtro de Kalman
step = times(2) - times(1);
timesLength = length(times);
for k=1:(timesLength-2)
    q_k = [quaternions.Estimated.q1(k); quaternions.Estimated.q2(k); quaternions.Estimated.q3(k); quaternions.Estimated.q4(k)];
    q_k_prop = [quaternions.Propagated.q1(k); quaternions.Propagated.q2(k); quaternions.Propagated.q3(k); quaternions.Propagated.q4(k)];
    u_k = [gyroData.Measured.wx(k); gyroData.Measured.wy(k); gyroData.Measured.wz(k)];
    x_k = [q_k; u_k];
    x_k_prop = [q_k_prop; u_k];
    zMedido_k1 = [accData.Measured.ax(k+1); accData.Measured.ay(k+1); accData.Measured.az(k+1); magData.Measured.mx(k+1); magData.Measured.my(k+1); magData.Measured.mz(k+1)];
%     zMedido_k1 = [accData.Measured.ax(k+1); accData.Measured.ay(k+1); accData.Measured.az(k+1); 0;0;0];
    [x_k1, P(:,:,k+1), ze_k1, z_k1] = extendedKalmanFilter(x_k, P(:,:,k), u_k, zMedido_k1, Q_init, R_init, initialAcc, initialMag, step, 1);
    
    [x_k1_prop, P_prop(:,:,k+1), ze_k1_prop, z_k1_prop] = extendedKalmanFilter(x_k_prop, P_prop(:,:,k), u_k, zMedido_k1, Q_init, R_init, initialAcc, initialMag, step, 0);
    
    quaternions.Propagated.q1(k+1) = x_k1_prop(1);
    quaternions.Propagated.q2(k+1) = x_k1_prop(2);
    quaternions.Propagated.q3(k+1) = x_k1_prop(3);
    quaternions.Propagated.q4(k+1) = x_k1_prop(4);
    
    quaternions.Estimated.q1(k+1) = x_k1(1);
    quaternions.Estimated.q2(k+1) = x_k1(2);
    quaternions.Estimated.q3(k+1) = x_k1(3);
    quaternions.Estimated.q4(k+1) = x_k1(4);
    
    accData.Estimated.ax(k+1) = ze_k1(1);
    accData.Estimated.ay(k+1) = ze_k1(2);
    accData.Estimated.az(k+1) = ze_k1(3);
    
    magData.Estimated.mx(k+1) = ze_k1(4);
    magData.Estimated.my(k+1) = ze_k1(5);
    magData.Estimated.mz(k+1) = ze_k1(6);
end

figure()
plot(accData.Estimated.ax);
hold on;
plot(accData.Estimated.ay);
hold on;
plot(accData.Estimated.az);
hold on;
legend('a_x','a_y','a_z');
grid on;
title('Dados de acelera��o filtrados')
ylim([-10, 10]);

%% Gr�ficos dos quaternions
eulerEstimated = zeros(3, timesLength);
eulerPropagated = zeros(3, timesLength);
qEstimated = zeros(4, timesLength);
qPropagated = zeros(4, timesLength);
for k = 1:timesLength
%     qReal = [quaternions.Real.q1(k), quaternions.Real.q2(k), quaternions.Real.q3(k), quaternions.Real.q4(k)];
    qEstimated(:, k) = [quaternions.Estimated.q1(k), quaternions.Estimated.q2(k), quaternions.Estimated.q3(k), quaternions.Estimated.q4(k)];
    qPropagated(:, k) = [quaternions.Propagated.q1(k), quaternions.Propagated.q2(k), quaternions.Propagated.q3(k), quaternions.Propagated.q4(k)];
    eulerEstimated(:, k) = q2eYZX(qEstimated(:,k));
    eulerPropagated(:,k) = q2eYZX(qPropagated(:,k));
%     eulerReal(:, k) = (quaternion2Euler(qReal));
end

% [stim_nav_out, ahrs_nav_out, R, Q, gyroData, accData, magData, times] = adaptFrequencies(stim_nav, ahrs_nav, R_init, Q_init, gyroData, accData, magData);
% rollFromGravity = (180/pi)*atan2(accData.Estimated.ay, accData.Estimated.az);
% pitchFromGravity = (180/pi)*atan2(-accData.Estimated.ax, accData.Estimated.ay./sind(rollFromGravity));


colors = linspecer(3);
figure()
plot(times, eulerPropagated(1,:),'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, eulerEstimated(1,:), '--' ,'Color', colors(2,:), 'LineWidth', lineWidth);
hold on;
% plot(times, pitchFromGravity, 'Color', colors(3,:), 'LineWidth', lineWidth);
legend({'\theta_{Propagado}','\theta_{Estimado}'}, 'FontSize', fontSize);
grid on
title('Arfagem \theta (Pitch)')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Arfagem \theta [Graus]', 'FontSize', fontSize)
% print('-depsc', sprintf('QEKFSimulTheta.eps'));
% movefile('QEKFSimulTheta.eps','C:\ITA\tg\Cap3');
figure()
plot(ahrs_nav.times, ahrs_nav.pitch, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
title('Arfagem \theta (Pitch) - AHRS')
legend('pitch')
xlabel('Amostra', 'FontSize', fontSize)
ylabel('Graus [�]', 'FontSize', fontSize)
grid on


figure()
plot(times, eulerPropagated(2,:), 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, eulerEstimated(2,:), '--' , 'Color' ,colors(2,:), 'LineWidth', lineWidth);
% hold on;
% plot(times, ahrs_nav_out.heading, 'Color', colors(3,:), 'LineWidth', lineWidth);
legend({'\psi_{Propagado}','\psi_{Estimado}'}, 'FontSize', fontSize);
grid on
title('Guinada \psi (Yaw)')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Guinada \psi [Graus]', 'FontSize', fontSize)
% print('-depsc', sprintf('QEKFSimulPsi.eps'));
% movefile('QEKFSimulPsi.eps','C:\ITA\tg\Cap3');
figure()
plot(ahrs_nav.times, ahrs_nav.heading, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
legend('heading')
title('Guinada \psi (Yaw) - AHRS')
xlabel('Amostra', 'FontSize', fontSize)
ylabel('Graus [�]', 'FontSize', fontSize)
grid on




figure()
plot(times, eulerPropagated(3,:), 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, eulerEstimated(3,:), '--' , 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
% plot(times, rollFromGravity, 'Color', colors(3,:), 'LineWidth', lineWidth);
legend({'\phi_{Propagado}','\phi_{Estimado}'}, 'FontSize', fontSize);
grid on
title('Rolamento \phi (Roll)')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Rolamento \phi [Graus]', 'FontSize', fontSize)
% print('-depsc', sprintf('QEKFSimulPhi.eps'));
% movefile('QEKFSimulPhi.eps','C:\ITA\tg\Cap3');

figure()
plot(ahrs_nav.times, ahrs_nav.roll, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
hold on
title('Rolamento \phi (Roll) - AHRS')
legend('roll')
xlabel('Amostra', 'FontSize', fontSize)
ylabel('Graus [�]', 'FontSize', fontSize)
grid on
toc


%%
% Vectors in local frame

ahrsInitialPoint = 5167;
tableInitialPoint = 12311;
ekfInitialPoint = 26346;


o  = [ 0  0  0]';

vx = [10  0  0]';

vy = [ 0 10  0]';

vz = [ 0  0 10]';
% Convert to rotation matrix - Rot converts global to local frame
figure()
axis([-11 11 -11 11 -11 11]);
grid on;
hold on;
grid on;
hold on;
colors = linspecer(3);
for k = 0:10:timesLength-ekfInitialPoint
% for k = 1:10:timesLength
    Rot = quaternion2DCM(qEstimated(:,ekfInitialPoint+k/2));
%     Rot = quaternion2DCM(qEstimated(:,k));
    % Plottin
    
    Vx = Rot'*vx;        % rotates coordinate system
    
    Vy = Rot'*vy;
    
    Vz = Rot'*vz;
    
    eixoXEstimated = line([o(1), Vx(1)], [o(2), Vx(2)], [o(3), Vx(3)],'Color' ,colors(1,:)); 
    
    hold on
    
    eixoYEstimated = line([o(1), Vy(1)], [o(2), Vy(2)], [o(3), Vy(3)],'Color' ,colors(1,:)); 
    
    hold on
    
    eixoZEstimated = line([o(1), Vz(1)], [o(2), Vz(2)], [o(3), Vz(3)],'Color' ,colors(1,:)); 
    
    hold on
    
    Rot = quaternion2DCM(qReal(:,tableInitialPoint+k/5));
%     Rot = quaternion2DCM(qPropagated(:,k));
    % Plotting
    
    Vx = Rot'*vx;        % rotates coordinate system
    
    Vy = Rot'*vy;
    
    Vz = Rot'*vz;
    
    eixoXProp = line([o(1), Vx(1)], [o(2), Vx(2)], [o(3), Vx(3)],'Color' ,colors(2,:)); 
    
    hold on
    
    eixoYProp = line([o(1), Vy(1)], [o(2), Vy(2)], [o(3), Vy(3)],'Color' ,colors(2,:)); 
    
    hold on
    
    eixoZProp = line([o(1), Vz(1)], [o(2), Vz(2)], [o(3), Vz(3)],'Color' ,colors(2,:)); 
    
    hold on
    
    drawnow
    
    delete(eixoXEstimated); delete(eixoYEstimated); delete(eixoZEstimated);
    
    delete(eixoXProp); delete(eixoYProp); delete(eixoZProp);
end
hold off

