classdef Gyrometer
    %IMU Summary of this class goes here
    %   bablabla
    
    properties
        wx %omega x
        wy %omega y
        wz %omega z
        totalPoints
    end
    
    methods
        function obj = Gyrometer(wx, wy, wz)
            %IMU Construct an instance of this class
            %   Detailed explanation goes here
            obj.wx = wx;
            obj.wy = wy;
            obj.wz = wz;
            obj.totalPoints = length(wx);
        end
        
        function obj = addPoint(obj,wx,wy,wz)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            obj.totalPoints = obj.totalPoints+1;
            obj.wx(obj.totalPoints) = wx;
            obj.wy(obj.totalPoints) = wy;
            obj.wz(obj.totalPoints) = wz;
        end
        
        function plotData(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            figure()
            hold on;
            plot(obj.wx);
            hold on;
            plot(obj.wy);
            hold on;
            plot(obj.wz);
            legend('p = \omega_x', 'q = \omega_y ', 'r = \omega_z');
            grid on;
        end
    end
end

