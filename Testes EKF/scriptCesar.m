%% Calibration data obtained from other scripts

Sf_stim_acc = [9.8227         0         0;
                    0    9.8227         0;
                    0         0    9.8220];
                
bias_stim_acc = [ -0.0170; 0.0452; -0.0568];

Htil_stim_gyros =[ 1.0000    0.0045   -0.0071;
                  -0.0040    1.0000    0.0002;
                   0.0085   -0.0004    1.0000];

FE_stim_gyros = [ 0.0174         0         0;
                       0    0.0174         0;
                       0         0    0.0174];
                   
bias_stim_gyros = [   -0.0006; 0.0011; -0.0000];


%% Reading and calibration of STIM data

fileName = '28082018stim_nav.csv';
% fileName = '28082018stim_giros.csv';
stim_nav = readSTIMdata(fileName);

% plotNonCalibredSTIMGraphics(lineWidth, fontSize, stim_nav);

accAux.ax = stim_nav.ax;
accAux.ay = stim_nav.ay;
accAux.az = stim_nav.az;

accData.Calibrated.ax = zeros(1, length(stim_nav.ax));
accData.Calibrated.ay = zeros(1, length(stim_nav.ay));
accData.Calibrated.az = zeros(1, length(stim_nav.az));

for i=1:length(accAux.ax)
    Aaux = M_align_stim_acc*(Sf_stim_acc*[accAux.ax(i);accAux.ay(i);accAux.az(i)]-bias_stim_acc);
    accData.Calibrated.ax(i) = Aaux(1);
    accData.Calibrated.ay(i) = Aaux(2);
    accData.Calibrated.az(i) = Aaux(3);
end

gyroAux.wx = stim_nav.wx;
gyroAux.wy = stim_nav.wy;
gyroAux.wz = stim_nav.wz;

gyroData.Calibrated.wx = zeros(1, length(stim_nav.wx));
gyroData.Calibrated.wy = zeros(1, length(stim_nav.wy));
gyroData.Calibrated.wz = zeros(1, length(stim_nav.wz));

for i=1:length(gyroAux.wx)
    Aaux = Htil_stim_gyros\(FE_stim_gyros*[gyroAux.wx(i);gyroAux.wy(i);gyroAux.wz(i)]-bias_stim_gyros);
    gyroData.Calibrated.wx(i) = Aaux(1);
    gyroData.Calibrated.wy(i) = Aaux(2);
    gyroData.Calibrated.wz(i) = Aaux(3);
end
