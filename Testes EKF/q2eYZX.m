function [euler] = q2eYZX(q)
%Converte o quaternion (q = [0 0 0 1]) para os angulos de euler theta, psi,
%fi na ordem de rota��o YZX
%
%Autor: 1T Roberto BRUSNICKI - ACE-C
%Data: 18/09/2017


q1 = q(1);
q2 = q(2);
q3 = q(3);
q4 = q(4);

theta = real(atan2( -2*(q1*q3 - q4*q2 ), q4^2 + q1^2 - q2^2 - q3^2 ));
psi   = real(asin( 2*q1*q2 + 2*q4*q3 ));
fi    = real(atan2( -2*q2*q3 + 2*q4*q1, q4^2-q1^2+q2^2-q3^2 ));

euler = [theta; psi; fi];
end