%% Ensaio de calibra��o do aceler�metro

% %% Leitura dos dados da AHRS
% % 
% fileName = '28082018ahrs.csv';
% 
% ahrs = readAHRSdata(fileName);
% 
% lineWidth = 1.5;
% fontSize = 14;
% 
% colors = linspecer(3);
% 
% % plotNonCalibredAHRSGraphics(lineWidth, fontSize, ahrs);
% 
% figure()
% plot(ahrs.times, ahrs.mx, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
% hold on;
% plot(ahrs.times, ahrs.my, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
% hold on;
% plot(ahrs.times, ahrs.mz, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
% legend('m_x','m_y','m_z')
% xlabel('Tempo [s]', 'FontSize', fontSize)
% ylabel('Dado bruto do sensor', 'FontSize', fontSize)
% grid on
% % title('Acc AHRS');
% % print('-depsc', sprintf('MAG-magBrutosAHRS.eps'));
% % movefile('MAG-magBrutosAHRS.eps','C:\ITA\tg\Cap3');
% % xlim([15 30])
% % ylim([-4000, 5000])
% % print('-depsc', sprintf('MAG-accBrutoZoom.eps'));
% % movefile('MAG-accBrutoZoom.eps','C:\ITA\tg\Cap3');
% figure()
% plot(ahrs.times, ahrs.ax, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
% hold on;
% plot(ahrs.times, ahrs.ay, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
% hold on;
% plot(ahrs.times, ahrs.az, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
% legend('m_x','m_y','m_z')
% xlabel('Tempo [s]', 'FontSize', fontSize)
% ylabel('Dado bruto do sensor', 'FontSize', fontSize)
% grid on
% 
% interval.lower = [6000, 9750, 13400, 16700, 20500, 24000, 28000, 32000, 35300, 38500, 42300, 45500, 49100, 53000, 57000, 60263];
% interval.upper = interval.lower + 1000;
% 
% totalExperiments = 16;
% % 
% %% Separa��o das matrizes
% 
% [G, Ac, Mag] = getAHRSExperimentalMatrices(ahrs, totalExperiments, interval);
% % 
% % Campo magn�tico em NEU atrav�s do Modelo WMM2015
% % data: 02/05/18
% % mag = [13599.4; -4583.6; 10607.0]*10^(-9); %T
% %data: 28/08/18
% mag = [13584.6; -4583.8; 10622.0];%T
% m.N = mag(1);
% m.E = mag(2);
% m.U = mag(3);
% f0 = [   m.U  m.E  m.N;
%         -m.U  m.E -m.N;
%          m.U -m.N  m.E;
%         -m.U  m.N  m.E;
%         
%          m.U -m.E -m.N;
%         -m.U -m.E  m.N;
%          m.U  m.N -m.E;
%         -m.U -m.N -m.E;
%     
%          m.N  m.U  m.E;
%          m.N -m.U -m.E;
%         -m.N  m.U -m.E;
%         -m.N -m.U  m.E;
%         
%         -m.N  m.E  m.U;
%          m.N  m.E -m.U;
%          m.N -m.E  m.U;
%         -m.N -m.E -m.U;];
% 
% % f0(:, 1) = -f0(:,1);
% % f0(:, 2) = -f0(:,2);
% 
% [Mag_align, mag_bias, mag_Sf] = magnetometerCalibration(0, Mag, f0);
% 
% 
% magAux.mx = ahrs.mx;
% magAux.my = ahrs.my;
% magAux.mz = ahrs.mz;
% 
% magData.Calibrated.mx = zeros(1, length(ahrs.mx));
% magData.Calibrated.my = zeros(1, length(ahrs.my));
% magData.Calibrated.mz = zeros(1, length(ahrs.mz));
% 
% % Main loop of calibration
% 
% for i=1:length(magAux.mx)
%     Maux = Mag_align*(mag_Sf*[magAux.mx(i);magAux.my(i);magAux.mz(i)]-mag_bias);
%     magData.Calibrated.mx(i) = Maux(1);
%     magData.Calibrated.my(i) = Maux(2);
%     magData.Calibrated.mz(i) = Maux(3);
% end
% 
% 
% figure()
% plot(ahrs.times, magData.Calibrated.mx, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
% hold on;
% plot(ahrs.times, magData.Calibrated.my, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
% hold on;
% plot(ahrs.times, magData.Calibrated.mz, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
% legend('m_x','m_y','m_z')
% xlabel('Tempo [s]', 'FontSize', fontSize)
% ylabel('Campo Magn�tico [T]', 'FontSize', fontSize)
% grid on
% % title('Acc AHRS');
% print('-depsc', sprintf('MAG-magCalibradoAHRS.eps'));
% movefile('MAG-magCalibradoAHRS.eps','C:\ITA\tg\Cap3');
% % xlim([15 30])
% % ylim([-4000, 5000])
% % print('-depsc', sprintf('MAG-accBrutoZoom.eps'));
% % movefile('MAG-accBrutoZoom.eps','C:\ITA\tg\Cap3');
% %%
% clear f0;
% f0 = -9.8*[ 1   0   0;
%       -1   0   0;
%        1   0   0;
%       -1   0   0;
%       
%        1   0   0;
%       -1   0   0;
%        1   0   0;
%       -1   0   0;
%       
%        0   1   0;
%        0  -1   0;
%        0   1   0;
%        0  -1   0;
%        
%        0   0   1;
%        0   0  -1;
%        0   0   1;
%        0   0  -1];
% 
% % f0(:, 1) = -f0(:,1);
% % f0(:, 2) = -f0(:,2);
%    
% [Acc_align, acc_bias, acc_Sf] = accelerometerCalibration(0, Ac, f0);
% 
% accAux = Accelerometer(ahrs.ax, ahrs.ay, ahrs.az);
% 
% for i=1:length(accAux.ax)
%     Aaux = Acc_align*(acc_Sf*[accAux.ax(i);accAux.ay(i);accAux.az(i)]-acc_bias);
%     if i~=1
%         accDataCalibrated = accDataCalibrated.addPoint(Aaux(1), Aaux(2), Aaux(3));
%     else
%         accDataCalibrated = Accelerometer(Aaux(1), Aaux(2), Aaux(3));
%     end
% end
% accDataCalibrated.plotData();
% 
% 
% magAux = Magnetometer(ahrs.mx, ahrs.my, ahrs.mz);
% 
% for i=1:length(magAux.mx)
%     Maux = Mag_align*(mag_Sf*[magAux.mx(i);magAux.my(i);magAux.mz(i)]-mag_bias);
%     if i~=1
%         magDataCalibrated = magDataCalibrated.addPoint(Maux(1)/norm(Maux), Maux(2)/norm(Maux), Maux(3)/norm(Maux));
%     else
%         magDataCalibrated = Magnetometer(Maux(1)/norm(Maux), Maux(2)/norm(Maux), Maux(3)/norm(Maux));
%     end
% end
% magDataCalibrated.plotData();
% figure()
% plot3(magDataCalibrated.mx, magDataCalibrated.my, magDataCalibrated.mz, '.');
% axis equal;
% grid on

%% Plotting graphics

% colors = linspecer(3);
% 
% figure()
% plot(ahrs.times, magDataCalibrated.ax, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
% hold on;
% plot(ahrs.times, magDataCalibrated.ay, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
% hold on;
% plot(ahrs.times, magDataCalibrated.az, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
% legend('m_x','m_y','m_z')
% xlabel('Tempo [s] ', 'FontSize', fontSize)
% ylabel('Acelera��o [g]', 'FontSize', fontSize)
% grid on
% % title('Acc AHRS');
% print('-depsc', sprintf('ACC-accCalibradoSTIM.eps'));
% % movefile('ACC-accCalibradoSTIM.eps','C:\ITA\tg\Cap3');
% xlim([30 55])
% ylim([0.98, 1.02])
% print('-depsc', sprintf('ACC-accCalibradoSTIMPatamar.eps'));
% % movefile('ACC-accCalibradoSTIMPatamar.eps','C:\ITA\tg\Cap3');
% xlim([30 55])
% ylim([-0.03, 0.03])
% print('-depsc', sprintf('ACC-accCalibradoSTIMBias.eps'));
% % movefile('ACC-accCalibradoSTIMBias.eps','C:\ITA\tg\Cap3');

%% Leitura dos dados da STIM

fileName = '28082018-2stim.csv';

stim = readSTIMdata(fileName);

lineWidth = 1.5;

fontSize = 14;

colors = linspecer(3);

% plotNonCalibredSTIMGraphics(lineWidth, fontSize, stim);
figure()
plot(stim.times, stim.ax, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(stim.times, stim.ay, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
plot(stim.times, stim.az, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('a_x','a_y','a_z')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Dado bruto do sensor', 'FontSize', fontSize)
grid on
% title('Acc AHRS');
print('-depsc', sprintf('ACC-accBrutosSTIM.eps'));
movefile('ACC-accBrutosSTIM.eps','C:\ITA\tg\Cap3');


xlim([15 30])
ylim([0.98, 1.02])
print('-depsc', sprintf('ACC-accBrutosSTIMPatamar.eps'));
movefile('ACC-accBrutosSTIMPatamar.eps','C:\ITA\tg\Cap3');
xlim([15 30])
ylim([-0.03, 0.03])
print('-depsc', sprintf('ACC-accBrutosSTIMBias.eps'));
movefile('ACC-accBrutosSTIMBias.eps','C:\ITA\tg\Cap3');

%% Divis�o dos ensaios

interval.lower = [13750, 25000, 33650, 43200, 52000, 62000, 70100, 79000, 88500, 97000, 107000, 115350, 125000, 132500, 142000, 151750];
interval.upper = interval.lower + 1000;

totalExperiments = 16;
% 
%% Separa��o das matrizes

[G, Ac] = getStimExperimentalMatrices(stim, totalExperiments, interval);

clear f0;
f0 = [ 1   0   0;
      -1   0   0;
       1   0   0;
      -1   0   0;
      
       1   0   0;
      -1   0   0;
       1   0   0;
      -1   0   0;
      
       0   1   0;
       0  -1   0;
       0   1   0;
       0  -1   0;
       
       0   0   1;
       0   0  -1;
       0   0   1;
       0   0  -1];

[M_align_stim_acc, bias_stim_acc, Sf_stim_acc] = accelerometerCalibration(0, Ac, f0);


%% Applying calibration
accAux = Accelerometer(stim.ax, stim.ay, stim.az);

for i=1:length(accAux.ax)
    Aaux = M_align_stim_acc*(Sf_stim_acc*[accAux.ax(i);accAux.ay(i);accAux.az(i)]-bias_stim_acc);
    if i~=1
        accDataCalibrated = accDataCalibrated.addPoint(Aaux(1), Aaux(2), Aaux(3));
    else
        accDataCalibrated = Accelerometer(Aaux(1), Aaux(2), Aaux(3));
    end
end
accDataCalibrated.plotData();

%% Plotting graphics

colors = linspecer(3);

figure()
plot(stim.times, accDataCalibrated.ax, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(stim.times, accDataCalibrated.ay, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
plot(stim.times, accDataCalibrated.az, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('a_x','a_y','a_z')
xlabel('Tempo [s] ', 'FontSize', fontSize)
ylabel('Acelera��o [g]', 'FontSize', fontSize)
grid on
% title('Acc AHRS');
print('-depsc', sprintf('ACC-accCalibradoSTIM.eps'));
movefile('ACC-accCalibradoSTIM.eps','C:\ITA\tg\Cap3');
xlim([15 30])
ylim([0.98, 1.02])
print('-depsc', sprintf('ACC-accCalibradoSTIMPatamar.eps'));
movefile('ACC-accCalibradoSTIMPatamar.eps','C:\ITA\tg\Cap3');
xlim([15 30])
ylim([-0.03, 0.03])
print('-depsc', sprintf('ACC-accCalibradoSTIMBias.eps'));
movefile('ACC-accCalibradoSTIMBias.eps','C:\ITA\tg\Cap3');
