function [] = plotNonCalibredAHRSGraphics(lineWidth, fontSize, ahrs)
%PLOTNONCALIBREDSTIMGRAPHICS Summary of this function goes here
%   Detailed explanation goes here

colors = linspecer(3);

figure()
plot(ahrs.times, ahrs.wx, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(ahrs.times, ahrs.wy, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
plot(ahrs.times, ahrs.wz, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('\omega_x','\omega_y','\omega_z')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Dado bruto do sensor', 'FontSize', fontSize)
grid on
% title('Gyros AHRS');
% print('-depsc', sprintf('ACC-gyrosBrutosAHRS.eps'));
% movefile('ACC-gyrosBrutosAHRS.eps','C:\ITA\tg\Cap3');

figure()
plot(ahrs.times, ahrs.ax, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(ahrs.times, ahrs.ay, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
plot(ahrs.times, ahrs.az, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('a_x','a_y','a_z')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Dado bruto do sensor', 'FontSize', fontSize)
grid on
% title('Acc AHRS');
% print('-depsc', sprintf('ACC-accBrutosAHRS.eps'));
% movefile('ACC-accBrutosAHRS.eps','C:\ITA\tg\Cap3');



figure()
plot(ahrs.times, ahrs.mx, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(ahrs.times, ahrs.my, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
plot(ahrs.times, ahrs.mz, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('m_x','m_y','m_z')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Dado bruto do sensor', 'FontSize', fontSize)
grid on
% title('Acc AHRS');
% print('-depsc', sprintf('ACC-magBrutosAHRS.eps'));
% movefile('ACC-magBrutosAHRS.eps','C:\ITA\tg\Cap3');

figure()
plot3(ahrs.mx, ahrs.my, ahrs.mz, '.');
grid on; axis equal;

nome = 'Mag XY';
figure('Name',nome)
plot(ahrs.my,ahrs.mx, '.'); axis equal
title(nome)
xlabel('Y'); ylabel('X');
grid on
nome = 'Mag XZ';

figure('Name',nome)
plot(ahrs.mz,ahrs.mx, '.'); axis equal
title(nome)
xlabel('Z'); ylabel('X');
grid on
nome = 'Mag YZ';

figure('Name',nome)
plot(ahrs.mz,ahrs.my, '.'); axis equal
title(nome)
xlabel('Z'); ylabel('Y');
grid on

figure()
plot(ahrs.times, ahrs.heading, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(ahrs.times, ahrs.pitch, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
plot(ahrs.times, ahrs.roll, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
legend('heading','pitch','roll')
xlabel('Amostra', 'FontSize', fontSize)
ylabel('Graus [�]', 'FontSize', fontSize)
grid on
% title('headings AHRS');
% print('-depsc', sprintf('ACC-headingsAHRS.eps'));
% movefile('ACC-headingsAHRS.eps','C:\TG\Tese\tg\Cap3');

%% Gr�ficos para mostrar o bias
% xlim([30 55])
% ylim([0.98, 1.02])
% print('-depsc', sprintf('ACC-accBrutosAHRSPatamar.eps'));
% % movefile('ACC-accBrutosAHRSPatamar.eps','C:\ITA\tg\Cap3');
% xlim([30 55])
% ylim([-0.03, 0.03])
% print('-depsc', sprintf('ACC-accBrutosAHRSBias.eps'));
% % movefile('ACC-accBrutosAHRSBias.eps','C:\ITA\tg\Cap3');

end