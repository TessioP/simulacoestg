function [q] = e2qZYX(e)
%Converte angulos de euler (ZYX - psi, teta, fi) para quaternion na 
%conven��o [eixo de rota��o, angulo de rota��o] 
%
%Autor: 1T Roberto BRUSNICKI - ASE-C
%Data: 04/10/2017

psi  = pi/180*e(1);     %z
teta = pi/180*e(2);     %y
fi   = pi/180*e(3);     %x


q1 = [cos(psi/2)*cos(teta/2)*sin(fi/2)-sin(psi/2)*sin(teta/2)*cos(fi/2) ];
q2 = [cos(psi/2)*sin(teta/2)*cos(fi/2)+sin(psi/2)*cos(teta/2)*sin(fi/2) ];
q3 = [sin(psi/2)*cos(teta/2)*cos(fi/2)-cos(psi/2)*sin(teta/2)*sin(fi/2) ];
q4 = [cos(psi/2)*cos(teta/2)*cos(fi/2)+sin(psi/2)*sin(teta/2)*sin(fi/2) ];


q = [ q1; q2; q3; q4];
end