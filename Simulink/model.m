% syms J kv kp s;
% 
% sys1 = kv/J/s;
% 
% sys2 = symbolicNegativeFeedback(sys1, 1);
% sys2 = simplify(sys2);
% sys = symbolicNegativeFeedback(sys2*kp/s, 1);
% sys = simplify(sys);

% Inicialização dos parâmetros de simulação

axStd = 0.0230*10;
ayStd = 0.0197*10;
azStd = 0.0716*5;

mxStd = 0.0062*15;
myStd = 0.0071*15;
mzStd = 0.0067*15;

wxStd = 0.0013*50;
wyStd = 0.0014*50;
wzStd = 0.0013*50;

gyroNoise = [wxStd, wyStd, wzStd].^2;
accNoise = [axStd, ayStd, azStd].^2;
magNoise = [mxStd, myStd, mzStd].^2;

Q_init = diag(gyroNoise);

R_init = diag([accNoise, magNoise]);

noises = [gyroNoise', accNoise', magNoise'];

initialTheta = deg2rad(-10);
initialPsi = deg2rad(-30);
initialPhi = deg2rad(10);

% initialTheta = deg2rad(0);
% initialPsi = deg2rad(0);
% initialPhi = deg2rad(0);


DCM = angle2dcm(initialTheta, initialPsi, initialPhi, 'YZX');
qInit = angle2quat(initialTheta, initialPsi, initialPhi, 'YZX');
qInit = [qInit(2:4), qInit(1)]

thetaRef = deg2rad(0);
psiRef = deg2rad(30);
phiRef = deg2rad(0);

initialMag = DCM*[0.5311; 0.6189; 0.5788];
initialMag = initialMag/norm(initialMag);
initialAcc = DCM*[0.0076; 0.0118; 9.7735];

initialPositions = [initialAcc, initialMag];

Jx = 4.67;
Jy = 5.28;
Jz = 28.3;

tMin = 0.01;

simulationStep = 1/100;

wn = 2/2/pi;

xi = 0.7;

omegaD = wn*sqrt(1 - xi^2);

beta = acos(xi);

tr = (pi - beta)/omegaD;

sigma = xi*wn;

Mp = exp(-sigma/omegaD*pi);

kVX = 2*xi*wn*Jx;

kPX = wn^2*Jx/kVX;

kVY = 2*xi*wn*Jy;

kPY = wn^2*Jy/kVY;

kVZ = 2*xi*wn*Jz;

kPZ = wn^2*Jz/kVZ;

tauMax = 4;

pwmFrequency = 200;


