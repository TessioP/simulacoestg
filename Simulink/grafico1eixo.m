psiReal = psi.signals.values(:,1);
psiPWM = psi.signals.values(:,2);
torqueReal = torque.signals.values(:,1);
torquePWM = torque.signals.values(:,2);
times = psi.time;

lineWidth = 1.5;
fontSize = 14;

colors = linspecer(3);

figure()
plot(times, psiReal, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
legend('\psi')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Guinada [rad]', 'FontSize', fontSize)
xlim([0 40])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta1Eixo.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');


figure()
plot(times, torqueReal, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
legend('\tau')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Torque [Nm]', 'FontSize', fontSize)
xlim([0 40])
% ylim([0.98, 1.02])
grid on
fileName = 'torque1Eixo.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');

%%

psiReal = psi01s.signals.values(:,1);
psi01 = psi01s.signals.values(:,2);
psi05 = psi05s.signals.values(:,2);
psi1 = psi1s.signals.values(:,2);
psi10 = psi10s.signals.values(:,2);
psi100 = psi100s.signals.values(:,2);
psi200 = psi200s.signals.values(:,2);

MpReal = (max(psiReal)-psiReal(end))/psiReal(end)*100
Mp01 = (max(psi01)-psi01(end))/psi01(end)*100
Mp05 = (max(psi05)-psi05(end))/psi05(end)*100
Mp1 = (max(psi1)-psi1(end))/psi1(end)*100
Mp10 = (max(psi10)-psi10(end))/psi10(end)*100
Mp100 = (max(psi100)-psi100(end))/psi100(end)*100
Mp200 = (max(psi200)-psi200(end))/psi200(end)*100

torqueReal = torque01s.signals.values(:,1);
torque01 = torque01s.signals.values(:,2);
torque05 = torque05s.signals.values(:,2);
torque1 = torque1s.signals.values(:,2);
torque10 = torque10s.signals.values(:,2);
torque100 = torque100s.signals.values(:,2);
torque200 = torque200s.signals.values(:,2);

times = psi01s.time;

lineWidth = 1;
fontSize = 14;

colors = linspecer(7);

figure()
plot(times, psiReal, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, psi01, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
plot(times, psi05, 'Color' ,colors(3,:), 'LineWidth', lineWidth);
hold on;
plot(times, psi1, 'Color' ,colors(4,:), 'LineWidth', lineWidth);
hold on;
plot(times, psi10, 'Color' ,colors(5,:), 'LineWidth', lineWidth);
hold on;
plot(times, psi100, 'Color' ,colors(6,:), 'LineWidth', lineWidth);
hold on;
plot(times, psi200, 'Color' ,colors(7,:), 'LineWidth', lineWidth);
legend('\psi','\psi_{0.1Hz}','\psi_{0.5Hz}','\psi_{1Hz}','\psi_{10Hz}','\psi_{100Hz}', '\psi_{200Hz}')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Guinada [rad]', 'FontSize', fontSize)
xlim([0 40])
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'respostasFrequencia.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');

colors = linspecer(2);


figure()
plot(times, torque01, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, torqueReal, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Torque [Nm]', 'FontSize', fontSize)
xlim([0 40])
legend('\tau_{0.1Hz}', '\tau_{real}')
grid on
fileName = 'tauOut01.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');


figure()
plot(times, torque05, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, torqueReal, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Torque [Nm]', 'FontSize', fontSize)
xlim([0 40])
grid on
legend('\tau_{0.5Hz}', '\tau_{real}')
fileName = 'tauOut05.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');


figure()
plot(times, torque1, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, torqueReal, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Torque [Nm]', 'FontSize', fontSize)
xlim([0 40])
grid on
legend('\tau_{1Hz}', '\tau_{real}')
fileName = 'tauOut1.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');



figure()
plot(times, torque10, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, torqueReal, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Torque [Nm]', 'FontSize', fontSize)
xlim([0 40])
grid on
legend('\tau_{10Hz}', '\tau_{real}')
fileName = 'tauOut10.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');



figure()
plot(times, torque100, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, torqueReal, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Torque [Nm]', 'FontSize', fontSize)
xlim([0 40])
grid on
legend('\tau_{100Hz}', '\tau_{real}')
fileName = 'tauOut100.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');


figure()
plot(times, torque200, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on;
plot(times, torqueReal, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold on;
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Torque [Nm]', 'FontSize', fontSize)
xlim([0 40])
grid on
legend('\tau_{200Hz}', '\tau_{real}')
fileName = 'tauOut200.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');


%%

lineWidth = 1.5;
fontSize = 14;

colors = linspecer(2);

figure()
plot(pwmOut.time, pwmOut.signals.values(:,1), 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(pwmOut.time, pwmOut.signals.values(:,2),'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend('PWM_{out}', 'Sawtooth')
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Torque [Nm]', 'FontSize', fontSize)
xlim([0 3])
% ylim([0.98, 1.02])
grid on
fileName = 'pwmGeneratorOutput.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');