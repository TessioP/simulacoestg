function [sys,past_ydu0,str,ts] = qekf_sfunction(t, past_ydu, inputs, flag, simulationStep, Q_init, R_init, initialMag, initialAcc)

% Parameters:
% KMPC: MPC Gain Vector
% T: Sampling period
%
% Notes:
% 1) At time k, the state past_ydu of this S-function comprises the following n past measurements and (m - 1) past control moves:
%    past_ydu = [y(k - 1) ... y(k - n) du(k - 1) ... du(k - m + 1)]'       
% 2) At time k, the inputs to the S-function are yref(k), y(k)
% 3) At each simulation iteration, the output of the S-function is calculated (case 3) and then the state is updated (case 2)
%
% Author: Roberto Kawakami Harrop Galvao
% ITA - Divisao de Engenharia Eletronica
% Date: 21 March 2017

switch flag,

case 0
[sys,past_ydu0,str,ts] = mdlInitialize(simulationStep); % S-function Initialization

case 2
sys = mdlUpdate();
   
case {1,4,9}
sys = []; % Unused Flags
   
case 3 % Evaluate Function
mag = inputs(1:3);

acc = inputs(4:6);

gyro = inputs(7:9);
    
u_k = gyro;

zMedido_k1 = [acc; mag];


global x_k1 P_k1
P_k7 = reshape(past_ydu(8:56), 7, 7);
x_k = past_ydu(1:7);
[x_k1, P_k1, ~, z_k1] = qekf7States(x_k, P_k7, u_k, zMedido_k1, Q_init, R_init, initialAcc, initialMag, simulationStep, 1);
dphi = x_k1(5);
dtheta = x_k1(6);
dpsi = x_k1(7);

% Conven��o quaternio do c�digo: q = [imag real].
% Conven��o quaternio da fun��o quat2angle: q = [real imag].
q_k1 = [x_k1(4), x_k1(1:3)'];
% Conven��o euler do c�digo: theta em y (pitch), psi em z (yaw) e phi em x (roll).
% quat2angle retorna: [pitch, roll, yaw] = [theta, phi, psi]

[theta, psi, phi] = quat2angle(q_k1, 'YZX');

sys = [theta; psi; phi; dtheta; dpsi; dphi; z_k1];

end

%%%%%%%%%%%%%%%%%%%%%%%%

%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,past_ydu0,str,ts] = mdlInitialize(simulationStep)
%
% call simsizes for a sizes structure, fill it in and convert it to a
% sizes array.
%
%
sizes = simsizes;

numDiscStates = 56;

sizes.NumContStates  = 0;
sizes.NumDiscStates  = numDiscStates;
sizes.NumOutputs     = 12;
sizes.NumInputs      = 9;
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;   % Just one sample time

sys = simsizes(sizes);

%
% initialize the initial conditions
%
x_k0 = [0 0 0 1 0 0 0]';
P_k0 = reshape(0.1*eye(7), 49, 1);
past_ydu0 = [x_k0; P_k0];
%
% str is always an empty matrix
%
str = [];

%
% initialize the array of sample times
%
ts  = [simulationStep 0];


% end mdlInitializeSizes

%=======================================================================
% mdlUpdate
% Handle discrete state updates, sample time hits, and major time step
% requirements.
%=======================================================================
%
function sys = mdlUpdate()

global x_k1 P_k1
P_k1_linear = reshape(P_k1, 49, 1);
sys = [x_k1; P_k1_linear];
%end mdlUpdate