%% Gr�ficos com Ru�do Baixo
psiReal = psi.signals.values(:,1);
psiHat = psi.signals.values(:,2);
thetaReal = theta.signals.values(:,1);
thetaHat = theta.signals.values(:,2);
phiReal = phi.signals.values(:,1);
phiHat = phi.signals.values(:,2);

times = psi.time;

lineWidth = 1.5;
fontSize = 14;

colors = linspecer(2);

figure()
plot(times, psiReal*180/pi, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(times, psiHat*180/pi, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\psi', '\psi_{estimado}'},'FontSize',fontSize,'Location','northwest','NumColumns',1);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Guinada [^o]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosPsiGuinada.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');

figure()
plot(times, thetaReal*180/pi, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(times, thetaHat*180/pi, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\theta', '\theta_{estimado}'},'FontSize',fontSize,'Location','southeast','NumColumns',1);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Arfagem [^o]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosThetaArfagem.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');

figure()
plot(times, phiReal*180/pi, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(times, phiHat*180/pi, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\phi', '\phi_{estimado}'},'FontSize',fontSize,'Location','northeast','NumColumns',1);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Rolamento [^o]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosPhiRolamento.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');


%% Graficos com Ruido Alto

gyroReal = [gyroX.signals.values(:, 1), gyroY.signals.values(:, 1), gyroZ.signals.values(:, 1)];
gyroHat = [gyroX.signals.values(:, 2), gyroY.signals.values(:, 2), gyroZ.signals.values(:, 2)];

accHat = z_k1.signals.values(:, 1:3);
magHat = z_k1.signals.values(:, 4:6);
accReal = acc.signals.values;
magReal = mag.signals.values;
psiReal = psi.signals.values(:,1);
psiHat = psi.signals.values(:,2);
thetaReal = theta.signals.values(:,1);
thetaHat = theta.signals.values(:,2);
phiReal = phi.signals.values(:,1);
phiHat = phi.signals.values(:,2);

times = psi.time;

lineWidth = 1.5;
fontSize = 14;

colors = linspecer(2);

figure()
plot(times, psiReal*180/pi, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(times, psiHat*180/pi, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\psi', '\psi_{estimado}'},'FontSize',fontSize,'Location','northwest','NumColumns',1);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Guinada [^o]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosPsiGuinadaRuidoAlto.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');

figure()
plot(times, thetaReal*180/pi, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(times, thetaHat*180/pi, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\theta', '\theta_{estimado}'},'FontSize',fontSize,'Location','southeast','NumColumns',1);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Arfagem [^o]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosThetaArfagemRuidoAlto.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');

figure()
plot(times, phiReal*180/pi, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(times, phiHat*180/pi, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold off
legend({'\phi', '\phi_{estimado}'},'FontSize',fontSize,'Location','northeast','NumColumns',1);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Rolamento [^o]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosPhiRolamentoRuidoAlto.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');

figure()
p1 = plot(acc.time, accReal, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
p2 = plot(acc.time, accHat, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold off
legend([p1(1,:), p2(1,:)], {'acc', 'acc_{estimado}'},'FontSize',fontSize,'Location','northeast');
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Acelera��o [m/s^2]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosAccRuidoAlto.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');

figure()
p1 = plot(mag.time, magReal, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
p2 = plot(mag.time, magHat, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold off
% legend
legend([p1(1,:) p2(1,:)],{'mag','mag_{estimado}'} ,'FontSize',fontSize,'Location','northeast');
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Campo magn�tico', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosMagRuidoAlto.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');


figure()
p1 = plot(gyroX.time, gyroReal, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
p2 = plot(gyroX.time, gyroHat, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
hold off
% legend
legend([p1(1,:) p2(1,:)],{'gyro','gyro_{estimado}'} ,'FontSize',fontSize,'Location','northeast');
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Velocidade �ngular [rad/s]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosGyroRuidoAlto.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');


%% Graficos malha fechada

psiReal = psi.signals.values(:,1);
psiHat = psi.signals.values(:,2);
thetaReal = theta.signals.values(:,1);
thetaHat = theta.signals.values(:,2);
phiReal = phi.signals.values(:,1);
phiHat = phi.signals.values(:,2);

MpPsi = (max(psiReal)-psiReal(end))/psiReal(end)*100
% MpTheta = (max(thetaReal)-thetaReal(end))/thetaReal(end)*100
% MpPhi = (-min(psiReal)-psiReal(end))/psiReal(end)

times = psi.time;

lineWidth = 1.5;
fontSize = 14;

colors = linspecer(2);

figure()
plot(times, psiReal*180/pi, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(times, psiHat*180/pi, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\psi', '\psi_{estimado}'},'FontSize',fontSize,'Location','northwest','NumColumns',1);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Guinada [^o]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosPsiGuinadaMalhaFechada.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');

figure()
plot(times, thetaReal*180/pi, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(times, thetaHat*180/pi, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\theta', '\theta_{estimado}'},'FontSize',fontSize,'Location','southeast','NumColumns',1);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Arfagem [^o]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosThetaArfagemMalhaFechada.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');

figure()
plot(times, phiReal*180/pi, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(times, phiHat*180/pi, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\phi', '\phi_{estimado}'},'FontSize',fontSize,'Location','northeast','NumColumns',1);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Rolamento [^o]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosPhiRolamentoMalhaFechada.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');


%% Graficos malha fechada sem filtro

psiReal = psi.signals.values(:,1);
psiHat = psi.signals.values(:,2);
thetaReal = theta.signals.values(:,1);
thetaHat = theta.signals.values(:,2);
phiReal = phi.signals.values(:,1);
phiHat = phi.signals.values(:,2);

% MpPsi = (max(psiReal)-psiReal(end))/psiReal(end)*100
% MpTheta = (max(thetaReal)-thetaReal(end))/thetaReal(end)*100
% MpPhi = (-min(psiReal)-psiReal(end))/psiReal(end)

times = psi.time;

lineWidth = 1.5;
fontSize = 14;

colors = linspecer(2);

figure()
plot(times, psiReal*180/pi, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(times, psiHat*180/pi, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\psi', '\psi_{estimado}'},'FontSize',fontSize,'Location','northwest','NumColumns',1);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Guinada [^o]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosPsiGuinadaMalhaFechadaSemFiltro.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');

figure()
plot(times, thetaReal*180/pi, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(times, thetaHat*180/pi, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\theta', '\theta_{estimado}'},'FontSize',fontSize,'Location','southeast','NumColumns',1);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Arfagem [^o]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosThetaArfagemMalhaFechadaSemFiltro.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');

figure()
plot(times, phiReal*180/pi, 'Color' ,colors(1,:), 'LineWidth', lineWidth);
hold on
plot(times, phiHat*180/pi, 'Color' ,colors(2,:), 'LineWidth', lineWidth);
legend({'\phi', '\phi_{estimado}'},'FontSize',fontSize,'Location','northeast','NumColumns',1);
xlabel('Tempo [s]', 'FontSize', fontSize)
ylabel('Rolamento [^o]', 'FontSize', fontSize)
% xlim([50 415])
% ylim([0.98, 1.02])
grid on
fileName = 'resposta3EixosPhiRolamentoMalhaFechadaSemFiltro.eps';
print('-depsc', sprintf(fileName));
movefile(fileName,'C:\ITA\tg\Cap4');