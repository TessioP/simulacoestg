%% D� a express�o para a propaga��o das medidas utilizada no QEKF

syms xe1 xe2 xe3 xe4 xe5 xe6 xe7 ax ay az mx my mz p q r dt u1 u2 u3
%xe = [q1 q2 q3 q4 p q r]
D11 = 1-2*xe2^2-2*xe3^2;
D12 = 2*(xe1*xe2+xe4*xe3);
D13 = 2*(xe1*xe3-xe4*xe2);
D21 = 2*(xe1*xe2-xe4*xe3);
D22 = 1-2*xe1^2-2*xe3^2;
D23 = 2*(xe3*xe2+xe4*xe1);
D31 = 2*(xe1*xe3+xe4*xe2);
D32 = 2*(xe3*xe2-xe4*xe1);
D33 = 1-2*xe1^2-2*xe2^2;


D = [ D11 D12 D13;
      D21 D22 D23;
      D31 D32 D33];

Z(1:3, 1) = D*[ax; ay; az];

Z(4:6, 1) = D*[mx; my; mz];

H = jacobian(Z, [xe1 xe2 xe3 xe4 xe5 xe6 xe7].');

H = simplify(H);




% f = D*[]
% F_k1 = jacobian(, [xe1 xe2 xe3 xe4].');
% F_k1 = simplify(F_k1)


Omega = 0.5*[ 0  r -q  p;
             -r  0  p  q;
              q -p  0  r;
             -p -q -r  0];

xe = [xe1 xe2 xe3 xe4 p q r];
         
syms mod T;

f = [(cos(mod*T/2)*eye(4) + 1/mod*sin(mod*T/2)*Omega)*[xe1; xe2; xe3; xe4]; xe5; xe6; xe7];
f = simplify(f);
F = jacobian(f, xe);
F = simplify(F)

% f = [(cos(mod*T/2)*eye(4) + 1/mod*sin(mod*T/2)*Omega)*[xe1; xe2; xe3; xe4];];
% f = simplify(f);
% F = jacobian(f, [p q r]);
% F = simplify(F)
