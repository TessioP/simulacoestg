close all;
% clear accData speedData quaternions;
clear all;
%% Par�metros de tempo
clc;

step = 10e-2;

simulationTime = 200;

times = 0:step:simulationTime;

%% Ru�dos para a simula��o da medi��o
clc;

sigma_acc = 0.2;

sigma_gyro = 0.2;

accNoise = eye(3)*sigma_acc^2; %covari�ncia do ru�do de medidas dos gir�metros

gyroNoise = eye(3)*sigma_gyro^2;
% bodyNoise = eye(3)*sigma_b^2; %covari�ncia do ru�do de medidas no sistema Sb

%% Ru�dos utilizados no filtro
clc;
% g = 9.8;
% sigma_ax = std(az(1:5000));
% sigma_ay = std(-ay(1:5000));
% sigma_az = std(ax(1:5000));
% sigma_gx = std(-gz(1:5000));
% sigma_gy = std(-gy(1:5000));
% sigma_gz = std(gx(1:5000));

% R = diag([sigma_ax sigma_ay sigma_az].^2);
R = eye(3)*sigma_acc^2;

Q = eye(7)*sigma_gyro^2;
% Q(5:7, 5:7) = diag([sigma_gx sigma_gy sigma_gz].^2);

%% Leitura inicial
clc;

% landmarkPositions = [0 0; 5/13 -5/13; -12/13 -12/13];

initialAcc = [-9.8 0 0]';

%% Condi��es iniciais do sistema
clc;

P(:,:,1)=0.01*eye(7); % Covari�ncia do erro de estima��o no instante zero

%% Condi��es iniciais do filtro
clc;

%% Simula��o das medidas
% g = 9.8;
[speedData, accData, quaternions] = generateData(times, gyroNoise, initialAcc, accNoise);
% % 
% speedData.Measured = IMU(gz, -gy, gx);
% accData.Measured = Acc(-az, -ay, ax);

quaternions.Estimated = Quaternion(0.1,0.1,0.1,1.1);

speedData.Estimated = IMU(speedData.Measured.p(1), speedData.Measured.q(1), speedData.Measured.r(1));

accData.Estimated = Acc(accData.Measured.ax(1), accData.Measured.ay(1), accData.Measured.az(1));

%% Filtro de Kalman
clc;
for k=1:length(times)-1
    q_k = [quaternions.Estimated.q1(k); quaternions.Estimated.q2(k); quaternions.Estimated.q3(k); quaternions.Estimated.q4(k)];
    u_k = [speedData.Measured.p(k); speedData.Measured.q(k); speedData.Measured.r(k)];
    x_k = [q_k; u_k];
    z_k1 = [accData.Measured.ax(k+1); accData.Measured.ay(k+1); accData.Measured.az(k+1)];
    [x_k1, P(:,:,k+1), ze_k1] = extendedKalmanFilter(x_k, P(:,:,k), u_k, z_k1, Q, R, step);
    quaternions.Estimated = quaternions.Estimated.addPointFromQuaternion(x_k1(1), x_k1(2), x_k1(3), x_k1(4));
    speedData.Estimated = speedData.Estimated.addPoint(x_k1(5), x_k1(6), x_k1(7));
    accData.Estimated = accData.Estimated.addPoint(ze_k1(1), ze_k1(2), ze_k1(3));
end
