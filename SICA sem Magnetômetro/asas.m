syms xe1 xe2 xe3 xe4 xe5 xe6 xe7 g R
D11 = 1-2*xe2^2-2*xe3^2;
D12 = 2*(xe1*xe2+xe4*xe3);
D13 = 2*(xe1*xe3-xe4*xe2);
D21 = 2*(xe1*xe2-xe4*xe3);
D22 = 1-2*xe1^2-2*xe3^2;
D23 = 2*(xe3*xe2+xe4*xe1);
D31 = 2*(xe1*xe3+xe4*xe2);
D32 = 2*(xe3*xe2-xe4*xe1);
D33 = 1-2*xe1^2-2*xe2^2;


D = [ D11 D12 D13;
      D21 D22 D23;
      D31 D32 D33];
  
Z = D*[-1 0 0].' - R/g*[xe6^2 + xe7^2; xe5^2 + xe7^2; xe5^2 + xe6^2]

jacobian(Z, [xe1 xe2 xe3 xe4 xe5 xe6 xe7].')