function D = quaternion2DCM(q)
%quaternion2DCM: Fun��o que converte um quaternio da forma da classe "Quaternion" para uma representa��o em
%matriz de cossenos diretores
D11 = 1-2*q(2)^2-2*q(3)^2;
D12 = 2*(q(1)*q(2)+q(4)*q(3));
D13 = 2*(q(1)*q(3)-q(4)*q(2));
D21 = 2*(q(1)*q(2)-q(4)*q(3));
D22 = 1-2*q(1)^2-2*q(3)^2;
D23 = 2*(q(3)*q(2)+q(4)*q(1));
D31 = 2*(q(1)*q(3)+q(4)*q(2));
D32 = 2*(q(3)*q(2)-q(4)*q(1));
D33 = 1-2*q(1)^2-2*q(2)^2;

D = [ D11 D12 D13;
      D21 D22 D23;
      D31 D32 D33];
end