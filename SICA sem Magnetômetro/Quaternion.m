classdef Quaternion
    %QUATERNION Armazena quaternions da forma [q1 q2 q3 q4]', em que q4 � a
    %parte real
    %aiushiuahsd
    properties
        q1
        q2
        q3
        q4
        DCM
        totalPoints
    end
    
    methods
        function obj = Quaternion(q1, q2, q3, q4)
            %QUATERNION Construct an instance of this class
            %   Detailed explanation goes here
            obj.q1 = q1;
            obj.q2 = q2;
            obj.q3 = q3;
            obj.q4 = q4;
            obj.totalPoints = length(q1);
            obj.DCM = quaternion2DCM([q1, q2, q3, q4]);
        end
        
        function obj = addPointFromQuaternion(obj,q1,q2,q3,q4)
            %addPoint Summary of this method goes here
            %   Detailed explanation goes here
            obj.totalPoints = obj.totalPoints+1;
            obj.q1(obj.totalPoints) = q1;
            obj.q2(obj.totalPoints) = q2;
            obj.q3(obj.totalPoints) = q3;
            obj.q4(obj.totalPoints) = q4;
            obj.DCM(:,:,obj.totalPoints) = quaternion2DCM([q1, q2, q3, q4]);
        end
        
        function obj = addPointFromPQR(obj, p, q, r, step)
            currentQuaternion = [obj.q1(obj.totalPoints), obj.q2(obj.totalPoints), obj.q3(obj.totalPoints), obj.q4(obj.totalPoints)]';
            obj.totalPoints = obj.totalPoints+1;
            [auxQuaternion, ~] = quaternionPropagation(currentQuaternion, [p q r], step);
            obj.q1(obj.totalPoints) = auxQuaternion(1);
            obj.q2(obj.totalPoints) = auxQuaternion(2);
            obj.q3(obj.totalPoints) = auxQuaternion(3);
            obj.q4(obj.totalPoints) = auxQuaternion(4);
            obj.DCM(:,:,obj.totalPoints) = quaternion2DCM(auxQuaternion);
        end
        
        function plotData(obj)
            figure()
            plot(obj.q1);
            hold on
            plot(obj.q2);
            hold on
            plot(obj.q3);
            hold on
            plot(obj.q4);
            hold on
            legend('q_1','q_2','q_3','q_4');
            grid on;
        end
    end
end

