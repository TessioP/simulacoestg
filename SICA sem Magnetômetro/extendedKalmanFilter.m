function [x_k1, P_k1, ze_k1] = extendedKalmanFilter(x_k, P_k, u_k, z_k1, Q_k, R_k1, dt)
%EXTENDEDKALMANFILTER Summary of this function goes here
%   Detailed explanation goes here
g = 9.8;
Lx = 1;
Ly = 1;
Lz = 1;
R=1;
%% 1a)Propaga��o do estado estimado
% C�lculo de xhat_e(k+1|k):
% propaga��o do estado estimado pela din�mica do sistema
qe_k = x_k(1:4);
wm_k = x_k(5:7);
[qe_k1, Exp_Omega_k_T] = quaternionPropagation(qe_k, wm_k, dt);

we_k1 = u_k;

xe_k1 = [qe_k1; we_k1];

% C�lculo de P(k+1|k):
% Propaga��o da matriz de covari�ncia (nesse passo, a incerteza deve
% aumentar)
F_k_1  =[[ 1              0.5*dt*u_k(3) -0.5*dt*u_k(2)  0.5*dt*u_k(1)];
         [-0.5*dt*u_k(3)  1              0.5*dt*u_k(1)  0.5*dt*u_k(2)];
         [ 0.5*dt*u_k(2) -0.5*dt*u_k(1)  1              0.5*dt*u_k(3)];
         [-0.5*dt*u_k(1) -0.5*dt*u_k(2) -0.5*dt*u_k(3)  1            ]];

F_k_2  =[[ 0.5*dt*qe_k(4) -0.5*dt*qe_k(3)  0.5*dt*qe_k(2)];
         [ 0.5*dt*qe_k(3)  0.5*dt*qe_k(4) -0.5*dt*qe_k(1)];
         [-0.5*dt*qe_k(2)  0.5*dt*qe_k(1)  0.5*dt*qe_k(4)];
         [-0.5*dt*qe_k(1) -0.5*dt*qe_k(2) -0.5*dt*qe_k(3)]];
     
F_k  = zeros(7,7);

F_k(1:4,1:4) = F_k_1;
F_k(1:4,5:7) = F_k_2;

Csi_k = [-qe_k(2) -qe_k(3) -qe_k(4);
          qe_k(1) -qe_k(4)  qe_k(3);
          qe_k(4)  qe_k(1) -qe_k(2);
         -qe_k(3)  qe_k(2)  qe_k(1)];

Gama_k = (dt/2)*Exp_Omega_k_T*Csi_k;       

Q_q_k = Gama_k*Q_k(5:7,5:7)*Gama_k';    

Q_k(1:4,1:4) = Q_q_k;

P_k1 = F_k*P_k*F_k' + Q_k;


%% Predi��o das medidas
xe1 = xe_k1(1);      %q1
xe2 = xe_k1(2);      %q2
xe3 = xe_k1(3);      %q3
xe4 = xe_k1(4);      %q4
xe5 = xe_k1(5);      %wx
xe6 = xe_k1(6);      %wy
xe7 = xe_k1(7);      %wz

% C�lculo de zhat(k+1|k)
% 
% ze_k1 = quaternion2DCM(qe_k1)*[0 0 -g]' - R*[xe6^2 + xe7^2; xe5^2 + xe7^2; xe5^2 + xe6^2];

ze_k1 =  [   g*(2*xe2^2 + 2*xe3^2 - 1) - R*(xe6^2 + xe7^2);
           - R*(xe5^2 + xe7^2) - g*(2*xe1*xe2 - 2*xe3*xe4);
           - R*(xe5^2 + xe6^2) - g*(2*xe1*xe3 + 2*xe2*xe4)];
% 
% ze_k1 = [   2*xe2^2 + 2*xe3^2 - (R*(xe6^2 + xe7^2))/g - 1;
%             2*xe3*xe4 - 2*xe1*xe2 - (R*(xe5^2 + xe7^2))/g;
%           - 2*xe1*xe3 - 2*xe2*xe4 - (R*(xe5^2 + xe6^2))/g];

%% C�lculo de Pz(k+1|k)
H_k1 = [        0,  4*g*xe2,  4*g*xe3,        0,        0, -2*R*xe6, -2*R*xe7;
         -2*g*xe2, -2*g*xe1,  2*g*xe4,  2*g*xe3, -2*R*xe5,        0, -2*R*xe7;
         -2*g*xe3, -2*g*xe4, -2*g*xe1, -2*g*xe2, -2*R*xe5, -2*R*xe6,        0];

% H_k1 = [      0,  4*xe2,  4*xe3,      0,            0, -(2*R*xe6)/g, -(2*R*xe7)/g;
%          -2*xe2, -2*xe1,  2*xe4,  2*xe3, -(2*R*xe5)/g,            0, -(2*R*xe7)/g;
%          -2*xe3, -2*xe4, -2*xe1, -2*xe2, -(2*R*xe5)/g, -(2*R*xe6)/g,            0];

Pz_k1 = H_k1*P_k1*H_k1' + R_k1;

%% 1c) Covari�ncia cruzada, Pzx(k+1|k)
Pxz_k1 = P_k1*H_k1';
 
%% 2a) Ganho de Kalman
K_k1 = Pxz_k1*pinv(Pz_k1);
 
%% 2b) Atualiza��o da estimativa
% ze_k1 = H_k1*xe_k1;
x_k1 = xe_k1 + K_k1*(z_k1 - ze_k1);
x_k1(1:4) = x_k1(1:4)/norm(x_k1(1:4));  %normaliza��o do quaternion
 
%% 3) Atualiza��o da covari�ncia
P_k1 = P_k1 - K_k1*Pz_k1*K_k1';

xe1 = x_k1(1);      %q1
xe2 = x_k1(2);      %q2
xe3 = x_k1(3);      %q3
xe4 = x_k1(4);      %q4
xe5 = x_k1(5);      %wx
xe6 = x_k1(6);      %wy
xe7 = x_k1(7);      %wz

ze_k1 =  [   g*(2*xe2^2 + 2*xe3^2 - 1) - R*(xe6^2 + xe7^2);
           - R*(xe5^2 + xe7^2) - g*(2*xe1*xe2 - 2*xe3*xe4);
           - R*(xe5^2 + xe6^2) - g*(2*xe1*xe3 + 2*xe2*xe4)];
end

