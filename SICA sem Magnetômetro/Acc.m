classdef Acc
    %Acc Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        ax
        ay
        az
        totalPoints
    end
    
    methods
        function obj = Acc(ax,ay,az)
            %Acc Construct an instance of this class
            %   Detailed explanation goes here
            obj.ax = ax;% zeros(1, totalPoints)];
            obj.ay = ay;% zeros(1, totalPoints)];
            obj.az = az;% zeros(1, totalPoints)];
            obj.totalPoints = length(ax);
        end
        
        function obj = addPoint(obj,ax,ay,az)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            obj.totalPoints = obj.totalPoints+1;
            obj.ax(obj.totalPoints) = ax;
            obj.ay(obj.totalPoints) = ay;
            obj.az(obj.totalPoints) = az;
        end
        function plotData(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            figure()
            hold on;
            plot(obj.ax);
            hold on;
            plot(obj.ay);
            hold on;
            plot(obj.az);
            legend('a_x', 'a_y ', 'a_z');
            grid on;
        end
    end
end