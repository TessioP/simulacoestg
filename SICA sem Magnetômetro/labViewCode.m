clear all;close all;
%% Par�metros de tempo
clc;

step = 0.01;

simulationTime = 50;

times = 0:step:simulationTime;

%% Ru�dos para a simula��o da medi��o
clc;

sigma_acc = 0.2;

sigma_gyro = 0.2;

accNoise = eye(3)*sigma_acc^2; %covari�ncia do ru�do de medidas dos gir�metros

gyroNoise = eye(3)*sigma_gyro^2;
% bodyNoise = eye(3)*sigma_b^2; %covari�ncia do ru�do de medidas no sistema Sb

%% Ru�dos utilizados no filtro
clc;

% R = eye(3)*sigma_acc^2;

Q = eye(7)*sigma_gyro^2;



%% Leitura inicial
clc;

% landmarkPositions = [0 0; 5/13 -5/13; -12/13 -12/13];

initialAcc = [-9.8 0 0]';

%% Condi��es iniciais do sistema
clc;

P(:,:,1)=0.01*eye(7); % Covari�ncia do erro de estima��o no instante zero

%% Condi��es iniciais do filtro
clc;

%% Simula��o das medidas

%% Inicializa��o das structs
R = 1;
speedData.Real = 0.2*[sin(times); cos(times); -(sin(times).*cos(times))];
speedData.Measured = zeros(3, length(times));
speedData.Estimated = zeros(3, length(times));
quaternions.Real = zeros(4, length(times));
quaternions.Estimated = zeros(4, length(times));
accData.Real = zeros(3, length(times));
accData.Measured = zeros(3, length(times));
accData.Estimated = zeros(3, length(times));

%% Gera��o dos valores reais de velocidade e do primeiro sinal ruidoso
speedData.Measured(:,1) = speedData.Real(:,1)+(gyroNoise)*randn(3,1);


%% O objeto inicializa no quaternio [0 0 0 1]'
quaternions.Real(:, 1) = [0, 0, 0, 1]';

%% Primeiro valor de acelera��o do sistema
% Auxiliares para criar os dados de acelera��o
p = speedData.Real(1,1);
q = speedData.Real(2,1);
r = speedData.Real(3,1);
accData.Real(:, 1) = quaternion2DCM(quaternions.Real(:, 1))*initialAcc - R*[q^2 + r^2; p^2 + r^2; p^2 + q^2];
accData.Measured(:, 1) = accData.Real(:, 1) + (accNoise)*randn(3,1);

%% Preenchendo os vetores de simula��o
for k=1:length(times)-1
    % Vari�veis auxiliares para c�lculo da propaga��o do quat�rnio
    p = speedData.Real(1, k+1);
    q = speedData.Real(2, k+1);
    r = speedData.Real(3, k+1);
    quaternions.Real(:, k+1) = quaternionPropagation(quaternions.Real(:, k), speedData.Real(:, k), step);
    % Vari�veis auxiliares para c�lculo das componentes de acelera��o
    accData.Real(:, k+1) = quaternion2DCM(quaternions.Real(:, k+1))*initialAcc - R*[q^2 + r^2; p^2 + r^2; p^2 + q^2];
    accData.Measured(:, k+1) = accData.Real(:, k+1) + (accNoise)*randn(3,1);
    
    % Inser��o do erro de medida nas velocidades
    speedData.Measured(:, k+1) = [p q r]' + (gyroNoise)*randn(3,1);
end

quaternions.Estimated(:, 1) = quaternions.Real(:,1);
accData.Estimated = accData.Measured(:,1);
speedData.Estimated = speedData.Measured(:,1);

%% Filtro de Kalman

for k=1:length(times)-1
tic;
    q_k = quaternions.Estimated(:, k);
    u_k = speedData.Measured(:, k);
    x_k = [q_k; u_k];
    z_k1 = accData.Measured(:, k+1);
    P_k = P(:,:,k);
    Q_k = Q;
    R_k1 = eye(3)*sigma_acc^2;
    dt = step;
    
    %% function [x_k1, P_k1, ze_k1] = extendedKalmanFilter(x_k, P_k, u_k, z_k1, Q_k, R_k1, dt)
    g = 9.8;
    R = 1;
    %% 1a)Propaga��o do estado estimado
    % C�lculo de xhat_e(k+1|k):
    % propaga��o do estado estimado pela din�mica do sistema
    qe_k = x_k(1:4);
    wm_k = x_k(5:7);
    [qe_k1, Exp_Omega_k_T] = quaternionPropagation(qe_k, wm_k, dt);
    
    we_k1 = u_k;
    
    xe_k1 = [qe_k1; we_k1];
    
    % C�lculo de P(k+1|k):
    % Propaga��o da matriz de covari�ncia (nesse passo, a incerteza deve
    % aumentar)
    F_k_1  =[ 1               0.5*dt*u_k(3)  -0.5*dt*u_k(2)  0.5*dt*u_k(1);
             -0.5*dt*u_k(3)   1               0.5*dt*u_k(1)  0.5*dt*u_k(2);
              0.5*dt*u_k(2)  -0.5*dt*u_k(1)   1              0.5*dt*u_k(3);
             -0.5*dt*u_k(1)  -0.5*dt*u_k(2)  -0.5*dt*u_k(3)  1            ];
    
    F_k_2  =[ 0.5*dt*qe_k(4) -0.5*dt*qe_k(3)  0.5*dt*qe_k(2);
              0.5*dt*qe_k(3)  0.5*dt*qe_k(4) -0.5*dt*qe_k(1);
             -0.5*dt*qe_k(2)  0.5*dt*qe_k(1)  0.5*dt*qe_k(4);
             -0.5*dt*qe_k(1) -0.5*dt*qe_k(2) -0.5*dt*qe_k(3)];
    
    F_k  = zeros(7,7);
    
    F_k(1:4,1:4) = F_k_1;
    F_k(1:4,5:7) = F_k_2;
    
    Csi_k = [-qe_k(2) -qe_k(3) -qe_k(4);
              qe_k(1) -qe_k(4)  qe_k(3);
              qe_k(4)  qe_k(1) -qe_k(2);
             -qe_k(3)  qe_k(2)  qe_k(1)];
    
    Gama_k = (dt/2)*Exp_Omega_k_T*Csi_k;
    
    Q_q_k = Gama_k*Q_k(5:7,5:7)*Gama_k';
    
    Q_k(1:4,1:4) = Q_q_k;
    
    P_k1 = F_k*P_k*F_k' + Q_k;
    
    
    %% Predi��o das medidas
    xe1 = xe_k1(1);      %q1
    xe2 = xe_k1(2);      %q2
    xe3 = xe_k1(3);      %q3
    xe4 = xe_k1(4);      %q4
    xe5 = xe_k1(5);      %wx
    xe6 = xe_k1(6);      %wy
    xe7 = xe_k1(7);      %wz
    
    % C�lculo de zhat(k+1|k)
    %
    % ze_k1 = quaternion2DCM(qe_k1)*[0 0 -g]' - R*[xe6^2 + xe7^2; xe5^2 + xe7^2; xe5^2 + xe6^2];
    
    ze_k1 =  [   g*(2*xe2^2 + 2*xe3^2 - 1) - R*(xe6^2 + xe7^2);
               - R*(xe5^2 + xe7^2) - g*(2*xe1*xe2 - 2*xe3*xe4);
               - R*(xe5^2 + xe6^2) - g*(2*xe1*xe3 + 2*xe2*xe4)];
    
    %% C�lculo de Pz(k+1|k)
    H_k1 = [        0,  4*g*xe2,  4*g*xe3,        0,        0, -2*R*xe6, -2*R*xe7;
             -2*g*xe2, -2*g*xe1,  2*g*xe4,  2*g*xe3, -2*R*xe5,        0, -2*R*xe7;
             -2*g*xe3, -2*g*xe4, -2*g*xe1, -2*g*xe2, -2*R*xe5, -2*R*xe6,        0];
    
    Pz_k1 = H_k1*P_k1*H_k1' + R_k1;
    
    %% 1c) Covari�ncia cruzada, Pzx(k+1|k)
    Pxz_k1 = P_k1*H_k1';
    
    %% 2a) Ganho de Kalman
    K_k1 = Pxz_k1*pinv(Pz_k1);
    
    %% 2b) Atualiza��o da estimativa
    % ze_k1 = H_k1*xe_k1;
    x_k1 = xe_k1 + K_k1*(z_k1 - ze_k1);
    x_k1(1:4) = x_k1(1:4)/norm(x_k1(1:4));  %normaliza��o do quaternion
    
    %% 3) Atualiza��o da covari�ncia
    P_k1 = P_k1 - K_k1*Pz_k1*K_k1';
    
    xe1 = x_k1(1);      %q1
    xe2 = x_k1(2);      %q2
    xe3 = x_k1(3);      %q3
    xe4 = x_k1(4);      %q4
    xe5 = x_k1(5);      %wx
    xe6 = x_k1(6);      %wy
    xe7 = x_k1(7);      %wz
    
    ze_k1 =  [   g*(2*xe2^2 + 2*xe3^2 - 1) - R*(xe6^2 + xe7^2);
        - R*(xe5^2 + xe7^2) - g*(2*xe1*xe2 - 2*xe3*xe4);
        - R*(xe5^2 + xe6^2) - g*(2*xe1*xe3 + 2*xe2*xe4)];
    
    P(:,:,k+1) = P_k1;
    quaternions.Estimated(:, k+1) = [x_k1(1), x_k1(2), x_k1(3), x_k1(4)]';
    speedData.Estimated(:, k+1) = [x_k1(5), x_k1(6), x_k1(7)]';
    accData.Estimated(:, k+1) = [ze_k1(1), ze_k1(2), ze_k1(3)]';
elapsedTime(k) = toc;
end

%% Gera��o dos gr�ficos
figure()
plot(times, speedData.Measured(1, :), times, speedData.Measured(2, :), times, speedData.Measured(3, :));
title('Dados medidos de velocidade')
grid on;
legend('p = \omega_x','r = \omega_y','q = \omega_z')

figure()
plot(times, speedData.Estimated(1, :), times, speedData.Estimated(2, :), times, speedData.Estimated(3, :));
title('Dados estimados de velocidade')
grid on;
legend('p = \omega_x','r = \omega_y','q = \omega_z')

figure()
plot(times, accData.Real(1, :), times, accData.Real(2, :), times, accData.Real(3, :));
title('Dados reais de acelera��o')
grid on;
legend('a_x','a_y','a_z')

figure()
plot(times, accData.Measured(1, :), times, accData.Measured(2, :), times, accData.Measured(3, :));
title('Dados medidos de acelera��o')
grid on;
legend('a_x','a_y','a_z')

figure()
plot(times, quaternions.Real(1, :), times, quaternions.Real(2, :), times, quaternions.Real(3, :), times, quaternions.Real(4, :))
title('Quaternions reais')
grid on;
legend('q_1','q_2','q_3','q_4')

figure()
plot(times, quaternions.Estimated(1, :), times, quaternions.Estimated(2, :), times, quaternions.Estimated(3, :), times, quaternions.Estimated(4, :))
title('Quaternions estimados')
grid on;
legend('q_1','q_2','q_3','q_4')

fontSize = 14;
lineWidth = 1.2;
handles = findall(0, 'type', 'figure');
for i=1:length(handles)
    handle = handles(i);
    set(findall(handle, '-property', 'LineWidth'), 'LineWidth', lineWidth);
    set(findall(handle, '-property', 'FontSize'), 'FontSize', fontSize);
    %     print(handle, '-depsc2', sprintf('figure%d.eps', i));
end