
clear all
close all
clc

filename = 'magCalibration.txt'; % apenas o MAG
%  filename = 'teste_mesa_vermelha_2.txt'; % apenas o MAG
% % filename = 'teste_mesa_vermelha_acc.txt'; % apenas o MAG
data = importdata(filename);

%% verifica��o do Magnet�metro

mag=data(:,1:3);

%% verifica��o do Gir�metro

gyro = data(:,7:9);

%% verifica��o do Aceler�metro

acc=data(:,4:6);




% Leitura dos dados
aux = importdata('mag.xlsx');
tempo = aux(:,1);       % dados de tempo
sensor = aux(:,2:4);    % dados do sensor em x, y e z [micro-Gauss]
dtempo=diff(tempo);     % diferen�a de tempos
t0=mean(dtempo);        % m�dia das diferen�as de tempos
y=diff(sensor);  % if phase
y=diff(sensor)/t0;  % if frequency

N=length(tempo);        % n�mero de pontos de dados consecutivos

for n=1:floor(N/2) 
    
    % RESET do vetor das m�dias dos clusteres com n 
    % pontos de dados consecutivos referente aos respectivos eixos
    YX = zeros(1,n*floor( N/n));	% eixo X
    YY = zeros(1,n*floor( N/n));	% eixo Y
    YZ = zeros(1,n*floor( N/n));	% eixo Z
    
    % RESET do auxiliar para o c�lculo do (desvio padr�o)^2 das m�dias dos clusteres 
    % com n pontos de dados consecutivos referente aos respectivos eixos
    sigmaX2(n) = 0;	% eixo X
    sigmaY2(n) = 0;   % eixo Y
    sigmaZ2(n) = 0;   % eixo Z
        
    for k=1:n:n*floor( N/n)
    % para cada cluster com n pontos de dados consecutivos, 
    % com in�cio no k'�simo �ndice dos pontos de dados do sensor
                                 
        % c�lculo da m�dia do cluster referente aos respectivos eixos
        for i = k:k+n-1  
            % if phase
            YX(k)=YX(k)+(y(i,1))/(n*t0);
            YY(k)=YY(k)+(y(i,2))/(n*t0);
            YZ(k)=YZ(k)+(y(i,3))/(n*t0); 
%             % if frequency
%             YX(k)=YX(k)+(y(i,1))/(n);
%             YY(k)=YY(k)+(y(i,2))/(n);
%             YZ(k)=YZ(k)+(y(i,3))/(n); 
        end       
        
             
                  
        if a>1
        	% C�lculo do (desvio padr�o)^2 dos clusteres com n pontos de dados consecutivos 
        	sigmaX2(n)=sigmaX2(n)+((media_clusterX(a)-media_clusterX(a-1))^2)/(2*(N-2*n));
        	sigmaY2(n)=sigmaY2(n)+((media_clusterY(a)-media_clusterY(a-1))^2)/(2*(N-2*n));
        	sigmaZ2(n)=sigmaZ2(n)+((media_clusterZ(a)-media_clusterZ(a-1))^2)/(2*(N-2*n));
        end
    end              
    
    % C�lculo do desvio padr�o dos clusteres com n pontos de dados consecutivos
    sigmaX(n)=sqrt(sigmaX2(n));
    sigmaY(n)=sqrt(sigmaY2(n));
    sigmaZ(n)=sqrt(sigmaZ2(n));
    tempo(n)=mean(mediadtempo);
end

T=1:n ; %vetor dos tempos de amostras

%% plote dos gr�ficos de Allan Variance

figure
plot(T,sigmaX)
title('Allan Variance do magnet�metro')
ylabel('(\sigma(n))')
xlabel(' n')
hold on
plot(T,sigmaY)
plot(T,sigmaZ)
legend('eixo X','eixo Y','eixo Z')
grid on


figure
plot(log10(T),log10(sigmaX),'b')
title('Allan Variance do magnet�metro')
ylabel('log (\sigma(n))')
xlabel('log (n)')
hold on
plot(log10(T),log10(sigmaY),'r')
plot(log10(T),log10(sigmaZ),'y')
legend('eixo X','eixo Y','eixo Z')
grid on

b=[log10(sigmaX'),log10(sigmaY'),log10(sigmaZ')];

% reta para o c�lculo do erro de quantiza��o Q
t=T;
m=-1;
sQX=m*log10(t)+b(1,1);
sQY=m*log10(t)+b(1,2);
sQZ=m*log10(t)+b(1,3);

% reta para o c�lculo do caminho aleat�rio em �ngulo N
t=T;
m=-1/2;
sNX=m*log10(t)+b(8,1);
sNY=m*log10(t)+b(1,2);
sNZ=m*log10(t)+b(1,3);

% reta para o c�lculo do deriva do vi�s B

for i=2:length(T)/2
    if sigmaX(i)<sigmaX(i-1)
        sBX=sigmaX(i);
    end
    if sigmaY(i)<sigmaY(i-1)
        sBY=sigmaY(i);
    end
    if sigmaZ(i)<sigmaZ(i-1)
        sBZ=sigmaZ(i);
    end        
end

% reta para o c�lculo do caminho aleat�rio em velocidade
t=T;
m=1/2;
sKX=m*log10(t)+b(30,1);
sKY=m*log10(t)+b(1,2);
sKZ=m*log10(t)+b(1,3);

% reta para o c�lculo do deriva da rampa de velocidade
t=T;
m=1;
sRX=m*log10(t)+b(1,1);
sRY=m*log10(t)+b(1,2);
sRZ=m*log10(t)+b(1,3);


% erro de quantiza��o Q
t=sqrt(3);
m=-1;
QX=m*log10(t)+b(1,1);
QY=m*log10(t)+b(1,2);
QZ=m*log10(t)+b(1,3);

% caminho aleat�rio em �ngulo N
t=1;
m=-1/2;
NX=m*log10(t)+b(8,1);
NY=m*log10(t)+b(1,2);
NZ=m*log10(t)+b(1,3);

% deriva do vi�s B

BX=sqrt(pi/2/log(2))*sBX;
BY=sqrt(pi/2/log(2))*sBY;
BZ=sqrt(pi/2/log(2))*sBZ;

% caminho aleat�rio em velocidade
t=3;
m=1/2;
KX=m*log10(t)+b(1,1);
KY=m*log10(t)+b(1,2);
KZ=m*log10(t)+b(1,3);

% deriva da rampa de velocidade
t=sqrt(2);
m=1;
RX=m*log10(t)+b(1,1);
RY=m*log10(t)+b(1,2);
RZ=m*log10(t)+b(1,3);

Q=[QX,QY,QZ]
N=[NX,NY,NZ]
B=[BX,BY,BZ]
K=[KX,KY,KZ]
R=[RX,RY,RZ]

figure
plot(log10(T),log10(sigmaX),'b')
title('Allan Variance do magnet�metro do eixo X')
ylabel('log (\sigma(n))')
xlabel('log (n)')
hold on
plot(log10(T),sQX,'k')
plot(sqrt(3)*ones(1,length(sQX)),sQX,'k--')
plot(log10(T),sNX,'k')
plot(ones(1,length(sNX)),sNX,'k--')
plot(log10(T),sBX*ones(1,length(sBX)),'k')
plot(log10(T),sKX,'k')
plot(3*ones(1,length(sKX)),sKX,'k--')
plot(log10(T),sRX,'k')
plot(sqrt(2)*ones(1,length(sRX)),sRX,'k--')

legend('eixo X','eixo Y','eixo Z')
grid on





% g1=fittype(@(a,x) -x+a)
% f1=fit(log10(T),log10(sigmaY),g1,'StarPoint',[1 n])
% 
% plot(f1,log10(T),log10(sigmaY))


