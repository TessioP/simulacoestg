function [ M_align, Sf, bias ] = F_Gyro_Align( w, Wrt, eNL, G )
% Fun��o para alinhar um conjunto de gir�metros dispostos em uma geometria
% qualquer

%Autores: AspOf Nathalia Mattos
%ASE-C
%Data: 27/04/2018

% Par�metros de entrada:
% w: Matriz com sequ�ncia de rota��es aplicadas ao referencial local.
% Matriz de estados contendo os valores das componentes das rota��es
% normalizadas e aplicadas ao sistema de coordenadas local escolhido. Para
% N estados esta matriz conter� N linhas e para K giros a matriz conter� K
% colunas.

% Wrt: (Graus/s) M�dulo da velocidade angular aplicada aos eixos
% escolhidos. Normalmente estes eixos s�o os eixos da mesa de rota��o
% devidamente nivelada e com o �ngulo de azimute compensado

% eNL: (Graus) �ngulo de desalinhamento da mesa. Rota��o no eixo Norte.
% Erro no nivelamento da mesa em graus no plano vertical-leste. Expressa o
% desalinhamento entre o sistema local e o NEU. Pode ser compensado para
% uma mesa com 3 ou mais graus de liberdade, neste caso eNL = 0

% G: Matriz de medidas, onde cada coluna corresponde �s medidas fornecidas
% pelo grupo de K giros que est�o sendo alinhados para um dado estado.
% As medidas est�o em unidades naturais dos Giros, em Volts
% Cada linha da matriz w corresponde a um estado no sistema local.
% Para um ensaio com N estados, uma matriz w com N linhas deve ser
% fornecida
% A matriz de medidas G conter� N conjuntos de medidas dos giros
% organizados em colunas. Se o sistema contiver K giros, a matriz G conter�
% K*N colunas, onde:


% Par�metro de sa�da:
% M_align: Matriz de alinhamento com rela��o ao NEU. Matriz de alinhamento,
% que transporta as medidas dos giros para o sistema de coordenadas NEU
% Sf: Matriz diagonal com os fatores de escala dos Giros em (�/s)/mV
% bias: Vetor com os biases dos giros em unidades de (�/s) no sistema de
% coordenadas NEU
% dif_bias: Matriz de biases calculado para os estados diferenciais

% Matriz para a corre��o do nivelamento da mesa em torno do eixo Z (Norte)
% Esta matriz rotaciona vetores no sistema local para o NEU
M_L2NEU = [cosd(eNL) -sind(eNL) 0;
           sind(eNL) cosd(eNL) 0;
           0 0 1];
       
% N�mero de posi��es ensaiadas
% = n�mero de linhas da matriz w
% = n�mero de estados
% Cada linha da matriz de w � um estado, que corresponde a um vetor
% normalizado com a dire��o imposta no sistema local
% Cada vetor corresponde aos valores reais observados pelos sensores
E = size(w,1);

% N�mero de Giros
% = n�mero de colunas da matriz w
Qg = size(w,2);

% Calculando a m�dia dos valores das colunas de G
% Calcula a m�dia das medidas de cada Giro do grupo para cada estado -
% G(grupo,estado)
Gm = mean(G)

%% Modelo de Erros

% SF*Dg = HLs*dw+ruido

%% Estima��o por m�nimos quadrados

% Calculando a matriz das diferen�as das medidas complementares (rota��es
% no mesmo plano mas com sentidos opostos)
for j = 1: (E/2)
    for i=1:Qg
        Gbar(i,j)=Gm((j-1)*E/2+i)-Gm((j-1)*E/2+i+Qg);
    end
end

% Criando nova matriz de estados
% Formada pela diferen�a entre rota��es opostas em um mesmo plano
% A nova matriz de estados possui metade dos estados da anterior
% Agora os novos estados est�o representados pelas colunas de W
for j = 1:(E/2)
    W(:,j) = Wrt*(w(2*j-1,:)'-w(2*j,:)');
end

% C�lculo da matriz de forma (matriz de desalinhamento) que inclui os
% fatores de escala. Utiliza-se m�nimos quadrados para calcular esta matriz
% Matriz que associa a rota��o real aplicada �s
% leituras dos giros, incluindo tamb�m seus erros
% de alinhamento, fatores de escala de cada um
% dos sensores e outros erros n�o considerados,
% onde HSF=inv(SF)*H_sL 
Hf_til = Gbar*pinv(W)	

% C�lculo da Matriz diagonal com fatores de escala
% Encontra a Matroz de fatores de escala para os giros em ((Deg/s)/mV)
% Sepada os fatores de escala da matriz de forma 
% A matriz de forma � ortogonal e o m�dulo de suas linhas � 1
% Ent�o, o m�dulo das linhas da matriz de forma combinadas s�o os fatores
% de escala
for i=1:Qg
    % estima��o dos fatores de escala [(grau/s)/(mV)]
    SF(i,i)=1/norm(Hf_til(i,:),2); % Matriz diagonal do fator de escala
end

% C�lculo da matriz de forma ou de desalinhamento para o sistema de
% coordenadas local
% Retira a contribui��o do fator de escala da matriz de forma combinada
% matriz da transforma��o do sistema de coordenadas local para
% o sistema dos sensores
H_til=SF*Hf_til %HsL

% C�lculo da matriz de alinhamento ou de desalinhamento inversa
% Utilizada para obter as velocidades angulares no sistema de coordenadas
% local
H_star=inv(H_til) %HLs 

%% C�lculo dos biases

%Preenche a matriz de medidas Gv (em unidades naturais)
% O vetor de medidas m�dias Gm � organizado de outra forma em Gv
% Cada coluna de Gv corresponde �s medidas m�dias em um dado estado (E
% estados)
% As linhas correspondem �s medidas de cada um dos giros (Qg giros)
for j=1:E
    for i=1:Qg
        Gv(i,j)=Gm((j-1)*Qg+i);
    end
end

% C�lculo do vetor de bias dos Giros em graus/s no sistema local
% M = mean(A,dim): the mean along dimension dim. 
b_til = mean(SF*Gv-H_til*Wrt*w',2);

% Calcula o bias residual da diferen�a de duas rota��es complementares os
% valores devem ser muito pr�ximos de zero, pois em teoria para os novos
% estados (diferen�a entre rota��es complementares) o bias � cancelado
% Apenas para testes
b_dif= mean(SF*Gbar-H_til*W,2);

% C�lculo da matriz de alinhamento, do vetor de bias e do vetor residual no
% sistema de coordenadas NEU
% Fator de escala � o mesmo em qualquer referencial
M_align=M_L2NEU*H_star
Sf = SF
bias=M_L2NEU*b_til
end

