%Fun��o principal para a calibra��o do girosc�pio

%Autores: AspOf Nathalia Mattos
%ASE-C
%Data: 02/05/2018

%Com base no Relat�rio T�cnico Calibra��o dos Sensores Inerciais de uma IMU
% de Marcelo Carvalho Tosin, 2012

% Uso: Calibra��o do gir�metro L3G4200D do GY80
% Dados de entrada relativos ao ensaio do dia 26/04/18
% Fun��es utilizadas:
% function [ M_align, Sf, bias ] = F_Gyro_Align( w, Wrt, eNL, G )
% fun��o que considera a diferen�a das medidas antag�nicas
% ou
% function [ M_align, Sf, bias ] = F_Align( w*Wrt, eNL, G )
% fun��o que n�o considera a diferen�a das medidas antag�nicas

% Par�metros de entrada:
% w: Matriz com sequ�ncia de rota��es aplicadas ao referencial local
% Wrt: m�dulo da velocidade angular aplicada. (Graus/s)
% eNL: �ngulo de desalinhamento da mesa. Rota��o no eixo Norte. (Graus)
% G: referencia para a matriz "giros" em ensaios em colunas

% Par�metro de sa�da:
% M_align: Matriz de alinhamento com rela��o ao NEU
% Sf: Matriz diagonal com os fatores de escala dos Giros
% bias: Vetor com os biases dos giros

clear all
clc
close all

tic % in�cio do programa

% �ngulo de desalinhamento da mesa
% Rota��o no eixo Z (norte) desalinhando os eixos XY (regra da m�o direita,
% sentido norte)
eNL = 0; % mesa de 3 eixos

% Matriz de estados com a sequ�ncia de rota��es
w = [1 0 0;
    -1 0 0;
    1 0 0;
    -1 0 0;
    0 1 0;
    0 -1 0;
    0 1 0;
    0 -1 0;
    0 0 1;
    0 0 -1;
    0 0 -1;
    0 0 1];	

% Taxa de rota��o da mesa. � o m�dulo dos estados impostos. � constante
% para os ensaios na mesa
Wrt = 30; % (Graus/s)

% N�mero de posi��es ensaiadas
% = n�mero de linhas da matriz w
% = n�mero de estados
% Cada linha da matriz de w � um estado, que corresponde a um vetor
% normalizado com a dire��o imposta no sistema local
% Cada vetor corresponde aos valores reais observados pelos sensores
E = size(w,1);

% N�mero de Giros
% = n�mero de colunas da matriz w
Qm = size(w,2);

% Carrega arquivo com os dados dos giros
filename = 'teste_mesa_vermelha_2.txt';
data = importdata(filename);

gyro = data(:,7:9);
figure
plot(gyro(:,1)')
title('medidas do gir�metro originais')
xlabel('n�mero de dados')
ylabel('medida do sensor(U.G.)')
hold on
plot(gyro(:,2)')
plot(gyro(:,3)')
legend('eixo x','eixo y','eixo z')

mag=data(:,1:3);
figure
plot(mag(:,1)')
title('medidas do magnet�metro originais')
xlabel('n�mero de dados')
ylabel('medida do sensor(U.M.)')
hold on
plot(mag(:,2)')
plot(mag(:,3)')
legend('eixo x','eixo y','eixo z')

acc=data(:,4:6);
figure
plot(acc(:,1)')
title('medidas do aceler�metro originais')
xlabel('n�mero de dados')
ylabel('medida do sensor(U.A.)')
hold on
plot(acc(:,2)')
plot(acc(:,3)')
legend('eixo x','eixo y','eixo z')

figure
plot3(mag(:,1),mag(:,2),mag(:,3)); axis equal
title('medidas do magnet�metro originais em 3D')
xlabel('X(U.M.)'); ylabel('Y(U.M.)'); zlabel('Z(U.M.)');
grid on

%% Identifica��o dos estados:
%%% De acordo com os dados do arquivo 'teste_mesa_vermelha_2.txt'
%%% os estados do experimento s�o identificados segundo os seguintes
%%% ensaios:

% Ensaio 01: 5092:9241

interval.lower(1) = 5092;
interval.upper(1) = 9241;

% Ensaio 02: 9245:13310

interval.lower(2) = 9245;
interval.upper(2) = 13310;

% Ensaio 03: 14450:18590

interval.lower(3) = 14450;
interval.upper(3) = 18590;

% Ensaio 04: 18670:22740

interval.lower(4) = 18670;
interval.upper(4) = 22740;

% Ensaio 05: 23570:27570

interval.lower(5) = 23450;
interval.upper(5) = 27585;

% Ensaio 06: 27590:31670

interval.lower(6) = 27580;
interval.upper(6) = 31710;

% Ensaio 07: 32920:36830

interval.lower(7) = 32740;
interval.upper(7) = 36890;

% Ensaio 08: 37080:41020

interval.lower(8) = 36880;
interval.upper(8) = 41065;

% Ensaio 09: 41980:45880

interval.lower(9) = 41740;
interval.upper(9) = 45955;

% Ensaio 10: 46090:49970

interval.lower(10) = 45950;
interval.upper(10) = 50080;

% Ensaio 11: 51250:55180

interval.lower(11) = 51090;
interval.upper(11) = 55200;

% Ensaio 12: 55400:59250

interval.lower(12) = 55190;
interval.upper(12) = 59350;

%% N�mero de dados em cada amostra a ser considerada
for k = 1:E
    d(k)=interval.upper(k)- interval.lower(k);
end
Namostras = min(d);

%% Gir�metro

%%% Preencher a matriz de dados do gir�metros G
Ng=floor(Namostras/3);
G = zeros (Ng,E*Qm); % [U.G.]
for k=1:12
    G(:,3*k-2:3*k) = data(interval.lower(k)+Namostras/3:(interval.lower(k)+2*Namostras/3-1),7:9);
end

%% Estima��o por m�nimos quadrados
% Fun��o para obter os par�metros de calibra��o do sensor
% [ M_align, Sf, bias] = F_Gyro_Align( w, Wrt, eNL, G )
[ M_align, Sf, bias] = F_Align( w*Wrt, eNL, G ) 

save('MbiasSF_Gyro.mat','M_align', 'Sf', 'bias')

%% Verifica��o da Calibra��o do sensor
sensor=gyro';
Ws = M_align*Sf*sensor-M_align*bias*ones(1,size(sensor,2));

figure
plot(Ws(1,:))
title('medidas do gir�metro p�s estima��o')  
xlabel('n�mero de dados')
ylabel('medida do sensor (Graus/s)')
hold on
plot(Ws(2,:))
plot(Ws(3,:))
legend('eixo x','eixo y','eixo z')


