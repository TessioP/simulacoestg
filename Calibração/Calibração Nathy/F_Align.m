function [ M_align, Sf, bias ] = F_Align( f0, eNL, Medidas )
% Fun��o para alinhar um conjunto de sensores dispostos em uma geometria
% qualquer

%Autores: AspOf Nathalia Mattos
%ASE-C
%Data: 02/05/2018

% Par�metros de entrada:
% f0: Matriz com sequ�ncia de estados aplicados ao sistema de 
% coordenadas local escolhido.
% Para N estados, esta matriz conter� N linhas e para K aceler�metros 
% a matriz conter� K colunas. Expressa as medidas lidas no sistema
% ortogonal local
% eNL: (Graus) �ngulo de desalinhamento da mesa. Rota��o no eixo Norte.
% Erro no nivelamento da mesa em graus no plano vertical-leste. Expressa o
% desalinhamento entre o sistema local e o NEU. Pode ser compensado para
% uma mesa com 3 ou mais graus de liberdade, neste caso eNL = 0
% Medidas: Matriz de medidas, onde cada coluna corresponde �s medidas fornecidas
% pelo grupo de K sensores que est�o sendo alinhados para um dado estado.
% As medidas est�o em unidades naturais dos sensores
% Cada linha da matriz f0 corresponde a um estado no sistema local.
% Para um ensaio com N estados, uma matriz f0 com N linhas deve ser
% fornecida
% A matriz Medidas conter� N conjuntos de medidas dos sensores
% organizados em colunas. Se o sistema contiver K sensores, a matriz Medidas conter�
% K*N colunas

% Par�metro de sa�da:
% M_align: Matriz de alinhamento com rela��o ao NEU. Matriz de alinhamento,
% que transporta as medidas dos sensores para o sistema de coordenadas NEU
% A matriz possue dimens�o (3 x K), onde K � a quantidade de sensores.
% Sf: Matriz diagonal com os fatores de escala dos sensores em unidades de:
% [g(lab)/U.A. para aceler�metro; grau/s/U.G. para gir�metro; T/U.M. para magnet�metro; ]
% bias: Vetor com os biases dos sensores em unidades de:
% [g(lab) em NED para aceler�metro; grau/s para gir�metro; T para magnet�metro; ]

% Matriz para a corre��o do nivelamento da mesa em torno do eixo Z (Norte)
% Esta matriz rotaciona vetores no sistema local para o NEU
M_L2NEU = [cosd(eNL) -sind(eNL) 0;
           sind(eNL) cosd(eNL) 0;
           0 0 1];
       
% N�mero de posi��es ensaiadas
% = n�mero de linhas da matriz f0
% = n�mero de estados
% Cada linha da matriz de f0 � um estado, que corresponde a um vetor
% normalizado com a dire��o imposta no sistema local
% Cada vetor corresponde aos valores reais observados pelos sensores
E = size(f0,1);

% N�mero de sensores
% = n�mero de colunas da matriz f0
Qa = size(f0,2);

% Calculando a m�dia dos valores das colunas de Medidas
% Calcula a m�dia das medidas de cada sensor do grupo para cada estado -
% Medidas(grupo,estado)
Am = mean(Medidas);

% Reserva de espa�o para a matriz diagonal com os fatores de escala Fc e a
% matriz A com os vetores de medidas, cada qual em uma coluna
Fc = eye(Qa);
A = zeros(Qa,E);

% Preenche a matriz de medidas Ac (em unidades naturais)
% O vetor de medidas m�dias Am � organizado de outra forma em A
% Cada coluna de A corresponde �s medidas m�dias em um dado estado (E
% estados)
% As linhas correspondem �s medidas de cada um dos sensores (Qa sensores)
for j=1:E
    for i=1:Qa
        A(i,j)=Am((j-1)*Qa+i);
    end
end

% Aumentar a matriz F com os vetores de medidas
F = [f0';ones(1,E)];

%% Modelo de Erros

% SF*A = HLs*F+ruido

%% Estima��o por m�nimos quadrados

% C�lculo da matriz de forma (matriz de desalinhamento) estendida que 
% inclui os fatores de escala. 
% Matriz de forma concatenada com os biases dos sensores na �ltima coluna e 
% multiplicada pelos fatores de corre��o dos sensores nas linhas
% Utiliza-se m�nimos quadrados para calcular esta matriz
% Matriz que associa a medida real aplicada �s leituras dos sensores,
% incluindo tamb�m seus erros de alinhamento, fatores de corre��o de cada um
% dos sensores e outros erros n�o considerados, onde M=inv(Fc)*Mb_sL
Mbf_til = A*pinv(F)	

% C�lculo da Matriz diagonal com fatores de corre��o
% Encontra a Matriz de fatores de escala para os sensores 
% Sepada os fatores de escala da matriz de forma 
% A matriz de forma Mbf_til(i,1:Qa) � ortogonal e o m�dulo de suas linhas � 1
% Ent�o, o m�dulo das linhas da matriz de forma combinadas s�o os fatores
% de escala
for i=1:Qa
    % estima��o dos fatores de escala 
    Fc(i,i)=1/norm(Mbf_til(i,1:Qa),2); % Matriz diagonal do fator de escala
end

% C�lculo da matriz de forma ou de desalinhamento para o sistema de
% coordenadas local estendida
% Aquela concatenada com os biases dos sensores na �ltima coluna
% Retira a contribui��o do fator de escala da matriz de forma combinada
Mb_til = Fc*Mbf_til 

% C�lculo da matriz de forma ou de desalinhamento para o sistema de
% coordenadas local
% matriz da transforma��o do sistema de coordenadas local para
% o sistema dos sensores
M_til = Mb_til(:,1:Qa) %HsL

%% C�lculo dos biases 

% C�lculo do vetor de bias dos sensores no sistema local
b_til = Mb_til(:,Qa+1)

% C�lculo da matriz de alinhamento ou de desalinhamento inversa
% Utilizada para obter as medidas no sistema de coordenadas
% local
M_star = inv(M_til) %HLs 

% C�lculo da matriz de alinhamento, do vetor de bias e do vetor residual no
% sistema de coordenadas NEU
% Fator de escala � o mesmo em qualquer referencial
M_align=M_L2NEU*M_star
Sf = Fc
bias=M_L2NEU*b_til
end

