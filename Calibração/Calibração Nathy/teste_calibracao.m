 %Fun��o principal para o teste de calibra��o do magnet�metro
 % Atrav�s dos ensaios realizados no laborat�rio LICS

%Autores: AspOf Nathalia Mattos
%ASE-C
%Data: 23/08/2018

%Com base no Relat�rio T�cnico Calibra��o dos Sensores Inerciais de uma IMU
% de Marcelo Carvalho Tosin, 2012

% Usos: 
% 1. Calibra��o do magnet�metro HMC5883L do GY80
% Dados de entrada relativos ao ensaio do dia 02/05/18

% 2. Calibra��o do gir�metro L3G4200D do GY80
% Dados de entrada relativos ao ensaio do dia 26/04/18

% 3. Calibra��o do aceler�metro ADXL345 do GY80
% Dados de entrada relativos ao ensaio do dia 02/05/18

% Dados de entrada obtidos atrav�s da calibra��o dos sensores:
% function [ M_align, Sf, bias ] = F_Align( f0, eNL, Medidas )

% Par�metros de entrada:
% f0: Matriz com sequ�ncia de estados aplicados ao sistema de 
% coordenadas local escolhido.
% Para N estados, esta matriz conter� N linhas e para K aceler�metros 
% a matriz conter� K colunas. Expressa as medidas lidas no sistema
% ortogonal local
% eNL: (Graus) �ngulo de desalinhamento da mesa. Rota��o no eixo Norte.
% Erro no nivelamento da mesa em graus no plano vertical-leste. Expressa o
% desalinhamento entre o sistema local e o NEU. Pode ser compensado para
% uma mesa com 3 ou mais graus de liberdade, neste caso eNL = 0
% Medidas: Matriz de medidas, onde cada coluna corresponde �s medidas fornecidas
% pelo grupo de K sensores que est�o sendo alinhados para um dado estado.
% As medidas est�o em unidades naturais dos sensores
% Cada linha da matriz f0 corresponde a um estado no sistema local.
% Para um ensaio com N estados, uma matriz f0 com N linhas deve ser
% fornecida
% A matriz Medidas conter� N conjuntos de medidas dos sensores
% organizados em colunas. Se o sistema contiver K sensores, a matriz Medidas conter�
% K*N colunas

% Par�metro de sa�da:
% M_align: Matriz de alinhamento com rela��o ao NEU. Matriz de alinhamento,
% que transporta as medidas dos sensores para o sistema de coordenadas NEU
% A matriz possue dimens�o (3 x K), onde K � a quantidade de sensores.
% Sf: Matriz diagonal com os fatores de escala dos sensores em unidades de:
% [g(lab)/U.A. para aceler�metro; grau/s/U.G. para gir�metro; T/U.M. para magnet�metro; ]
% bias: Vetor com os biases dos sensores em unidades de:
% [g(lab) em NED para aceler�metro; grau/s para gir�metro; T para magnet�metro; ]

clear all
clc
close all



tic % in�cio do programa

% Carrega arquivo com os dados dos sensores
filename1 = 'magCalibration.txt'; % apenas o MAG
filename2 = 'teste_mesa_vermelha_2.txt'; % apenas o giro
filename3 = 'teste_mesa_vermelha_acc.txt'; % apenas o accel
data1 = importdata(filename1);
data2 = importdata(filename2);
data3 = importdata(filename3);
%% verifica��o do Magnet�metro

mag=data1(:,1:3);
figure
plot(mag(:,1)')
title('medidas do magnet�metro originais')
xlabel('n�mero de dados')
ylabel('medida do sensor(U.M.)')
hold on
plot(mag(:,2)')
plot(mag(:,3)')
legend('eixo x','eixo y','eixo z')

figure
plot3(mag(:,1),mag(:,2),mag(:,3)); axis equal
title('medidas do magnet�metro originais em 3D')
xlabel('X'); ylabel('Y'); zlabel('Z');
grid on

load ('MbiasSF_Mag.mat');
sensor=mag';
Ws = M_align*Sf*sensor-M_align*bias*ones(1,size(sensor,2));
for i=1:size(Ws,2)
    Ws(:,i) = Ws(:,i)/norm(Ws(:,i));
end

figure
plot(Ws(1,:))
title('medidas do magnet�metro calibrado')
xlabel('n�mero de dados')
ylabel('medida do sensor (T)')
hold on
plot(Ws(2,:))
plot(Ws(3,:))
legend('eixo x','eixo y','eixo z')

figure
plot3(Ws(1,:),Ws(2,:),Ws(3,:),'.'); axis equal
title('medidas do magnet�metro calibrado em 3D')
xlabel('X'); ylabel('Y'); zlabel('Z');
grid on

%% verifica��o do Gir�metro

gyro = data2(:,7:9);
figure
plot(gyro(:,1)')
title('medidas do gir�metro originais')
xlabel('n�mero de dados')
ylabel('medida do sensor(U.G.)')
hold on
plot(gyro(:,2)')
plot(gyro(:,3)')
legend('eixo x','eixo y','eixo z')

load ('MbiasSF_Gyro.mat');
sensor=gyro';
Ws = M_align*Sf*sensor-M_align*bias*ones(1,size(sensor,2));

figure
plot(Ws(1,:))
title('medidas do gir�metro calibrado')
xlabel('n�mero de dados')
ylabel('medida do sensor (Graus/s)')
hold on
plot(Ws(2,:))
plot(Ws(3,:))
legend('eixo x','eixo y','eixo z')

%% verifica��o do Aceler�metro

acc=data3(:,4:6);
figure
plot(acc(:,1)')
title('medidas do aceler�metro originais')
xlabel('n�mero de dados')
ylabel('medida do sensor(U.A.)')
hold on
plot(acc(:,2)')
plot(acc(:,3)')
legend('eixo x','eixo y','eixo z')

load ('MbiasSF_Acc.mat');
sensor=acc';
Ws = M_align*Sf*sensor-M_align*bias*ones(1,size(sensor,2));

figure
plot(Ws(1,:))
title('medidas do aceler�metro calibrado')  
xlabel('n�mero de medidas')
ylabel('medida do sensor (g(lab))')
hold on
plot(Ws(2,:))
plot(Ws(3,:))
legend('eixo x','eixo y','eixo z')


%%% Gravidade local
% g: (m/s^2) M�dulo da gravidade local aplicada aos eixos
% escolhidos. Normalmente estes eixos s�o os eixos da mesa de rota��o
% devidamente nivelada e com o �ngulo de azimute compensado
h = 600; %m
lat = -23.22; %graus
g =  gravitywgs84(h, -23, 0, 'Exact', 'None');

figure
plot(Ws(1,:)*g)
title('medidas do aceler�metro calibrado')  
xlabel('n�mero de medidas')
ylabel('medida do sensor (m/s^2)')
hold on
plot(Ws(2,:)*g)
plot(Ws(3,:)*g)
legend('eixo x','eixo y','eixo z')
