 %Fun��o principal para a calibra��o do aceler�metro

%Autores: AspOf Nathalia Mattos
%ASE-C
%Data: 02/05/2018

%Com base no Relat�rio T�cnico Calibra��o dos Sensores Inerciais de uma IMU
% de Marcelo Carvalho Tosin, 2012

% Uso: Calibra��o do aceler�metro ADXL345 do GY80
% Dados de entrada relativos ao ensaio do dia 02/05/18
% Fun��es utilizadas:
% function [ M_align, Sf, bias ] = F_Align( f0, eNL, Ac )

% Par�metros de entrada:
% f0: Matriz com sequ�ncia de for�as espec�ficas aplicadas ao referencial local
% eNL: �ngulo de desalinhamento da mesa. Rota��o no eixo Norte. (Graus)
% Ac: matriz de medidas dos aceler�metros. Matriz com os dados de interesse

% Par�metro de sa�da:
% M_align: Matriz de alinhamento dos sensores com rela��o ao NEU
% Sf: Matriz diagonal com os fatores de escala dos aceler�metros
% bias: Vetor com os biases dos aceler�metros no sistema NEU

clear all
clc
close all

tic % in�cio do programa

% �ngulo de desalinhamento da mesa
% Rota��o no eixo Z (norte) desalinhando os eixos XY (regra da m�o direita,
% sentido norte)
eNL = 0; % mesa de 3 eixos

% Matriz de estados com a sequ�ncia de for�as espec�ficas que agem sobre o
% sistema de coordenadas local 
% Inclue o erro de nivelamento da mesa no plano horizontal
f0 = [cosd(eNL) -sind(eNL) 0;
    -cosd(eNL) -sind(eNL) 0;
    cosd(eNL) 0 sind(eNL);
    -cosd(eNL) 0 sind(eNL);
    
    cosd(eNL) sind(eNL) 0;
    -cosd(eNL) sind(eNL) 0;
    cosd(eNL) 0 -sind(eNL);
    -cosd(eNL) 0 -sind(eNL);
    
    0 -cosd(eNL) sind(eNL);
    0 cosd(eNL) -sind(eNL);
    0 -cosd(eNL) -sind(eNL);
    0 cosd(eNL) sind(eNL);
    
    0 -sind(eNL) cosd(eNL);
    0 -sind(eNL) -cosd(eNL);
    0 sind(eNL) cosd(eNL);
    0 sind(eNL) -cosd(eNL)];

% N�mero de posi��es ensaiadas
% = n�mero de linhas da matriz f0
% = n�mero de estados
% Cada linha da matriz de f0 � um estado, que corresponde a um vetor
% normalizado com a dire��o imposta no sistema local
% Cada vetor corresponde aos valores reais observados pelos sensores
E = size(f0,1);

% N�mero de Aceler�metros
% = n�mero de colunas da matriz f0
Qa = size(f0,2);

% Carrega arquivo com os dados dos aceler�metros
filename = 'teste_mesa_vermelha_acc.txt';
data = importdata(filename);

gyro = data(:,7:9);
figure
plot(gyro(:,1)')
title('medidas do gir�metro originais')
xlabel('n�mero de dados')
ylabel('medida do sensor(U.G.)')
hold on
plot(gyro(:,2)')
plot(gyro(:,3)')
legend('eixo x','eixo y','eixo z')

mag=data(:,1:3);
figure
plot(mag(:,1)')
title('medidas do magnet�metro originais')
xlabel('n�mero de dados')
ylabel('medida do sensor(U.M.)')
hold on
plot(mag(:,2)')
plot(mag(:,3)')
legend('eixo x','eixo y','eixo z')

acc=data(:,4:6);
figure
plot(acc(:,1)')
title('medidas do aceler�metro originais')
xlabel('n�mero de dados')
ylabel('medida do sensor(U.A.)')
hold on
plot(acc(:,2)')
plot(acc(:,3)')
legend('eixo x','eixo y','eixo z')

%% Identifica��o dos estados:
%%% De acordo com os dados do arquivo 'teste_mesa_vermelha_acc.txt'
%%% os estados do experimento s�o identificados segundo os seguintes
%%% ensaios:

% Ensaio 1: 2944:4072

interval.lower(1) = 2944;
interval.upper(1) = 4072;

% Ensaio 02: 4714:5925

interval.lower(2) = 4714;
interval.upper(2) = 5925;

% Ensaio 03: 6491:7379

interval.lower(3) = 6491;
interval.upper(3) = 7379;

% Ensaio 04: 7944:8840

interval.lower(4) = 7944;
interval.upper(4) = 8840;

% Ensaio 05: 9477:10290

interval.lower(5) = 9477;
interval.upper(5) = 10290;

% Ensaio 06: 11090:11960

interval.lower(6) = 11090;
interval.upper(6) = 11960;

% Ensaio 07: 12620:13460

interval.lower(7) = 12620;
interval.upper(7) = 13460;

% Ensaio 08: 14150:14890

interval.lower(8) = 14150;
interval.upper(8) = 14890;

% Ensaio 09: 15690:16530

interval.lower(9) = 15690;
interval.upper(9) = 16530;

% Ensaio 10: 17140:17940

interval.lower(10) = 17140;
interval.upper(10) = 17940;

% Ensaio 11: 18730:19380

interval.lower(11) = 18730;
interval.upper(11) = 19380;

% Ensaio 12: 20040:20930

interval.lower(12) = 20040;
interval.upper(12) = 20930;

% Ensaio 13: 21570:22380

interval.lower(13) = 21570;
interval.upper(13) = 22380;

% Ensaio 14: 23090:23870

interval.lower(14) = 23090;
interval.upper(14) = 23870;

% Ensaio 15: 24470:25320

interval.lower(15) = 24470;
interval.upper(15) = 25320;

% Ensaio 16: 26060:26840

interval.lower(16) = 26060;
interval.upper(16) = 26840;

%% N�mero de dados em cada amostra a ser considerada
for k = 1:E
    d(k)=interval.upper(k)- interval.lower(k);
end
Namostras = min(d);

%% Aceler�metro

%%% Preencher a matriz de dados do aceler�metros Ac
Na=floor(Namostras/3);
Ac = zeros (Na,E*Qa); %[U.A.]
for k=1:E
    Ac(:,3*k-2:3*k) = data(interval.lower(k)+Na:(interval.lower(k)+2*Na-1),4:6);
end

%% Estima��o por m�nimos quadrados
% Fun��o para obter os par�metros de calibra��o do sensor
[ M_align, Sf, bias] = F_Align(  f0, eNL, Ac )

save('MbiasSF_Acc.mat','M_align', 'Sf', 'bias')

%% Verifica��o da Calibra��o do sensor
sensor=acc';
Ws = M_align*Sf*sensor-M_align*bias*ones(1,size(sensor,2)); %[g(lab)]

figure
plot(Ws(1,:))
title('medidas do aceler�metro p�s estima��o')  
xlabel('n�mero de medidas')
ylabel('medida do sensor (g(lab))')
hold on
plot(Ws(2,:))
plot(Ws(3,:))
legend('eixo x','eixo y','eixo z')

%%% Considerando a gravidade local

% g: (m/s^2) M�dulo da gravidade local aplicada aos eixos
% escolhidos. Normalmente estes eixos s�o os eixos da mesa de rota��o
% devidamente nivelada e com o �ngulo de azimute compensado
h = 600; %m
lat = -23.22; %graus
g =  gravitywgs84(h, -23, 0, 'Exact', 'None');

figure
plot(Ws(1,:)*g)
title('medidas do aceler�metro p�s estima��o')  
xlabel('n�mero de medidas')
ylabel('medida do sensor (m/s^2)')
hold on
plot(Ws(2,:)*g)
plot(Ws(3,:)*g)
legend('eixo x','eixo y','eixo z')
