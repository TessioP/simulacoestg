function [x_k1, P_k1] = ekf_loop(x_k, P_k, u_k, z_k1, Q_k, R_k1, dt)
% Filtro de Kalman Estendido para determina��o de atitude.
%
%   [x_k1, P_k1] = ekf_loop(x_k, P_k, u_k, z_k1, Q_k, R_k1) retorna o 
%   estado estimado no instante k+1 (x_k1) e matriz de covari�ncia 
%   associada P_k1 em fun��o do estado atual no instante k (x_k), da 
%   covari�ncia das incertezas associadas aos estado no intante k (P_k), 
%   das entradas de atua��o no instante k (u_k), das medidas obtidas dos 
%   sensores no instante k+1 (z_k1), das matrizes de covari�ncias
%   associadas ao ru�do de estado no instante k (Q_k) e do ru�do de medidas
%   no instante k+1 (R_k1).
%
%   x_k:  Vetor coluna 7x1 do estado estimados no instante k, sendo os
%         primeiros 4 valores correspondente ao quaternion de atitude, e os
%         3 demais, as velocidades angulares no ref. do corpo em rd/s
%   P_k:  Covariancia associada � incerteza do vetor de estados
%   u_k:  Vetor coluna 3x1 correspondente �s velocidades angulares de 
%         atua��o no ref. do corpo, em rd/s 
%   z_k1: Vetor coluna 6x1 das medidas, sendo os 3 primeiros valores 
%         correspondentes aos dados dos sensores girometricos em rd/s
%         e os demais valores correspondentes aos dados dos acelerometros 
%         em m/s^2. Todos as medidas s�o supostos lidos no ref. do corpo.
%   Q_k:  Covari�ncia do ru�do das entradas.
%   R_k1: Covariancia do ru�do das medidas.
%   dt:   Intervalo de tempo utilizado na discretiza��o
%
%   Autor: 1� Tenente Roberto BRUSNICKI - ASE-C
%   $Data: 04/10/2017 $

qe_k = x_k(1:4);
g = 9.7865;     % m/s^2    Valor local para S�o Jos� dos Campos/SP - Brasil

% Dist�ncias dos sensores ao eixo de giro da mesa (em metros)
Lx = 0.1;  % #### mudar para os valores corretos ####
Ly = 0.2;  % #### mudar para os valores corretos ####
Lz = 0.3;  % #### mudar para os valores corretos ####

%% Propaga��o do estado estimado

S = [  0      u_k(3) -u_k(2)  u_k(1);
     -u_k(3)    0     u_k(1)  u_k(2);
      u_k(2) -u_k(1)    0     u_k(3);
     -u_k(1) -u_k(2) -u_k(3)    0   ];

w_k = norm([u_k(1); u_k(2); u_k(3)]);

if w_k ~= 0
    Exp_Omega_k_T = cos(w_k*dt/2)*eye(4) + (1/w_k)*sin(w_k*dt/2)*S;
else
    Exp_Omega_k_T = eye(4) + (dt/2)*S;
end
  
qe_k1 = Exp_Omega_k_T*qe_k;

qe_k1 = qe_k1/norm(qe_k1);         %normaliza��o do quaternion

we_k1 = u_k;
xe_k1 = [ qe_k1; we_k1 ];

F_k_1  =[[ 1              0.5*dt*u_k(3) -0.5*dt*u_k(2)  0.5*dt*u_k(1)];
         [-0.5*dt*u_k(3)  1              0.5*dt*u_k(1)  0.5*dt*u_k(2)];
         [ 0.5*dt*u_k(2) -0.5*dt*u_k(1)  1              0.5*dt*u_k(3)];
         [-0.5*dt*u_k(1) -0.5*dt*u_k(2) -0.5*dt*u_k(3)  1            ]];

F_k_2  =[[ 0.5*dt*qe_k(4) -0.5*dt*qe_k(3)  0.5*dt*qe_k(2)];
         [ 0.5*dt*qe_k(3)  0.5*dt*qe_k(4) -0.5*dt*qe_k(1)];
         [-0.5*dt*qe_k(2)  0.5*dt*qe_k(1)  0.5*dt*qe_k(4)];
         [-0.5*dt*qe_k(1) -0.5*dt*qe_k(2) -0.5*dt*qe_k(3)]];
     
F_k  = zeros(7,7);

F_k(1:4,1:4) = F_k_1;
F_k(1:4,5:7) = F_k_2;

Csi_k = [-qe_k(2) -qe_k(3) -qe_k(4);
          qe_k(1) -qe_k(4)  qe_k(3);
          qe_k(4)  qe_k(1) -qe_k(2);
         -qe_k(3)  qe_k(2)  qe_k(1)];

Gama_k = (dt/2)*Exp_Omega_k_T*Csi_k;       

Q_q_k = Gama_k*Q_k(5:7,5:7)*Gama_k';    

Q_k(1:4,1:4) = Q_q_k;

P_k1 = F_k*P_k*F_k' + Q_k;

%% Predi��o das medidas
xe1 = xe_k1(1);      %q1
xe2 = xe_k1(2);      %q2
xe3 = xe_k1(3);      %q3
xe4 = xe_k1(4);      %q4
xe5 = xe_k1(5);      %wx
xe6 = xe_k1(6);      %wy
xe7 = xe_k1(7);      %wz
     

ze_k1  = [                      xe5;                               % gx
                                xe6;                               % gy
                                xe7;                               % gz 
          -2*g*(xe1*xe3-xe2*xe4)     - Lx*(xe6*xe6+xe7*xe7);       % ax
          -2*g*(xe2*xe3+xe1*xe4)     - Ly*(xe5*xe5+xe7*xe7);       % ay
          -g*(1-2*xe1*xe1-2*xe2*xe2) - Lz*(xe5*xe5+xe6*xe6)];      % az

H_k1 = [    0        0        0        0        1         0         0;
            0        0        0        0        0         1         0;
            0        0        0        0        0         0         1;
        -2*g*xe3  2*g*xe4 -2*g*xe1  2*g*xe2     0     -2*Lx*xe6 -2*Lx*xe7;
        -2*g*xe4 -2*g*xe3 -2*g*xe2 -2*g*xe1 -2*Ly*xe5     0     -2*Ly*xe7;
         4*g*xe1  4*g*xe2     0        0    -2*Lz*xe5 -2*Lz*xe6     0    ];

Pz_k1 = H_k1*P_k1*H_k1' + R_k1;

%% Covari�ncia cruzada entre estado e medidas     
Pxz_k1 = P_k1*H_k1';

%% Atualiza��o

%Ganho de Kalman
K_k1 = Pxz_k1*pinv(Pz_k1);

%Atualiza��o
x_k1 = xe_k1 + K_k1*(z_k1 - ze_k1);
x_k1(1:4) = x_k1(1:4)/norm(x_k1(1:4));  %normaliza��o do quaternion

P_k1 = P_k1 - K_k1*Pz_k1*K_k1';
    