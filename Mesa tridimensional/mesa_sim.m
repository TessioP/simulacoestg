function [p, w, a] = mesa_sim(M, dt)
%Simulador de v�rios movimentos de uma mesa de tr�s eixos.
%
%   [p, w, a] = mesa_sim(M) retorna as sequencias de posi��es
%   angulares (p), velocidades angulares (w) e acelera��es angulares (a) 
%   da mesa ao executar movimentos entre as posi�oes dadas pelas linhas da
%   matriz M, sendo a posi��o inicial a linha 1, a segunda posi��o a linha
%   2, e assim por diante.

%   p:   matriz 3xn das posi��es da mesa durante o movimento, em graus
%   w:   matriz 3xn das velocidades angulares da mesa, em graus/s 
%   a:   matriz 3xn das acelera��es angulares da mesa, em graus/s^2 
%   M:   matriz mx3 das m posi��es desejadas para a mesa 
%   dt:  Incremento de tempo em segundos

%   Autor: 1� Tenente Roberto BRUSNICKI - ASE-C
%   $Data: 26/09/2017 $
[n, m] = size(M);

p=[];
w=[];
a=[];

for i=1:n-1
    [p_aux w_aux] = mesa( M(i,:), M(i+1,:), dt );
    
    kk = length(p_aux);
    a_aux = zeros(3,kk);
    
    for k=1:kk-1
        a_aux(:,k) = (w_aux(:,k+1)-w_aux(:,k))/dt; 
    end
    
    p = [p p_aux];
    w = [w w_aux];
    a = [a a_aux];
    
end

% figure
% subplot(3,1,1)       
% plot((pi/180)*p');
% grid;
% title('posi��o')
% subplot(3,1,2)     
% plot((pi/180)*w');
% grid;      
% title('velocidade')
% subplot(3,1,3) 
% plot((pi/180)*a');
% grid;
% title('acelera��o')

