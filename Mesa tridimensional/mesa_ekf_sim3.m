function [x, u, z] = mesa_ekf_sim3(M, dt)
%Simulador de estima��o de atitude para uma mesa de tr�s eixos.
%
%   [x, u, z] = mesa_ekf_sim(M, dt) retorna as sequencias de estados
%   estimados (x) entradas de atua��o (u), e dados dos sensores (z) obtidos  
%   da mesa ao executar movimentos entre as posi�oes dadas pela matriz M.
%   Todas os dados s�o discretizados num intervalo de tempo dado por dt.
%
%   M:  Matriz mx3 composta por vetores linha 1x3 referentes �s posi��es  
%       angulares de cada eixo da mesa, dados em graus.
%   dt: Intervalo de tempo utilizado na discretiza��o
%   x:  Matriz 7xn dos vetores coluna 7x1 dos estado estimados, sendo os
%       primeiros 4 valores correspondente ao quaternion de atitude, e os
%       3 demais, as velocidades angulares no referencial do corpo em rd/s
%   u:  Matriz 3xn formada por vetores coluna 3x1 correspondete as 
%       velocidades angulares de atua��o no ref. do corpo, em rd/s 
%   z:  Matriz 6xn composta por vetores coluna 6x1, sendo os 3 primeiros 
%       valores correspondentes aos dados dos sensores girometricos em rd/s
%       e os demais valores correspondentes aos dados dos acelerometros em
%       m/s^2. Todos as medidas s�o supostos lidos no referencial do corpo.
%
%   Autor: 1� Tenente Roberto BRUSNICKI - ASE-C
%   $Data: 05/10/2017 $

if nargin == 0
%     Valores default, caso o usu�rio n�o forne�a entradas
   M = [[0 0 0];[185 0 0];[0 0 0];[0 180 0];[0 0 0];[0 0 150];[0 0 0];
       [30 -30 100];[0 0 0];[34 60 30];[185 -30 90];[-10 40 -35];[0 0 0]];
   dt = 0.01;
end

close all;
  
g = 9.7865;    % m/s^2    Valor local para S�o Jos� dos Campos/SP - Brasil

% Posi��o dos sensores em rela��o ao centro giro da mesa (em metros)
Lx = 0.1;           % #### mudar para os valores corretos ####
Ly = 0.2;           % #### mudar para os valores corretos ####
Lz = 0.3;           % #### mudar para os valores corretos ####

% Desvio padr�o utilizado nos ru�dos gaussianos
sigma1 = 0.01;      % desvio padr�o do ru�do adicionado � w_m 
sigma2 = 0.01;      % desvio padr�o do ru�do do giro
sigma3 = 0.01;      % desvio padr�o do ru�do do acelerometro
sigma4 = 0.01;      % P

Q = (sigma1^2)*eye(7);               % Covari�ncia do ru�do das entradas   

R = zeros(6,6);
R(1:3,1:3) = (sigma2^2)*eye(3);      % Covariancia do ru�do das medidas
R(4:6,4:6) = (sigma3^2)*eye(3);

P = (sigma4^2)*eye(7);               % Covariancia do ru�do do vetor de estados

% Cria os dados de posi��o, velocidade e acelera��o 
% para de cada eixo da mesa de forma independente 
[p_m, w_m, a_m] = mesa_sim(M, dt); 

p_m = p_m*(pi/180);     %rd
w_m = w_m*(pi/180);     %rd/s
% a_m = a_m*(pi/180);   %rd/s^2

n = length(p_m);    %numero de itera��es para o filtro
x = zeros(7,n);     % vetor de estados
u = zeros(3,n);     % vetor de entradas
z = zeros(6,n);     % vetor de medidas
qa = zeros(4,n);    % quaternion de atitude ideal
qa_r = zeros(4,n);  % quaternion de atitude real
w_b = zeros(3,n);   % velocidades angulares ideais no ref. do corpo
w_b_r = zeros(3,n); % velocidades angulares reais no ref. do corpo

% a_b = zeros(3,n);     % acelera��es angulares ideais no ref. do corpo
% a_b_r = zeros(3,n);   % acelera��es angulares reais no ref. do corpo

w_r = w_m + sigma1*wgn(3, n, 1); % cria velocidades angulares ruidosas
p_r = zeros(3,n);               
a_r = zeros(3,n);
for k=2:n
    % cria posi��o ruidosa a partir da velocidade ruidosa
    p_r(:,k) = p_r(:,k-1) + w_r(:,k-1)*dt;   
    % cria acelera��o ruidosa a partir da velocidade ruidosa
    a_r(:,k-1) = (w_r(:,k) - w_r(:,k-1))/dt;  
end

% A partir dos dados fornecidos pela simula��o da mesa (fun��o mesa_sim),
% e ap�s adi��o do ru�do acima, cria os dados simulados dos sensores no 
% referencial do corpo, de forma ideal (apenas com o ruido advindo da
% atua��o n�o-ideal)
for k=1:n
    
    fi = p_m(1,k);
    teta = p_m(2,k);
%    psi = p_m(3,k);        % angulos ideais da mesa
    
    fi_r = p_r(1,k);
    teta_r = p_r(2,k);
%     psi_r = p_r(3,k);      % angulos reais da mesa

% Transfere as velocidades angulares da mesa para o referencial do corpo
   D3 = [ 1      0       -sin(teta)     ;
          0   cos(fi)  cos(teta)*sin(fi);
          0  -sin(fi)  cos(teta)*cos(fi)];
   w_b(:,k) = D3*w_m(:,k);
%    a_b(:,k) = D3*a_m(:,k);
   
   D1 = [ 1      0       -sin(teta_r)         ;
          0   cos(fi_r)  cos(teta_r)*sin(fi_r);
          0  -sin(fi_r)  cos(teta_r)*cos(fi_r)];
   w_b_r(:,k) = D1*w_r(:,k);
%    a_b_r(:,k) = D3*a_r(:,k);
   
% Calcula quaternion de atitude 'ideal'            
    qa(:,k)   = e2q3([p_m(3,k),p_m(2,k),p_m(1,k)]);       
    qa_r(:,k) = e2q3([p_r(3,k),p_r(2,k),p_r(1,k)]);          
  
% Calcula matriz de rota��o do ref. inercial para o ref. do corpo
    D2_r = q2m(qa_r(:,k));
    
% Dados 'reais' dos giros, sem ruido do giro, mas com ruido da atua��o
    z(1:3,k) = w_b_r(:,k);        
    
% Cria os dados ideias dos sensores acelerometricos transferindo o vetor
% gravidade para o ref. do corpo, e soma as acelera��es centr�petas (e
% acelera��es tangencias)
    L = [ 0  -Lx -Lx;
         -Ly  0  -Ly;
         -Lz -Lz  0 ];
    Ac = L*(w_b_r(:,k).*w_b_r(:,k));    % acc's centr�petas
    
%     L = [ 0   Lz -Ly;
%          -Lz  0   Lx;
%           Ly -Lx  0 ];
%     At = L*a_b_r(:,k);                  % acc's tangenciais
   
% Dados 'reais' dos ac, sem ruido do acc, mas com ruido propagado da atua��o    
    z(4:6,k) = D2_r*[0 0 -g]' + Ac; %  + At;
end

% Cria sinal de comando de entrada 
u = w_b;         % Sem nenhum ruido! O comando � ideal.

% Adiciona ruido aos dados girom�tricos
z(1:3,:) = z(1:3,:) + sigma2*wgn(3, n, 1); 

% Adiciona ru�do aos dados acelerom�tricos
z(4:6,:) = z(4:6,:) + sigma3*wgn(3, n, 1); 

% Estado inicial
x(:,1) = [0 0 0 1 0 0 0]';

% Filtro de Kalman Estendido
for k=1:n-1;
    [x(:,k+1), P] = ekf_loop(x(:,k), P, u(:,k), z(:,k), Q, R, dt);
end


figure('pos',[10 100 950 500])
subplot(3,1,1) 
plot(qa');
grid;
title('quaternion comandado')
subplot(3,1,2)       
plot(qa_r');
grid;
title('quaternion executado')
subplot(3,1,3)       
plot(x(1:4,:)');
grid;
title('quaternion estimado');


figure('pos',[960 100 950 500])
subplot(3,1,1)
plot(w_b');
title('wb comandado');
grid;
subplot(3,1,2)      
plot(w_b_r');
title('wb executado');
grid;
subplot(3,1,3)
plot(x(5:7,:)');
title('wb estimado');
grid; 


figure('pos',[10 600 1900 400])
error = zeros(1,n);
for k=1:n
    e = qa_r(:,k) -x(1:4,k);
    error(k) = sqrt(sum(e.*e));
end
plot(error);
grid;
title('Distancia euclideana entre os quaternions executado e estimado')


