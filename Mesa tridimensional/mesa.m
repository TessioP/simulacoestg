function [p, w] = mesa(pos_0,pos_f, dt)
%Simulador de 1 �nico movimento de uma mesa de tr�s eixos.
%
%   [pos, w] = mesa(pos_0,pos_f) retorna as sequencias de posi��es
%   angulares (pos) e velocidades angulares (w) da mesa ao executar um
%   movimento entre as posi�oes inicial (pos_0) e posi��o final (pos_f)
%
%   pos_0: vetor 3x1 da posi��o inicial, dada em graus
%   pos_f: vetor 3x1 da posi��o final, dada em graus
%   p:   matriz 3xn das posi��es da mesa durante o movimento, em graus
%   w:   matriz 3xn das velocidades angulares da mesa, em graus/s 
%   dt:  Incremento de tempo em segundos
%
%   Autor: 1� Tenente Roberto BRUSNICKI - ASE-C
%   $Data: 26/09/2017 $

delta = pos_f'-pos_0';                  %�
sinal = sign(delta);                    %vetor 1x3 contendo +1 ou -1
delta = sinal.*delta;                   %torna o delta positivo por hora
T = 20;                                 %Intervalo de tempo total           #### calcular tempo necess�rio para fazer o movimento,
a = [30 15 5]';                         %�/s^2 acelera�ao angular constante         ### e retornar erro caso necess�rio
w_max = [45 20 10]';                    %�/s limite de velocidade angular
n = T/dt;                               %numero de passos no total
p = zeros(3,n);
p(:,n) = delta;
w = zeros(3,n);
w(:,1)   =  a*dt;
w(:,n-1) =  w(:,1);

t=dt;
p(:,2)  = p(:,1) + w(:,1)*t + a.*t.*t./2;
p(:,n-1)= p(:,n) - (p(:,2)-p(:,1));

for m=1:3
    
    i=2;
    t=2*dt;

    while (p(m,i)-p(m,1))<(p(m,n)-p(m,1))/2 &&  abs(w(m,i-1)) < w_max(m,1)
        p(m,i+1) = p(m,1) + w(m,1)*t + a(m,1)*t*t/2;
        p(m,n-i) = p(m,n) - p(m,i+1);
        w(m,i) = (p(m,i+1)-p(m,i))/dt;
        w(m,n-1) = w(m,i);
        i=i+1;
        t = t+dt;
    end

    flag = 0;
    n_aux = 0;
    if (p(m,i)-p(m,1)) > (p(m,n)-p(m,1))/2 

        if p(m,n+1-i) > p(m,i-1) 
            p(m,i) = (p(m,n)+p(m,1))/2;               %percurso m�dio
            p(m,i+1:2*i-1) = p(m,n+2-i:n);            %concatena o trecho final
            p(m,2*i:n) = p(m,n);                      %mantem a posi��o final 
            flag = 1;
        else
            p(m,i:2*i-2) = p(m,n+2-i:n);              %concatena o trecho final
            p(m,2*i-1:n) = p(m,n);                    %mantem a posi��o final
        end

    else
        n_aux = (p(m,n+2-i)-p(m,i-1))/(w_max(m)*dt);  %calcula n de passos faltantes
        n_aux = n_aux+1-mod(n_aux,1);                 %arredonda para o inteiro acima
        p_aux =(p(m,n+2-i)-p(m,i-1))/n_aux;           %calcula o passo a ser dado
        p(m,i-1:i-1+n_aux)=p(m,i-1):p_aux:p(m,n-i+2); %computa trecho com velocidade cte
        p(m,i-1+n_aux:2*i-2+n_aux-1) = p(m,n-i+2:n);  %concatena o trecho final
        p(m,2*i-2+n_aux:n) = p(m,n);                  %mantem a posi��o final    
        n_aux = n_aux - 1;                            %para uso posterior
    end

    for k=1:n-1
        w(m,k) = (p(m,k+1)-p(m,k))/dt;
    end

    j=0;
    while w(m,i-1) < max(w(m,:))
        j=j+1;
        w(m,i-1-j:i-1+j+flag+n_aux) = mean(w(m,i-1-j:i-1+j+flag+n_aux));
    end                                            

    for k = i-j:i+j+flag
        p(m,k) = p(m,k-1) + w(m,k-1)*dt;
    end

end

for k=1:3
    p(k,:) = pos_0(1,k) + sinal(k)*p(k,:);
    w(k,:) = sinal(k).*w(k,:);
end
% plot(p');
% grid;
% figure;
% plot(w');
% grid;